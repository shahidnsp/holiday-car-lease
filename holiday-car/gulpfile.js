var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.sass('app.scss');
    mix.rubySass('app.scss', null, {container: 'application-css', verbose: true});

    mix.scripts(['dash/angular.min.js','dash/angular-route.min.js','dash/angular-resource.min.js','dash/angular-notify.js','dash/ui-bootstrap-tpls.min.js','dash/angular-fancy-modal.min.js'],'public/js/angularjs.min.js');
    mix.scripts(['dash/angularscript.js','dash/services/*.js','dash/controller/*.js'],'public/js/app.js');

    mix.copy('resources/assets/js/dash/*.js','public/js');
    mix.copy('resources/assets/js/dash/plugins/float/*.js','public/js/plugins/float');
    mix.copy('resources/assets/js/dash/plugins/morris/*.js','public/js/plugins/morris');


    mix.copy('resources/assets/js/frontend/*.js','public/js');
    mix.copy('resources/assets/img','public/img');
    mix.copy('resources/assets/css','public/css');
    mix.copy('resources/assets/assets','public/assets');

    mix.copy('resources/assets/sass/dash/font-awesome/fonts','public/fonts');
});
