<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Gallery::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
    ];
});

$factory->define(App\Brand::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
    ];
});


$factory->define(App\NewestCar::class, function (Faker\Generator $faker) {
    return [

        'vehicle_id' => $faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\PopularCar::class, function (Faker\Generator $faker) {
    return [

        'vehicle_id' => $faker->randomElement([1,2,3,4,5]),
    ];
});
$factory->define(App\Booking::class, function (Faker\Generator $faker) {
    return [

        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'pickup_date' => $faker->dateTime,
        'return_date' => $faker->dateTime,
        'description' => $faker->sentence,
        'pickup_location_id' => $faker->randomElement([1,2,3,4,5]),
        'contact_information_id' => $faker->randomElement([1,2,3,4,5]),
        'home_address_id' => $faker->randomElement([1,2,3,4,5]),
        'proof_id' => $faker->randomElement([1,2,3,4,5]),
        'vehicle_id' => $faker->randomElement([1,2,3,4,5]),
        'user_id' => $faker->randomElement([1,2,3,4,5]),
        'bookingthrough' => $faker->randomElement(['Website','Android','iOS']),
        'isnew' => $faker->randomElement([1,0]),
        'amount' => '1000',
    ];
});
$factory->define(App\File::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->randomElement(['car.png','car2.png']),
        'temp' => 'cartemp.png',
        'gallery_id' => $faker->randomElement([1,2,3,4,5]),
        'vehicle_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]),
    ];
});

$factory->define(App\PickupLocation::class, function (Faker\Generator $faker) {
    return [

        'name' => $faker->name,
        'description' => $faker->sentence,
    ];
});
$factory->define(App\Vehicle::class, function (Faker\Generator $faker) {
    return [

        'name' => $faker->name,
        'transmission' => $faker->randomElement(['Automatic','Manual']),
        'displacement' => '1198 cc',
        'fuel' => $faker->randomElement(['Petrol','Diesel']),
        'bhp' => '67.04bhp@3800rpm',
        'mileage' => $faker->numberBetween(5,30),
        'torque' => '170Nm@1800-2400rpm',
        'engine' => '1.2-litre 86.8bhp 16V i-VTEC Engine',
        'seat' => $faker->randomElement([1,2,3,4,5]),
        'brand_id' => $faker->randomElement([1,2,3,4,5]),
        'center_lock' => $faker->randomElement([1,0]),
        'audio_system' => $faker->randomElement([1,0]),
        'video' => $faker->randomElement([1,0]),
        'gps' => $faker->randomElement([1,0]),
        'airbag' => $faker->randomElement([1,0]),
        'no_airbag' => $faker->numberBetween(1,5),
        'to15' => $faker->numberBetween(950,1600),
        'to25' => $faker->numberBetween(850,1500),
        'to35' => $faker->numberBetween(750,1400),
        'to45' => $faker->numberBetween(650,1300),
        'to55' => $faker->numberBetween(550,1200),
        'above' => $faker->numberBetween(450,110),
        'modelyear' => $faker->numberBetween(2010,2017),
        'noofgear' => $faker->numberBetween(4,8),
    ];
});

$factory->define(App\Testimonial::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'designation' => $faker->name,
        'content' => $faker->sentence,
        'photo' => 'profile.png',
    ];
});

$factory->define(App\Contact::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'subject' => $faker->firstName,
        'message' => $faker->sentence,
    ];
});

$factory->define(App\Slider::class, function (Faker\Generator $faker) {
    return [
        'photo' => 'slider1.jpg',
    ];
});

$factory->define(App\ContactInformation::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'mobile' => $faker->phoneNumber,
        'dateofbirth' => $faker->date,
        'gender' =>  $faker->randomElement(['Male','Female']),
        'user_id' =>  $faker->randomElement([1,2,3,4,5]),
    ];
});

$factory->define(App\HomeAddress::class, function (Faker\Generator $faker) {
    return [
        'address1' => $faker->address,
        'address2' => $faker->address,
        'post' => $faker->city,
        'pincode' => $faker->postcode,
        'city' =>  $faker->city,
        'state' =>  $faker->city,
        'user_id' =>  $faker->randomElement([1,2,3,4,5]),
    ];
});

$factory->define(App\Proof::class, function (Faker\Generator $faker) {
    return [
        'adhaar' => $faker->name,
        'license' => $faker->name,
        'user_id' =>  $faker->randomElement([1,2,3,4,5]),
    ];
});

$factory->define(App\Notification::class, function (Faker\Generator $faker) {
    return [
        'message' => $faker->sentence,
        'read' => $faker->randomElement([1,0]),
        'user_id' =>  $faker->randomElement([1,2,3,4,5]),
    ];
});