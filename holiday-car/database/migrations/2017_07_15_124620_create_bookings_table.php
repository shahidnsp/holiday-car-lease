<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->dateTime('pickup_date'); //CHANGED FROM Date to DateTime
            $table->dateTime('return_date'); //CHANGED FROM Date to DateTime
            $table->string('description');
            $table->integer('pickup_location_id');
            $table->integer('vehicle_id');
            $table->decimal('amount',10,2);
            $table->decimal('processfee',10,2)->default(0);
            $table->decimal('advance',10,2)->default(0);
            $table->boolean('isnew')->default(1);
            $table->string('bookingthrough');//NEW FIELD ADDED....................
            $table->integer('user_id')->nullable();//NEW FIELD ADDED....................
            $table->integer('contact_information_id')->nullable();//NEW FIELD ADDED....................
            $table->integer('home_address_id')->nullable();//NEW FIELD ADDED....................
            $table->integer('proof_id')->nullable();//NEW FIELD ADDED....................
            $table->integer('endbooking')->default(0);//NEW FIELD ADDED....................
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bookings');
    }
}
