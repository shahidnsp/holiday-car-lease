<?php

use Illuminate\Database\Seeder;

class HomeAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gallery=factory(App\HomeAddress::class,10)->create();
    }
}
