<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        Model::unguard();

        \App\User::create([
            'username'=>'admin',
            'password'=>'admin',
            'name'=>'Admin',
            'photo'=>'profile.png']);

        \App\PickupLocation::create([
            'name'=>'Kochi',
            'description'=>'']);

        \App\PickupLocation::create([
            'name'=>'Calicut',
            'description'=>'']);

        \App\PickupLocation::create([
            'name'=>'Trivandrum',
            'description'=>'']);

        \App\Brand::create([
            'name'=>'MARUTHI',
            'description'=>'']);

        \App\Brand::create([
            'name'=>'TOYOTA',
            'description'=>'']);

        \App\Brand::create([
            'name'=>'MAHINDRA',
            'description'=>'']);

        \App\Brand::create([
            'name'=>'HONDA',
            'description'=>'']);

        \App\Brand::create([
            'name'=>'HYUNDAI',
            'description'=>'']);

        \App\Brand::create([
            'name'=>'HYUNDAI',
            'description'=>'']);

        $info->comment('Admin user created');
        $info->error('Username : admin Password:admin');

        $info->info('Gallery table seeding started...');
        $this->call('GallerySeeder');

        $info->info('NewestCar table seeding started...');
        $this->call('NewestCarSeeder');

        $info->info('PopularCar table seeding started...');
        $this->call('PopularCarSeeder');

        $info->info('Booking table seeding started...');
        $this->call('BookingSeeder');

        $info->info('Files table seeding started...');
        $this->call('FileSeeder');

        $info->info('Vehicle table seeding started...');
        $this->call('VehicleSeeder');

        $info->info('Testimonial table seeding started...');
        $this->call('TestimonialSeeder');

        $info->info('Contact table seeding started...');
        $this->call('ContactSeeder');

        $info->info('Slider table seeding started...');
        $this->call('SliderSeeder');

        $info->info('Contact Information table seeding started...');
        $this->call('ContactInformationSeeder');

        $info->info('Home Address table seeding started...');
        $this->call('HomeAddressSeeder');

        $info->info('Proof table seeding started...');
        $this->call('ProofSeeder');

        $info->info('Notification table seeding started...');
        $this->call('NotificationSeeder');

        $info->error('Seeding Completed.........');
        Model::reguard();
    }
}
