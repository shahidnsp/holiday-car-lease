<?php

use Illuminate\Database\Seeder;

class PopularCarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $popularcar=factory(App\PopularCar::class,10)->create();
    }
}
