<?php

use Illuminate\Database\Seeder;

class NewestCarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $newestcar=factory(App\NewestCar::class,8)->create();
    }
}
