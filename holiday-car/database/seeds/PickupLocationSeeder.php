<?php

use Illuminate\Database\Seeder;

class PickupLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pickuplocation=factory(App\PickupLocation::class,10)->create();
    }
}
