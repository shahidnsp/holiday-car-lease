var app = angular.
    module('myWeb', []);

/*app.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});*/

app.controller('IndexController', function($scope,$http,$location,$anchorScroll){
    $scope.vehicle_id='1';
    $scope.selectCar=function(id){
        $scope.vehicle_id=id;
        console.log(id);
    };

    $scope.getVehicleAmount=function(pickup_date,return_date,vehicle_id){
        $http.post('getVehicleAmount',{fromDate:pickup_date,toDate:return_date,vehicle_id:vehicle_id}).
            success(function (data, status) {
                $scope.billamount = data;
                console.log(data);
            });
    };

    $scope.scrollDown=function(){
        $location.hash('message');
        $anchorScroll();
    };


    $scope.isOpen = false;

    $scope.openCalendar = function(e) {
        e.preventDefault();
        e.stopPropagation();

        $scope.isOpen = true;
    };
});