var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ngNotify',
        'ui.bootstrap',
        'vesparny.fancyModal',
        'BrandService',
        'LocationService',
        'BookingService',
        'VehcleService',
        'NewestCarService',
        'PopularCarService',
        'TestimonialService',
        'GalleryService',
        'ContactService',
        'SliderService',
    ]);

	app.config(function($routeProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard',
            controller: 'DashboardController'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'DashboardController'
		})
        .when('/brand', {
			templateUrl: 'template/brand',
			controller: 'BrandController'
		})
        .when('/location', {
            templateUrl: 'template/pickup_location',
            controller: 'LocationController'
        })
        .when('/booking', {
            templateUrl: 'template/booking',
            controller: 'BookingController'
        })
        .when('/vehicle', {
            templateUrl: 'template/vehicle',
            controller: 'VehicleController'
        })
        .when('/newestcar', {
            templateUrl: 'template/newestcar',
            controller: 'NewestCarController'
        })
         .when('/popularcar', {
            templateUrl: 'template/popularcar',
            controller: 'PopularCarController'
        })
        .when('/gallery', {
            templateUrl: 'template/gallery',
            controller: 'GalleryController'
        })
        .when('/testimonial', {
            templateUrl: 'template/testimonial',
            controller: 'TestimonialController'
        })
        .when('/contact', {
            templateUrl: 'template/contact',
            controller: 'ContactController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller: 'ProfileController'
        })
        .when('/slider', {
            templateUrl: 'template/slider',
            controller: 'SliderController'
        })
        .when('/status', {
            templateUrl: 'template/status',
            controller: 'StatusController'
        })
		.otherwise({
			redirectTo: 'template/dashboard'
		});
	});

    app.filter('pagination', function() {
      return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
          var start = (currentPage-1)*pageSize;
          var end = currentPage*pageSize;
          return input.slice(start, end);
        }
      };
    });
    app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);
    app.filter('sum', function(){
        return function(items, prop){
            return items.reduce(function(a, b){
                return a + b[prop];
            }, 0);
        };
    });

    app.directive('ngFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngFileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var values = [];

                    for (var i = 0; i < element[0].files.length; i++) {
                        var reader = new FileReader();
                        scope.index=i;
                        reader.onload = function (loadEvent) {
                            var value = {
                                lastModified: changeEvent.target.files[scope.index].lastModified,
                                lastModifiedDate: changeEvent.target.files[scope.index].lastModifiedDate,
                                name: changeEvent.target.files[scope.index].name,
                                size: changeEvent.target.files[scope.index].size,
                                type: changeEvent.target.files[scope.index].type,
                                data: loadEvent.target.result
                            }
                            values.push(value);
                        }

                        reader.readAsDataURL(changeEvent.target.files[i]);
                    }

                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        }
    }]);

    app.directive('showDuringResolve', function($rootScope) {

        return {
            link: function(scope, element) {

                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });

