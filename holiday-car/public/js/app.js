var app = angular.
    module('myApp', [
        'ngRoute',
        'ngResource',
        'ngNotify',
        'ui.bootstrap',
        'vesparny.fancyModal',
        'BrandService',
        'LocationService',
        'BookingService',
        'VehcleService',
        'NewestCarService',
        'PopularCarService',
        'TestimonialService',
        'GalleryService',
        'ContactService',
        'SliderService',
    ]);

	app.config(function($routeProvider, $locationProvider) {
		//$locationProvider.html5Mode(true);
		$routeProvider
		.when('/', {
			templateUrl: 'template/dashboard',
            controller: 'DashboardController'
		})
		.when('/dashboard', {
			templateUrl: 'template/dashboard',
			controller: 'DashboardController'
		})
        .when('/brand', {
			templateUrl: 'template/brand',
			controller: 'BrandController'
		})
        .when('/location', {
            templateUrl: 'template/pickup_location',
            controller: 'LocationController'
        })
        .when('/booking', {
            templateUrl: 'template/booking',
            controller: 'BookingController'
        })
        .when('/vehicle', {
            templateUrl: 'template/vehicle',
            controller: 'VehicleController'
        })
        .when('/newestcar', {
            templateUrl: 'template/newestcar',
            controller: 'NewestCarController'
        })
         .when('/popularcar', {
            templateUrl: 'template/popularcar',
            controller: 'PopularCarController'
        })
        .when('/gallery', {
            templateUrl: 'template/gallery',
            controller: 'GalleryController'
        })
        .when('/testimonial', {
            templateUrl: 'template/testimonial',
            controller: 'TestimonialController'
        })
        .when('/contact', {
            templateUrl: 'template/contact',
            controller: 'ContactController'
        })
        .when('/profile', {
            templateUrl: 'template/profile',
            controller: 'ProfileController'
        })
        .when('/slider', {
            templateUrl: 'template/slider',
            controller: 'SliderController'
        })
        .when('/status', {
            templateUrl: 'template/status',
            controller: 'StatusController'
        })
		.otherwise({
			redirectTo: 'template/dashboard'
		});
	});

    app.filter('pagination', function() {
      return function(input, currentPage, pageSize) {
        if(angular.isArray(input)) {
          var start = (currentPage-1)*pageSize;
          var end = currentPage*pageSize;
          return input.slice(start, end);
        }
      };
    });
    app.filter('percentage', ['$filter', function ($filter) {
        return function (input, decimals) {
            return $filter('number')(input * 100, decimals) + '%';
        };
    }]);
    app.filter('sum', function(){
        return function(items, prop){
            return items.reduce(function(a, b){
                return a + b[prop];
            }, 0);
        };
    });

    app.directive('ngFileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.ngFileModel);
                var isMultiple = attrs.multiple;
                var modelSetter = model.assign;
                element.bind("change", function (changeEvent) {
                    var values = [];

                    for (var i = 0; i < element[0].files.length; i++) {
                        var reader = new FileReader();
                        scope.index=i;
                        reader.onload = function (loadEvent) {
                            var value = {
                                lastModified: changeEvent.target.files[scope.index].lastModified,
                                lastModifiedDate: changeEvent.target.files[scope.index].lastModifiedDate,
                                name: changeEvent.target.files[scope.index].name,
                                size: changeEvent.target.files[scope.index].size,
                                type: changeEvent.target.files[scope.index].type,
                                data: loadEvent.target.result
                            }
                            values.push(value);
                        }

                        reader.readAsDataURL(changeEvent.target.files[i]);
                    }

                    scope.$apply(function () {
                        if (isMultiple) {
                            modelSetter(scope, values);
                        } else {
                            modelSetter(scope, values[0]);
                        }
                    });
                });
            }
        }
    }]);

    app.directive('showDuringResolve', function($rootScope) {

        return {
            link: function(scope, element) {

                element.addClass('ng-hide');

                var unregister = $rootScope.$on('$routeChangeStart', function() {
                    element.removeClass('ng-hide');
                });

                scope.$on('$destroy', unregister);
            }
        };
    });


/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('BookingService',[]).factory('Booking',['$resource',
    function($resource){
        return $resource('/api/booking/:bookingId',{
            bookingId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('BrandService',[]).factory('Brand',['$resource',
    function($resource){
        return $resource('/api/brand/:brandId',{
            brandId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('ContactService',[]).factory('Contact',['$resource',
    function($resource){
        return $resource('/api/contact/:contactId',{
            contactId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('GalleryService',[]).factory('Gallery',['$resource',
    function($resource){
        return $resource('/api/gallery/:galleryId',{
            galleryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('LocationService',[]).factory('Location',['$resource',
    function($resource){
        return $resource('/api/pickuplocatioon/:pickuplocatioonId',{
            pickuplocatioonId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('NewestCarService',[]).factory('NewestCar',['$resource',
    function($resource){
        return $resource('/api/newestcar/:newestcarId',{
            newestcarId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('PopularCarService',[]).factory('PopularCar',['$resource',
    function($resource){
        return $resource('/api/popularcar/:popularcarId',{
            popularcarId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('SliderService',[]).factory('Slider',['$resource',
    function($resource){
        return $resource('/api/slider/:sliderId',{
            sliderId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('TestimonialService',[]).factory('Testimonial',['$resource',
    function($resource){
        return $resource('/api/testimonial/:testimonialId',{
            testimonialId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('VehcleService',[]).factory('Vehicle',['$resource',
    function($resource){
        return $resource('/api/vehicle/:vehicleId',{
            vehicleId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);
app.controller('BookingController', function($scope,$http,$anchorScroll,$filter,$rootScope,$fancyModal,ngNotify,Booking,Vehicle,Location){

    $scope.bookingedit=false;
    $scope.showList=true;
    $scope.myBooking={};
    $scope.bookings=[];
    $scope.vehicles=[];
    $scope.locations=[];


    Booking.query(function(booking){
        $scope.bookings=booking;
    });

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    Location.query(function(location){
        $scope.Locations=location;
    });

    $scope.newBooking = function (argument) {
        $scope.bookingedit = true;
        $scope.newbooking = new Booking();
        $scope.curBooking = {};
        $scope.newbooking.pickup_date=new Date();
        $scope.newbooking.return_date=new Date();
        $scope.showList=true;
        $scope.myBooking={};
    };

    $scope.editBooking=function(thisBooking){
        $scope.bookingedit = true;
        $scope.newbooking =angular.copy(thisBooking);
        $scope.curBooking=thisBooking;
        $scope.showList=true;
        $scope.myBooking={};
    };

    $scope.fromDate=new Date();
    $scope.toDate=new Date();
    $scope.status=1;

    $scope.search=function(status,fromDate,toDate){
        Booking.query({status:status,fromDate:fromDate,toDate:toDate},function(booking){
            $scope.bookings=booking;
        });
    };

    $scope.getAmount=function(fromDate,toDate,vehicle_id){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd hh:mm:ss');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd hh:mm:ss');

        $http.post('/getVehicleAmount', {fromDate:from,toDate:to,vehicle_id:vehicle_id}).
            success(function (data, status) {
               $scope.newbooking.amount=data;
            });
    };


    $scope.showDetails=function(booking){
        $scope.myBooking=booking;
        $scope.showList=false;
    };

    $scope.backToList=function(){
        $scope.myBooking={};
        $scope.showList=true;
    };


    $scope.deleteBooking = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.bookings.indexOf(item);
                $scope.bookings.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Booking Removed Successfully');
            });
        }
    };

    $scope.endBooking = function (item) {
        var confirmDelete = confirm("Do you really need to end Booking ?");
        if (confirmDelete) {

            $http.post('/api/endbooking',{id:item.id}).
                success(function(data, status)
                {
                    angular.extend(item, item, data);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Ended Successfully');
                });


        }
    };


    $scope.addBooking = function () {
       // $scope.newbooking.pickup_date = $scope.newbooking.pickup_date.toISOString();
        //$scope.newbooking.return_date = $scope.newbooking.return_date.toISOString();
            if ($scope.curBooking.id) {
                $scope.newbooking.$update(function(brand){
                    angular.extend($scope.curBooking, $scope.curBooking, brand);

                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Updated Successfully');
                });
            } else {
                $scope.newbooking.$save(function (booking) {
                    $scope.bookings.push(booking);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Saved Successfully');
                });
            }

            $scope.bookingedit = false;
            $scope.newbooking = new Booking();
            $scope.showList=true;
        };
    
    $scope.cancelBooking = function () {
        $scope.bookingedit = false;
        $scope.newbooking = new Booking();
    };

    $scope.moveToNotified=function(booking){

        if(booking.isnew==1){
            var modal=$fancyModal.open({
                templateUrl: 'template/advance',
                controller: 'AdvanceModalCtrl',
                resolve: {
                    booking: function () {
                        return booking;
                    }
                }
            });

        }else{
            $http.post('/api/updateBookingStatus',{id:booking.id}).
                success(function(data, status)
                {
                    angular.extend(booking, booking, data);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Status Changed Successfully');
                });
        }

    };


    $rootScope.$on('$fancyModal.closed', function (e, id) {
        Booking.query(function(booking){
            $scope.bookings=booking;
        });
    });


});

app.controller('AdvanceModalCtrl', function($scope,$http,ngNotify,$fancyModal,booking){
    $scope.booking=booking;


    $scope.submit=function(booking){
        $http.post('/api/submitandapprovebooking', {id:booking.id,processfee:booking.processfee,advance:booking.advance}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});
app.controller('BrandController', function($scope,$http,$anchorScroll,ngNotify,Brand){
    $scope.brands=[];
    $scope.brandedit=false;

    Brand.query(function(brand){
       $scope.brands=brand;
    });

    $scope.newBrand = function (argument) {
        $scope.brandedit = true;
        $scope.newbrand = new Brand();
        $scope.curBrand = {};
    };
    $scope.editBrand = function (thisBrand) {
        $scope.brandedit = true;
        $scope.curBrand =  thisBrand;
        $scope.newbrand = angular.copy(thisBrand);
        $anchorScroll();
    };
    $scope.addBrand = function () {

        if ($scope.curBrand.id) {
            $scope.newbrand.$update(function(brand){
                angular.extend($scope.curBrand, $scope.curBrand, brand);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brand Updated Successfully');
            });
        } else{
            $scope.newbrand.$save(function(brand){
                $scope.brands.push(brand);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brand Saved Successfully');
            });
        }
        $scope.brandedit = false;
        $scope.newbrand = new Brand();
    };
    $scope.deleteBrand = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.brands.indexOf(item);
                $scope.brands.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brand Removed Successfully');
            });
        }
    };
    $scope.cancelBrand = function () {
        $scope.brandedit = false;
        $scope.newbrand = new Brand();
    };
});
app.controller('ContactController', function($scope,$http,$anchorScroll,ngNotify,Contact){
    $scope.contacts=[];

    Contact.query(function(contact){
        $scope.contacts=contact;
    });

    $scope.showMessage=false;
    $scope.myMessage={};

    $scope.showSingleMessage=function(message){
        $scope.showMessage=true;
        $scope.myMessage=message;
    };

    $scope.backToList=function(){
        $scope.showMessage=false;
        $scope.myMessage={};
    };
    
    $scope.deleteContact = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.contacts.indexOf(item);
                $scope.contacts.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Removed Successfully');
            });
        }
    };
});
app.controller('DashboardController', function($scope,$http){
    $scope.status={};

    function loadStatus() {
        $http.get('/api/getStatus').
            success(function (data, status) {
                $scope.status = data;
                console.log(status);
            });
    }
    loadStatus();

    $scope.todaybookings=[];
    function loadTodayBooking() {
        $http.get('/api/todayBooking').
            success(function (data, status) {
                $scope.todaybookings = data;
            });
    }
    loadTodayBooking();

    $scope.todaybookings=[];
    function loadnewBooking() {
        $http.get('/api/newBooking').
            success(function (data, status) {
                $scope.newbookings = data;
                console.log(data);
            });
    }
    loadnewBooking();
});
app.controller('GalleryController', function($scope,$http,$anchorScroll,ngNotify,Gallery){
    $scope.galleries=[];
    $scope.galleryedit=false;
    $scope.isEdit=false;

    Gallery.query(function(gallery){
        $scope.galleries=gallery;
    });

    $scope.newGallery = function (argument) {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.curGallery = {};
        $scope.isEdit=false;
    };
    $scope.editGallery = function (thisGallery) {
        $scope.galleryedit = true;
        $scope.curGallery =  thisGallery;
        $scope.newgallery = angular.copy(thisGallery);
        $anchorScroll();
        $scope.isEdit=true;
    };
    $scope.addGallery = function () {

        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function(gallery){
                angular.extend($scope.curGallery, $scope.curGallery, gallery);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });
        } else{
            $scope.newgallery.$save(function(gallery){
                $scope.galleries.push(gallery);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Saved Successfully');
            });
        }
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };
    $scope.deleteGallery = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.galleries.indexOf(item);
                $scope.galleries.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Removed Successfully');
            });
        }
    };
    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.deletePhoto=function(item){
        var r = confirm("Are you sure want to delete this photo!");
        if (r == true) {
            $http.delete('api/file/' + item.id).
                success(function (data, status, headers, config) {
                    var curIndex = $scope.newgallery.files.indexOf(item);
                    $scope.newgallery.files.splice(curIndex, 1);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };
});
app.controller('HomeController', function($scope,$http,$location){
    /* paggination */
    //TODO factory
    $scope.extra=false;
    if (localStorage.getItem("itemPerPage") === null) {
        localStorage.setItem("itemPerPage", 10);
    }
    function getPerPage(){
        return parseInt(localStorage.itemPerPage);
    }

    $scope.changeNum = function (itemNum) {
        localStorage.itemPerPage = itemNum;
        $scope.numPerPage = getPerPage();
    };

    $scope.name="";


    //console.log(localStorage.itemPerPage);
    $scope.numsForPage = [5, 10, 25, 50, 100];
    $scope.currentPage = 1;
    $scope.numPerPage = getPerPage();
    $scope.maxSize = 5;
    /* Nav menu */


    $scope.menuClass = function(page) {
        var current = $location.path().substring(1);
        return page === current ? "active" : "";
    };
});


app.directive('clickAnywhereButHere', function($document){
    return {
        restrict: 'A',
        link: function(scope, elem, attr, ctrl) {
            elem.bind('click', function(e) {
                // this part keeps it from firing the click on the document.
                e.stopPropagation();
            });
            $document.bind('click', function() {
                // magic here.
                scope.$apply(attr.clickAnywhereButHere);
            })
        }
    }
});
app.controller('LocationController', function($scope,$anchorScroll,ngNotify,Location){
    $scope.locations=[];

    $scope.locationedit=false;
    $scope.curLocation={};

    Location.query(function(location){
        $scope.locations=location;
    });

    $scope.newLocation=function(){
        $scope.locationedit=true;
        $scope.newlocation=new Location();
        $scope.curLocation={};
    };

    $scope.cancelLocation=function(){
        $scope.locationedit=false;
        $scope.newlocation=new Location();
    };

    $scope.editLocation=function(thisLocation){
        $scope.curLocation=thisLocation;
        $scope.newlocation=angular.copy(thisLocation);
        $scope.locationedit=true;
        $anchorScroll();
    };

    $scope.addLocation=function(){
        if( $scope.curLocation.id){
            $scope.newlocation.$update(function(brand){
                angular.extend($scope.curLocation, $scope.curLocation, brand);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Location Updated Successfully');
            });
        }else {
            $scope.newlocation.$save(function (location) {
                $scope.locations.push(location);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Location Saved Successfully');
            });
        }

        $scope.locationedit=false;
        $scope.newlocation=new Location();
    };

    $scope.deleteLocation=function(item){
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.locations.indexOf(item);
                $scope.locations.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Location Removed Successfully');
            });
        }
    };


});
app.controller('NewestCarController', function($scope,$anchorScroll,ngNotify,Vehicle,NewestCar){

    $scope.caredit=false;
    
    $scope.vehicles=[];

    $scope.newestcars=[];

    NewestCar.query(function(newestcar){
        $scope.newestcars=newestcar;
    });

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    $scope.newNewestCar = function (argument) {
        $scope.caredit = true;
        $scope.newcar = new NewestCar();
    };
    $scope.addNewestCar = function () {
        $scope.newcar.$save(function(newestcar){
            $scope.newestcars.push(newestcar);
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });

            ngNotify.set('NewestCar Saved Successfully');
        });
        $scope.caredit = false;
        $scope.newcar = new NewestCar();
    };
    $scope.deleteNewestCar = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.newestcars.indexOf(item);
                $scope.newestcars.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Newest Car Removed Successfully');
            });
        }
    };
    $scope.cancelNewestCar = function () {
        $scope.caredit = false;
        $scope.newcar = new NewestCar();
    };
});
app.controller('PopularCarController', function($scope,$anchorScroll,ngNotify,Vehicle,PopularCar){

    $scope.caredit=false;

    $scope.vehicles=[];
    $scope.popularcars=[];

    PopularCar.query(function(popularcar){
        $scope.popularcars=popularcar;
    });

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    $scope.newPopularCar = function (argument) {
        $scope.caredit = true;
        $scope.newcar = new PopularCar();
    };
    $scope.addPopularCar = function () {
        $scope.newcar.$save(function(popularcar){
            $scope.popularcars.push(popularcar);
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });

            ngNotify.set('PopularCar Saved Successfully');
        });
        $scope.caredit = false;
        $scope.newcar = new PopularCar();
    };
    $scope.deletePopularCar = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.popularcars.indexOf(item);
                $scope.popularcars.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('PopularCar Car Removed Successfully');
            });
        }
    };
    $scope.cancelPopularCar = function () {
        $scope.caredit = false;
        $scope.newcar = new PopularCar();
    };





});
app.controller('ProfileController', function($scope,$http,ngNotify){
    $scope.updateProfile=function(){
        $http.post('/api/updateProfile',{name:$scope.name,username:$scope.username}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Profile Changed Successfully...Please Re login to Change Profile name');
            });
    };

    $scope.changePassword=function(curpassword,newpassword,repassword){
        $http.post('/api/changePassword',{curpassword:curpassword,newpassword:newpassword,repassword:repassword}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Password Changed Successfully...Please Re login');
            });
    };
});
app.controller('SliderController', function($scope,$http,$anchorScroll,ngNotify,Slider){
    $scope.sliders=[];
    $scope.slideredit=false;
    $scope.isEdit=false;

    function loadSlider() {
        $scope.sliders=[];
        Slider.query(function (slider) {
            $scope.sliders = slider;
        });
    }
    loadSlider();

    $scope.newSlider = function (argument) {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.curSlider = {};
        $scope.isEdit=false;
    };

    $scope.addSlider = function () {

        $scope.newslider.$save(function(data){
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });
            Slider.query(function (slider) {
                $scope.sliders = slider;
            });
            ngNotify.set('Slider Saved Successfully');

            //loadSlider();
        });

        $scope.slideredit = false;
        $scope.newslider = new Slider();


    };
    $scope.deleteSlider = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.sliders.indexOf(item);
                $scope.sliders.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Removed Successfully');
            });
        }
    };
    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

});
app.controller('StatusController', function($scope,$http,$anchorScroll,$rootScope,$fancyModal,ngNotify){


    $scope.status='All';


    $scope.loadStatus=function(status){
        $http.get('/api/getVehicleStatus',{params:{status:status}}).
            success(function(data, status)
            {
                $scope.vehicles=data;
            });
    };

    $scope.loadStatus('All');

    $scope.endBooking=function(vehicle){
        var confirmDelete = confirm("Do you really need to end Booking ?");
        if (confirmDelete) {
            /*$http.post('/api/endbooking', {booking_id: vehicle.bookingid}).
                success(function (data, status) {
                    $scope.loadStatus($scope.status);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking ended Successfully');
                });*/
            $fancyModal.open({
                templateUrl: 'template/makepaymentexist',
                controller: 'MakePaymentExistModalCtrl',
                resolve: {
                    booking: function () {
                        return vehicle;
                    }
                }
            });

        }
    };

    $scope.message=function(vehicle){
        $fancyModal.open({
            templateUrl: 'template/message',
            controller: 'MessageModalCtrl',
            resolve: {
                user_id: function () {
                    return vehicle.user_id;
                }
            }
        });
    };

    $scope.makePayment=function(vehicle){
        $fancyModal.open({
            templateUrl: 'template/makepayment',
            controller: 'MakePaymentModalCtrl',
            resolve: {
                booking: function () {
                    return vehicle;
                }
            }
        });
    };

    $rootScope.$on('$fancyModal.closed', function (e, id) {
        $scope.loadStatus('All');
    });

   /* $scope.showDetails=function(booking){
        $scope.myBooking=booking;
        $scope.showList=false;
    };

    $scope.backToList=function(){
        $scope.myBooking={};
        $scope.showList=true;
    };*/

});

app.controller('MessageModalCtrl', function($scope,$http,ngNotify,$fancyModal,user_id){
    $scope.sendMessage=function(message){
        $http.post('/api/sendmessagetouser', {message:message,user_id:user_id}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});

app.controller('MakePaymentModalCtrl', function($scope,$http,ngNotify,$fancyModal,booking){
    $scope.booking=booking;

    $scope.balance=0;
    $scope.total=0;
    $scope.paid=0;

    var amount= 0,fee= 0,advance= 0,bal= 0,total=0;
    try{
        amount=parseFloat(booking.amount);
        fee=parseFloat(booking.processfee);
        advance=parseFloat(booking.advance);
        bal=(amount+fee)-advance;
        total=(amount+fee);
        $scope.balance=bal;
        $scope.total=total;
        $scope.paid=bal;
    }catch(ex) {}

    $scope.submit=function(booking,paid){
        $http.post('/api/makepayment', {id:booking.bookingid,paid:paid}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});

app.controller('MakePaymentExistModalCtrl', function($scope,$http,ngNotify,$fancyModal,booking){
    $scope.booking=booking;

    $scope.balance=0;
    $scope.total=0;
    $scope.paid=0;

    var amount= 0,fee= 0,advance= 0,bal= 0,total=0;
    try{
        amount=parseFloat(booking.amount);
        fee=parseFloat(booking.processfee);
        advance=parseFloat(booking.advance);
        bal=(amount+fee)-advance;
        total=(amount+fee);
        $scope.balance=bal;
        $scope.total=total;
        $scope.paid=bal;
    }catch(ex) {}

    $scope.submit=function(booking,paid){
        $http.post('/api/makepaymentexist', {id:booking.bookingid,paid:paid}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});
app.controller('TestimonialController', function($scope,$anchorScroll,ngNotify,Testimonial){
    $scope.testimonials=[];
    $scope.testimonialedit=false;

    Testimonial.query(function(testimonial){
        $scope.testimonials=testimonial;
    });

    $scope.newTestimonial = function (argument) {
        $scope.testimonialedit = true;
        $scope.newtestimonial = new Testimonial();
        $scope.curTestimonial = {};
    };
    $scope.editTestimonial = function (thisTestimonial) {
        $scope.testimonialedit = true;
        $scope.curTestimonial =  thisTestimonial;
        $scope.newtestimonial = angular.copy(thisTestimonial);
        $anchorScroll();
    };
    $scope.addTestimonial = function () {

        if ($scope.curTestimonial.id) {
            $scope.newtestimonial.$update(function(testimonial){
                angular.extend($scope.curTestimonial, $scope.curTestimonial, testimonial);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Updated Successfully');
            });
        } else{
            $scope.newtestimonial.$save(function(testimonial){
                $scope.testimonials.push(testimonial);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Saved Successfully');
            });
        }
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };
    $scope.deleteTestimonial = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.testimonials.indexOf(item);
                $scope.testimonials.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Testimonial Removed Successfully');
            });
        }
    };
    $scope.cancelTestimonial = function () {
        $scope.testimonialedit = false;
        $scope.newtestimonial = new Testimonial();
    };
});
app.controller('VehicleController', function($scope,$http,$anchorScroll,ngNotify,Vehicle,Brand){

    $scope.vehicles=[];
    $scope.brands=[];
    $scope.vehicleedit=false;
    $scope.showVehicle=false;

    $scope.isEdit=false;

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    Brand.query(function(brand){
        $scope.brands=brand;
    });

    $scope.myVehicle={};
    $scope.showSingleVehicle=function(vehicle){
        $scope.showVehicle=true;
        $scope.myVehicle=vehicle;
    };

    $scope.backToList=function(){
        $scope.showVehicle=false;
        $scope.myVehicle={};
    };

    $scope.newVehicle = function (argument) {
        $scope.vehicleedit = true;
        $scope.newvehicle = new Vehicle();
        $scope.curVehicle = {};
        $scope.isEdit=false;
    };

    $scope.editVehicle = function (thisVehicle) {
        $scope.vehicleedit = true;
        $scope.curVehicle =  thisVehicle;
        $scope.newvehicle = angular.copy(thisVehicle);
        $anchorScroll();
        $scope.isEdit=true;
    };

    $scope.cancelVehicle = function () {
        $scope.vehicleedit = false;
        $scope.newvehicle = new Vehicle();
    };

    $scope.deleteVehicle = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.vehicles.indexOf(item);
                $scope.vehicles.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Vehicle Removed Successfully');
            });
        }
    };

    $scope.addVehicle = function () {

        if ($scope.curVehicle.id) {
            $scope.newvehicle.$update(function(vehicle){
                angular.extend($scope.curVehicle, $scope.curVehicle, vehicle);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Vehicle Updated Successfully');
            });
        } else{
            $scope.newvehicle.$save(function(vehicle){
                $scope.vehicles.push(vehicle);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Vehicle Saved Successfully');
            });
        }
        $scope.vehicleedit = false;
        $scope.newvehicle = new Vehicle();
    };



    $scope.deletePhoto=function(item){
        var r = confirm("Are you sure want to delete this photo!");
        if (r == true) {
            $http.delete('api/file/' + item.id).
                success(function (data, status, headers, config) {
                    var curIndex = $scope.newvehicle.files.indexOf(item);
                    $scope.newvehicle.files.splice(curIndex, 1);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };





});
//# sourceMappingURL=app.js.map
