<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Testimonial | Holiday Car Rentals</title>
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>


	<!-- main stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/animate.css">



	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="js/respond.js"></script>
	<![endif]-->




</head>
<body>
<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="flat-information">
                    <li class="phone">
                        <a href="#" title="Phone number"><i>Call us:  (+91) 9947 818 815</i></a>
                    </li>
                    <li class="email">
                        <a href="#" title="Email address"><i>Email: holidayrentalcars@gmail.com</i></a>
                    </li>
                </ul>
                <div class="style-box text-right">
                    <ul class="flat-socials v1">
                        <li class="facebook">
                            <a href="#"><i class="fa fa-facebook-f"></i></a>
                        </li>
                        <li class="twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="instagram">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                    <div class="question">
                        <div onclick="javascript:location.href='contact'">
                            <i class="fa fa-question-circle-o"></i><p class="text">Have any questions ?</p>
                        </div>
                    </div>
                    <div class="box-text text-right">
                        <a href="#" data-toggle="modal" data-target=".booking-form">order now</a>
                    </div>   
                </div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>
<header class="stricky">
	<div class="navbar-width container">
		<div class="logo pull-left">
			<a href="index.html">
				<img src="img/logo.png" alt="Genurent Logo Image">
			</a>
		</div>
		<nav class="mainmenu-holder pull-right">
			<div class="nav-header">
                <ul class="navigation list-inline">
                    <li><a href="/">Home</a></li>
                    <li class="active" class="dropdown">
                        <a href="/about">About Us</a>
                        <ul class="submenu">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/testimonial">What our clients say</a></li>
                            <li><a href="/ratechart">Service Rates</a></li>
                        </ul>
                    </li>
                    <li><a href="/manual-cars">Manual Cars</a></li>
                    <li><a href="/automatic-cars">Automatic Cars</a></li>
                    <li><a href="/gallery">Our Gallery</a></li>
                    <li><a href="/contact">Contact us</a></li>
                </ul>
            </div>
			<div class="nav-footer">
				<ul class="list-inline">
					<li>
						<a href="#"><i class="icon icon-Search"></i></a>
						<ul class="search-box">
							<li>
								<form action="#">
									<input type="text" placeholder="Type and Hit Enter">
									<button type="submit"><i class="icon icon-Search"></i></button>
								</form>
							</li>
						</ul>
					</li>
					<li class="menu-expander hidden-lg hidden-md"><a href="#"><i class="icon icon-List"></i></a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>


	<section class="inner-banner inner-banner-6">
        <div class="container text-center">
            <h2><span>What our clients say</span></h2>
        </div>
    </section>

    <section class="testimonials section-padding testimonials-page">
        <div class="container">
            <div class="row">
                <?php
                    $testimonials=\App\Testimonial::all();
                ?>
                @foreach($testimonials as $testimonial)
                <div class="col-md-4 col-sm-6">
                    <div class="single-testimonials">
                        <p>“{{$testimonial->content}}”</p>
                        <div class="box">
                            <div class="img-box">
                                <img src="images/{{$testimonial->photo}}" class="img-circle" style="height: 79px;width: 79px;" alt="Awesome Image"/>
                            </div>
                            <div class="content">
                                <h3>{{$testimonial->name}}</h3>
                                <p>{{$testimonial->designation}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget about-widget">
						<div class="title">
							<h2><span>About Us</span></h2>
						</div>
						<p>We provides all types of cars that suits your tastes for monthly rental at Cochin, Calicut and Trivandrum International Airports. After Using for one or more months and you can return at Airports itself.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>Useful Links</span></h2>
                        </div>
                        <ul>
                            <li><a href="/about"><i class="fa fa-angle-right"></i> About Us</a></li>
                            <li><a href="/manual-cars"><i class="fa fa-angle-right"></i> Manual Cars</a></li>
                            <li><a href="/automatic-cars"><i class="fa fa-angle-right"></i> Automatic Cars</a></li>
                            <li><a href="/ratechart"><i class="fa fa-angle-right"></i> Service Rates</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>recent cars</span></h2>
                        </div>
                        <ul>
                        <?php
                            $recents=\App\NewestCar::with('vehicle.files')->take(4)->get();
                         ?>
                        @if(isset($recents))
                            @foreach($recents as $recent)
                               <li><a href="#"><i class="fa fa-angle-right"></i> {{$recent->vehicle->name}}</a></li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<div class="title">
							<h2><span>contact info</span></h2>
						</div>
						<ul class="contact-infos">
							<li>
								<div class="icon-box">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">
									<p>Calicut, Cochin, Trivandrum</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9947 818 815</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9562 157 211</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text">
									<p>holidayrentalcars@email.com</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<section class="bottom-bar">
		<div class="container">
			<div class="text pull-left">
				<p>Copyright <span id="year"></span> &copy; All Rights Reserved <a href="http://psybotechnologies.com/" target="_blank" style="color: #5c5c5c;">Psybo Technologies</a></p>
			</div>
			<div class="social pull-right">
				<ul class="list-inline">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</section>


	<script src="assets/jquery/jquery-1.11.3.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>

	<script src="assets/validate.js"></script>


	<!-- jQuery ui js -->
	<script src="assets/jquery-ui-1.11.4/jquery-ui.js"></script>


	<!-- mixit up -->
	<script src="assets/jquery.mixitup.min.js"></script>
	<!-- fancy box -->
	<script src="assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>



	<!-- custom.js -->
	<script src="js/default-map-script.js"></script>
	<script src="js/custom.js"></script>

    <!-- year scrpt -->
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>

</body>
</html>