<!DOCTYPE html>
<html lang="en" ng-app="myWeb">
<head>
	<meta charset="UTF-8">
	<title>Price | Holiday Car Rentals</title>
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>


	<!-- main stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/animate.css">



	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="js/respond.js"></script>
	<![endif]-->




</head>
<body ng-controller="IndexController">
<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="flat-information">
                    <li class="phone">
                        <a href="#" title="Phone number"><i>Call us:  (+91) 9947 818 815</i></a>
                    </li>
                    <li class="email">
                        <a href="#" title="Email address"><i>Email: holidayrentalcars@gmail.com</i></a>
                    </li>
                </ul>
                <div class="style-box text-right">
                    <ul class="flat-socials v1">
                        <li class="facebook">
                            <a href="#"><i class="fa fa-facebook-f"></i></a>
                        </li>
                        <li class="twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="instagram">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                    <div class="question">
                        <div onclick="javascript:location.href='contact'">
                            <i class="fa fa-question-circle-o"></i><p class="text">Have any questions ?</p>
                        </div>
                    </div>
                    <div class="box-text text-right">
                        <a href="#" data-toggle="modal" data-target=".booking-form">order now</a>
                    </div>   
                </div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>
<header class="stricky">
	<div class="navbar-width container">
		<div class="logo pull-left">
			<a href="index.html">
				<img src="img/logo.png" alt="Genurent Logo Image">
			</a>
		</div>
		<nav class="mainmenu-holder pull-right">
			<div class="nav-header">
                <ul class="navigation list-inline">
                    <li><a href="/">Home</a></li>
                    <li class="dropdown">
                        <a href="/about">About Us</a>
                        <ul class="submenu">
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/testimonial">What our clients say</a></li>
                            <li><a href="/ratechart">Service Rates</a></li>
                        </ul>
                    </li>
                    <li><a href="/manual-cars">Manual Cars</a></li>
                    <li class="active"><a href="/automatic-cars">Automatic Cars</a></li>
                    <li><a href="/gallery">Our Gallery</a></li>
                    <li><a href="/contact">Contact us</a></li>
                </ul>
            </div>
			<div class="nav-footer">
				<ul class="list-inline">
					<li>
						<a href="#"><i class="icon icon-Search"></i></a>
						<ul class="search-box">
							<li>
								<form action="#">
									<input type="text" placeholder="Type and Hit Enter">
									<button type="submit"><i class="icon icon-Search"></i></button>
								</form>
							</li>
						</ul>
					</li>
					<li class="menu-expander hidden-lg hidden-md"><a href="#"><i class="icon icon-List"></i></a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>


	<section class="inner-banner inner-banner-7">
		<div class="container text-center">
			<h2><span>Our Service Rates</span></h2>
		</div>
	</section>





	<section class="section-padding service-rate-table-wrapper">
		<div class="container">
			<div class="container">

			    <div class="genurent-rate-table-wrapper genurent-item">
			        <div class="genurent-rate-table-head-wrapper">
			            <div class="genurent-rate-table-column genurent-col-1"></div>
			            <div class="genurent-rate-table-column genurent-col-2">
			                <div class="genurent-rate-column genurent-feature">
			                    <div class="genurent-rate-table-column-inner">
			                        <div class="rate-table-title genurent-title-font">5 to 15 Days</div>
			                        <div class="rate-table-ribbon"></div>
			                    </div>
			                </div>
			                <div class="genurent-rate-column ">
			                    <div class="genurent-rate-table-column-inner">
			                        <div class="rate-table-title genurent-title-font">15 to 25 Days</div>
			                        <div class="rate-table-ribbon"></div>
			                    </div>
			                </div>
			                <div class="genurent-rate-column ">
			                    <div class="genurent-rate-table-column-inner">
			                        <div class="rate-table-title genurent-title-font">25 to 35 Days</div>
			                        <div class="rate-table-ribbon"></div>
			                    </div>
			                </div>
			                <div class="genurent-rate-column ">
			                    <div class="genurent-rate-table-column-inner">
			                        <div class="rate-table-title genurent-title-font">35 to 45 Days</div>
			                        <div class="rate-table-ribbon"></div>
			                    </div>
			                </div>
			                <div class="genurent-rate-column ">
			                    <div class="genurent-rate-table-column-inner">
			                        <div class="rate-table-title genurent-title-font">45 to 55 Days</div>
			                        <div class="rate-table-ribbon"></div>
			                    </div>
			                </div>
			                <div class="genurent-rate-column ">
			                    <div class="genurent-rate-table-column-inner">
			                        <div class="rate-table-title genurent-title-font">55 Above</div>
			                        <div class="rate-table-ribbon"></div>
			                    </div>
			                </div>
			                <div class="clear"></div>
			            </div>
			            <div class="clear"></div>
			        </div>

                    @foreach($vehicles as $vehicle)
			        <!-- one car start -->
			        <div class="genurent-rate-table-content-wrapper">
			            <div class="genurent-rate-table-content-row">
			                <div class="genurent-rate-table-column genurent-col-1">
			                    <div class="rate-table-car-image"><img src="{{url('images')}}/{{$vehicle->files->first()['name']}}" alt="" width="600" height="343">
			                    </div>
			                    <h3 class="rate-table-car-title"><a href="/showvehicle/{{$vehicle->id}}">{{$vehicle->brand->name}} {{$vehicle->name}}</a></h3>
			                </div>
			                <div class="genurent-rate-table-column genurent-col-2">
			                    <div class="genurent-rate-column genurent-feature">
			                        <div class="rate-table-price"><span>₹</span>{{$vehicle->to15}}</div>
			                    </div>
			                    <div class="genurent-rate-column ">
			                        <div class="rate-table-price"><span>₹</span>{{$vehicle->to25}}</div>
			                    </div>
			                    <div class="genurent-rate-column ">
			                        <div class="rate-table-price"><span>₹</span>{{$vehicle->to35}}</div>
			                    </div>
			                    <div class="genurent-rate-column ">
			                        <div class="rate-table-price"><span>₹</span>{{$vehicle->to45}}</div>
			                    </div>
			                    <div class="genurent-rate-column ">
			                        <div class="rate-table-price"><span>₹</span>{{$vehicle->to55}}</div>
			                    </div>
			                    <div class="genurent-rate-column ">
			                        <div class="rate-table-price"><span>₹</span>{{$vehicle->above}}</div>
			                    </div>
			                    <div class="clear"></div>
			                </div>
			                <div class="clear"></div>

			            </div>
			        </div>
			        <!-- one car end -->
                    @endforeach
			        <div class="genurent-rate-table-button-wrapper">
			            <div class="genurent-rate-table-column genurent-col-1"></div>
			            <div class="genurent-rate-table-column genurent-col-2">
			                <div  class="genurent-rate-button genurent-feature"><a  data-toggle="modal" data-target=".booking-form" class="rate-table-book-now" href="#genurent-contact-1">Book Now</a>
			                </div>
			                <div  class="genurent-rate-button "><a class="rate-table-book-now" href="#genurent-contact-2"  data-toggle="modal" data-target=".booking-form">Book Now</a>
			                </div>
			                <div  class="genurent-rate-button "><a class="rate-table-book-now" href="#genurent-contact-3"  data-toggle="modal" data-target=".booking-form">Book Now</a>
			                </div>
			                <div  class="genurent-rate-button "><a class="rate-table-book-now" href="#genurent-contact-3"  data-toggle="modal" data-target=".booking-form">Book Now</a>
			                </div>
			                <div  class="genurent-rate-button "><a class="rate-table-book-now" href="#genurent-contact-3"  data-toggle="modal" data-target=".booking-form">Book Now</a>
			                </div>
			                <div  class="genurent-rate-button "><a class="rate-table-book-now" href="#genurent-contact-3"  data-toggle="modal" data-target=".booking-form">Book Now</a>
			                </div>
			            </div>
			        </div>
			    </div>
			    <div class="clear"></div>
			    <br/>

			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget about-widget">
						<div class="title">
							<h2><span>About Us</span></h2>
						</div>
						<p>We provides all types of cars that suits your tastes for monthly rental at Cochin, Calicut and Trivandrum International Airports. After Using for one or more months and you can return at Airports itself.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>Useful Links</span></h2>
                        </div>
                        <ul>
                            <li><a href="/about"><i class="fa fa-angle-right"></i> About Us</a></li>
                            <li><a href="/manual-cars"><i class="fa fa-angle-right"></i> Manual Cars</a></li>
                            <li><a href="/automatic-cars"><i class="fa fa-angle-right"></i> Automatic Cars</a></li>
                            <li><a href="/ratechart"><i class="fa fa-angle-right"></i> Service Rates</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>recent cars</span></h2>
                        </div>
                        <ul>
                        <?php
                            $recents=\App\NewestCar::with('vehicle.files')->take(4)->get();
                         ?>
                        @if(isset($recents))
                            @foreach($recents as $recent)
                               <li><a href="#"><i class="fa fa-angle-right"></i> {{$recent->vehicle->name}}</a></li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<div class="title">
							<h2><span>contact info</span></h2>
						</div>
						<ul class="contact-infos">
							<li>
								<div class="icon-box">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">
									<p>Calicut, Cochin, Trivandrum</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9947 818 815</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9562 157 211</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text">
									<p>holidayrentalcars@email.com</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<section class="bottom-bar">
		<div class="container">
			<div class="text pull-left">
				<p>Copyright <span id="year"></span> &copy; All Rights Reserved <a href="http://psybotechnologies.com/" target="_blank" style="color: #5c5c5c;">Psybo Technologies</a></p>
			</div>
			<div class="social pull-right">
				<ul class="list-inline">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</section>


	<!-- Modal -->
    	<div  class="modal contact-page fade booking-form" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="booking-form">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3>Send message for Booking: </h3>
                        <form class="contact-form search-form-box" method="post" action="/sendbooking">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <label>Name <span>*</span></label>
                                        <input type="text" name="name" placeholder="Enter your name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <label>Email <span>*</span></label>
                                        <input type="text" name="email" placeholder="Enter your email">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <label>Phone <span>*</span></label>
                                        <input type="text" name="phone" placeholder="Enter your phone">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <?php $cars=\App\Vehicle::with('brand')->get(); ?>
                                        <label>Vehicle: <span>*</span></label>
                                        <select name="vehicle_id" ng-model="vehicle_id" id="vehicle_id" ng-model="vehicle_id" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" class="select-input" style="width: 100%;background: none;border: 2px solid #8A8989;height: 37px;outline: none;border-radius: 3px;" required="">
                                           <option value=""></option>
                                           @if(isset($cars))
                                           @foreach($cars as $car)
                                              <option value="{{$car->id}}">{{$car->brand->name}} {{$car->name}}-{{$car->transmission}}-{{$car->fuel}}</option>
                                           @endforeach
                                           @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <label>Pickup Date: <span>*</span></label>
                                        <input type="text" name="pickup_date" ng-model="pickup_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY" class="date-picker">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <label>Return Date: <span>*</span></label>
                                        <input type="text" name="return_date" ng-model="return_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY" class="date-picker">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <p class="expecte-amt">Expected Total Amount : <span><b>₹</b>@{{billamount}}</span></p>
                                        <input type="hidden" name="amount" value="@{{billamount}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-grp">
                                        <?php $locations=\App\PickupLocation::all(); ?>
                                        <label>Pickup Location:  <span>*</span></label>
                                        <select name="pickup_location_id"  id="pickup_location_id" class="select-input" style="width: 100%;background: none;border: 2px solid #8A8989;height: 37px;outline: none;border-radius: 3px;" required="">
                                            @if(isset($locations))
                                            @foreach($locations as $location)
                                            <option value="{{$location->id}}">{{$location->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="row">
                                <div class="col-md-12 clearfix">
                                    <label>Additional Note <span>*</span></label>
                                    <textarea name="message" placeholder="Enter Your message"></textarea>
                                    <button ng-href="#message" ng-click="scrollDown();" type="submit" class="pull-right thm-btn hvr-sweep-to-top">SEND MESSAGE</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




	<script src="assets/jquery/jquery-1.11.3.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>

	<script src="assets/validate.js"></script>


	<script src="assets/owl.carousel-2/owl.carousel.min.js"></script>

	<!-- jQuery ui js -->
	<script src="assets/jquery-ui-1.11.4/jquery-ui.js"></script>


	<!-- mixit up -->
	<script src="assets/jquery.mixitup.min.js"></script>
	<!-- fancy box -->
	<script src="assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>



	<!-- custom.js -->
	<script src="js/default-map-script.js"></script>
	<script src="js/custom.js"></script>

	<script src="js/angular.min.js"></script>
    <script src="js/webapp.js"></script>

    <!-- year scrpt -->
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>

</body>
</html>