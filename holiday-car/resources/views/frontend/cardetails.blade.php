<!DOCTYPE html>
<html lang="en" ng-app="myWeb">
<head>
	<meta charset="UTF-8">
	<title>Details | Holiday Car Rentals</title>
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="{{\Illuminate\Support\Facades\URL::asset('img/favicon.ico')}}"/>


	<!-- main stylesheet -->
	<link rel="stylesheet" href="{{\Illuminate\Support\Facades\URL::asset('css/style.css')}}">
	<link rel="stylesheet" href="{{\Illuminate\Support\Facades\URL::asset('css/responsive.css')}}">
    <link rel="stylesheet" href="css/animate.css">
    


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
    <script src="{{\Illuminate\Support\Facades\URL::asset('js/respond.js')}}"></script>


	<![endif]-->




</head>
<body ng-controller="IndexController">
<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="flat-information">
                    <li class="phone">
                        <a href="#" title="Phone number"><i>Call us:  (+91) 9947 818 815</i></a>
                    </li>
                    <li class="email">
                        <a href="#" title="Email address"><i>Email: holidayrentalcars@gmail.com</i></a>
                    </li>
                </ul>
                <div class="style-box text-right">
                    <ul class="flat-socials v1">
                        <li class="facebook">
                            <a href="#"><i class="fa fa-facebook-f"></i></a>
                        </li>
                        <li class="twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="instagram">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                    <div class="question">
                        <div onclick="javascript:location.href='contact'">
                            <i class="fa fa-question-circle-o"></i><p class="text">Have any questions ?</p>
                        </div>
                    </div>
                    <div class="box-text text-right">
                        <a href="#" data-toggle="modal" data-target=".booking-form">order now</a>
                    </div>   
                </div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>
<header class="stricky">
	<div class="navbar-width container">
		<div class="logo pull-left">
			<a href="/">
				<img src="{{\Illuminate\Support\Facades\URL::asset('img/logo.png')}}" alt="Genurent Logo Image">
			</a>
		</div>
		<nav class="mainmenu-holder pull-right">
			<div class="nav-header">
				<ul class="navigation list-inline">
					<li><a href="/">Home</a></li>
					<li class="dropdown">
						<a href="/about">About Us</a>
						<ul class="submenu">
							<li><a href="/about">About Us</a></li>
							<li><a href="/testimonial">What our clients say</a></li>
							<li><a href="/ratechart">Service Rates</a></li>
						</ul>
					</li>
					<li><a href="/manual-cars">Manual Cars</a></li>
					<li class="active"><a href="/automatic-cars">Automatic Cars</a></li>
					<li><a href="/gallery">Our Gallery</a></li>
					<li><a href="/contact">Contact us</a></li>
				</ul>
			</div>
			<div class="nav-footer">
				<ul class="list-inline">
					<li>
						<a href="#"><i class="icon icon-Search"></i></a>
						<ul class="search-box">
							<li>
								<form action="#">
									<input type="text" placeholder="Type and Hit Enter">
									<button type="submit"><i class="icon icon-Search"></i></button>
								</form>
							</li>
						</ul>
					</li>
					<li class="menu-expander hidden-lg hidden-md"><a href="#"><i class="icon icon-List"></i></a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>


	<section class="inner-banner inner-banner-2">
		<div class="container text-center">
			<h2><span>{{$mycar->name}}</span></h2>
		</div>
	</section>


	<section class="vehicle-sorter-area section-padding col-3-page">
		<div class="container">
			<div class="row">
                <div class="col-md-7">
                    <div class="single-car-carousel-content-box owl-carousel owl-theme">
                    @if(isset($mycar->files))
                        @foreach($mycar->files as $file)
                        <div class="item">
                            <img src="{{url('images')}}/{{$file->name}}" />
                        </div>
                        @endforeach
                    @endif
                    </div>
                    <div class="single-car-carousel-thumbnail-box owl-carousel owl-theme">
                        @if(isset($mycar->files))
                            @foreach($mycar->files as $file)
                            <div class="item">
                                <img src="{{url('images')}}/{{$file->name}}"/>
                            </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="single-vehicle-sorter">
                        <h3>{{$mycar->brand->name}} {{$mycar->name}}</h3>

                        <div class="middle-box-wrapper clearfix">
                            <div class="middle-box">
                                <ul>
                                   <li><span>Model Year</span><strong>:</strong> {{$mycar->modelyear}}</li>
                                   <li><span>Number of Gear</span><strong>:</strong> {{$mycar->noofgear}}</li>
                                   <li><span>Fuel</span><strong>:</strong> {{$mycar->fuel}}</li>
                                   <li><span>Mileage</span><strong>:</strong> {{$mycar->mileage}}</li>
                                   <li><span>Air Bag</span><strong>:</strong> @if($mycar->airbag=='1') YES @else NO @endif</li>
                                   <li><span>No.of Air Bag</span><strong>:</strong> {{$mycar->no_airbag}}</li>

                                   <li><span>Transmission</span><strong>:</strong> {{$mycar->transmission}}</li>
                                   <li><span>Engine</span><strong>:</strong> {{$mycar->engine}}</li>
                                   <li><span>Displacement</span><strong>:</strong>  {{$mycar->displacement}}</li>
                                   <li><span>BHP</span><strong>:</strong> {{$mycar->bhp}}</li>
                                   <li><span>Torque</span><strong>:</strong> {{$mycar->torque}}</li>
                                </ul>
                            </div>
                        </div>








                        <div class="bottom-box-wrapper clearfix" style="visibility: hidden;">
                            <p class="price-box hour pull-left">
                                Price per Month:
                                <span>₹&nbsp;{{$mycar->to35}}</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-vehicle-sorter">
                        <div class="middle-box-wrapper clearfix">
                            <div class="middle-box">
                                <ul>
                                   <li><span>Seat Availability</span><strong>:</strong> {{$mycar->seat}}</li>
                                   <li><span>Center Lock</span><strong>:</strong> @if($mycar->center_lock=='1') YES @else NO @endif</li>
                                   <li><span>Audio System</span><strong>:</strong> @if($mycar->audio_system=='1') YES @else NO @endif</li>
                                   <li><span>Video</span><strong>:</strong> @if($mycar->video=='1') YES @else NO @endif</li>
                                   <li><span>GPS</span><strong>:</strong> @if($mycar->gps=='1') YES @else NO @endif</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-vehicle-sorter">
                        <div class="middle-box-wrapper clearfix">
                            <div class="middle-box">
                                <ul>
                                    <li><span>5 TO 15 Days</span><strong>:</strong> &nbsp;₹&nbsp;{{$mycar->to15}}</li>
                                    <li><span>15 TO 25 Days</span><strong>:</strong> &nbsp;₹&nbsp;{{$mycar->to25}}</li>
                                    <li><span>25 TO 35 Days</span><strong>:</strong> &nbsp;₹&nbsp;{{$mycar->to35}}</li>
                                    <li><span>35 TO 45 Days</span><strong>:</strong> &nbsp;₹&nbsp;{{$mycar->to45}}</li>
                                    <li><span>45 TO 55 Days</span><strong>:</strong> &nbsp;₹&nbsp;{{$mycar->to55}}</li>
                                    <li><span>55 and Above</span><strong>:</strong> &nbsp;₹&nbsp;{{$mycar->above}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-vehicle-sorter">
                        <div class="middle-box-wrapper clearfix">
                            <div class="bottom-box-wrapper clearfix">
                                <p class="price-box hour pull-left">
                                    Price per Month:
                                    <span>₹&nbsp;{{$mycar->to35}}</span>
                                </p>
                            </div>
                        </div>
                        <a  ng-click="selectCar({{$mycar->id}});" data-toggle="modal" data-target=".booking-form" href="#"  class="thm-btn book-btn" style="margin-top: 61px;margin-left: 0px;padding: 15px;">Order Now</a
                    </div>
                </div>
            </div>
		</div>
	</section>



	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget about-widget">
						<div class="title">
							<h2><span>About Us</span></h2>
						</div>
						<p>We provides all types of cars that suits your tastes for monthly rental at Cochin, Calicut and Trivandrum International Airports. After Using for one or more months and you can return at Airports itself.</p>
					</div>
				</div>				
				<div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>Useful Links</span></h2>
                        </div>
                        <ul>
                            <li><a href="/about"><i class="fa fa-angle-right"></i> About Us</a></li>
                            <li><a href="/manual-cars"><i class="fa fa-angle-right"></i> Manual Cars</a></li>
                            <li><a href="/automatic-cars"><i class="fa fa-angle-right"></i> Automatic Cars</a></li>
                            <li><a href="/ratechart"><i class="fa fa-angle-right"></i> Service Rates</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>recent cars</span></h2>
                        </div>
                        <ul>

                        </ul>
                    </div>
                </div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<div class="title">
							<h2><span>contact info</span></h2>
						</div>
						<ul class="contact-infos">
							<li>
								<div class="icon-box">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">
									<p>Calicut, Cochin, Trivandrum</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9947 818 815</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9562 157 211</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text">
									<p>holidayrentalcars@gmail.com</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<section class="bottom-bar">
		<div class="container">
			<div class="text pull-left">
				<p>Copyright <span id="year"></span> &copy; All Rights Reserved <a href="http://psybotechnologies.com/" target="_blank" style="color: #5c5c5c;">Psybo Technologies</a></p>
			</div>
			<div class="social pull-right">
				<ul class="list-inline">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</section>




	<!-- Modal -->
	<div  class="modal contact-page fade booking-form" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="booking-form">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3>Send message for Booking: </h3>
                    <form class="contact-form search-form-box" method="post" action="/sendbooking">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <label>Name <span>*</span></label>
                                    <input type="text" name="name" placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <label>Email <span>*</span></label>
                                    <input type="text" name="email" placeholder="Enter your email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <label>Phone <span>*</span></label>
                                    <input type="text" name="phone" placeholder="Enter your phone">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <?php $vehicles=\App\Vehicle::with('brand')->get(); ?>
                                    <label>Vehicle: <span>*</span></label>
                                    <select name="vehicle_id" ng-model="vehicle_id" id="vehicle_id" ng-model="vehicle_id" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" class="select-input" style="width: 100%;background: none;border: 2px solid #8A8989;height: 37px;outline: none;border-radius: 3px;" required="">
                                       @if(isset($vehicles))
                                       @foreach($vehicles as $vehicle)
                                          <option value="{{$vehicle->id}}">{{$vehicle->brand->name}} {{$vehicle->name}}-{{$vehicle->transmission}}-{{$vehicle->fuel}}</option>
                                       @endforeach
                                       @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <label>Pickup Date: <span>*</span></label>
                                    <input type="text" name="pickup_date" ng-model="pickup_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY" class="date-picker">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <label>Return Date: <span>*</span></label>
                                    <input type="text" name="return_date" ng-model="return_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY" class="date-picker">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <p class="expecte-amt">Expected Total Amount : <span><b>₹</b>@{{billamount}}</span></p>
                                    <input type="hidden" name="amount" value="@{{billamount}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-grp">
                                    <?php $locations=\App\PickupLocation::all(); ?>
                                    <label>Pickup Location:  <span>*</span></label>
                                    <select name="pickup_location_id"  id="pickup_location_id" class="select-input" style="width: 100%;background: none;border: 2px solid #8A8989;height: 37px;outline: none;border-radius: 3px;" required="">
                                        <option value=""></option>
                                        @if(isset($locations))
                                        @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row">
                            <div class="col-md-12 clearfix">
                                <label>Additional Note <span>*</span></label>
                                <textarea name="message" placeholder="Enter Your message"></textarea>
                                <button type="submit" class="pull-right thm-btn hvr-sweep-to-top">SEND MESSAGE</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
		
	


	    <script src="{{\Illuminate\Support\Facades\URL::asset('assets/jquery/jquery-1.11.3.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>

    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/validate.js')}}"></script>

    	<!-- Revolution slider JS -->
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>

    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/owl.carousel-2/owl.carousel.min.js')}}"></script>

    	<!-- jQuery ui js -->
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/jquery-ui-1.11.4/jquery-ui.js')}}"></script>


    	<!-- mixit up -->
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/jquery.mixitup.min.js')}}"></script>
    	<!-- fancy box -->
    	<script src="{{\Illuminate\Support\Facades\URL::asset('assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js')}}"></script>



    	<!-- custom.js -->

    	<script src="{{\Illuminate\Support\Facades\URL::asset('js/default-map-script.js')}}"></script>
    	<script src="{{\Illuminate\Support\Facades\URL::asset('js/custom.js')}}"></script>

    	<script src="{{\Illuminate\Support\Facades\URL::asset('js/angular.min.js')}}"></script>
        <script src="{{\Illuminate\Support\Facades\URL::asset('js/webapp.js')}}"></script>

    <!-- year scrpt -->
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>

</body>
</html>
