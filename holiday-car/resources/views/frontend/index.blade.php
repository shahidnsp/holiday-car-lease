<!DOCTYPE html>
<html lang="en" ng-app="myWeb">
<head>
	<meta charset="UTF-8">
	<title>Home | Holiday Car Rentals</title>
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Rent a Car in Kerala - Holiday Rent a Car in Kerala provides the best rent car services in Kerala. Our rental services is covered every states in Kerala.." />
	<meta name="keywords" content="rent a car in kerala,rent car in kerala,rent car kerala,best rent car kerala,car hire in kerala,book rent car kerala,best rent a cars in kerala."/>
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>


	<!-- main stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/animate.css">

    <script src="assets/jquery/jquery-1.11.3.min.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="js/respond.js"></script>
	<![endif]-->


</head>
<body ng-controller="IndexController">

<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="flat-information">
                    <li class="phone">
                        <a href="#" title="Phone number"><i>Call us:  (+91) 9947 818 815</i></a>
                    </li>
                    <li class="email">
                        <a href="#" title="Email address"><i>Email: holidayrentalcars@gmail.com</i></a>
                    </li>
                </ul>
                <div class="style-box text-right">
                    <ul class="flat-socials v1">
                        <li class="facebook">
                            <a href="https://www.facebook.com/holidayrentcars/" target="_blank"><i class="fa fa-facebook-f"></i></a>
                        </li>
                        <li class="twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="instagram">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                    <div class="question">
                        <div onclick="javascript:location.href='contact'">
                            <i class="fa fa-question-circle-o"></i><p class="text">Have any questions ?</p>
                        </div>
                    </div>
                    <div class="box-text text-right">
                        <a href="#" data-toggle="modal" data-target=".booking-form">order now</a>
                    </div>   
                </div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>


<header class="stricky">
	<div class="navbar-width container">
		<div class="logo pull-left">
			<a href="index.html">
				<img src="img/logo.png" alt="Genurent Logo Image">
			</a>
		</div>
		<nav class="mainmenu-holder pull-right">
			<div class="nav-header">
				<ul class="navigation list-inline">
					<li class="active"><a href="/">Home</a></li>
					<li class="dropdown">
						<a href="/about">About Us</a>
						<ul class="submenu">
							<li><a href="/about">About Us</a></li>
							<li><a href="/testimonial">What our clients say</a></li>
							<li><a href="/ratechart">Service Rates</a></li>
						</ul>
					</li>
					<li><a href="/manual-cars">Manual Cars</a></li>
					<li><a href="/automatic-cars">Automatic Cars</a></li>
					<li><a href="/gallery">Our Gallery</a></li>
					<li><a href="/contact">Contact us</a></li>
				</ul>
			</div>
			<div class="nav-footer">
				<ul class="list-inline">
					<li>
						<a href="#"><i class="icon icon-Search"></i></a>
						<ul class="search-box">
							<li>
								<form action="#">
									<input type="text" placeholder="Type and Hit Enter">
									<button type="submit"><i class="icon icon-Search"></i></button>
								</form>
							</li>
						</ul>
					</li>
					<li class="menu-expander hidden-lg hidden-md"><a href="#" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>

	
	<section class="rev_slider_wrapper">
		<div id="slider1" class="rev_slider"  data-version="5.0">
			<ul>
			    <?php $sliders=\App\Slider::all(); ?>
			    @foreach($sliders as $slider)
                    <li data-transition="slidingoverlayleft">
                        <img src="images/{{$slider->photo}}"  alt="" width="1280" height="936" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" >
                        <div class="tp-caption sfr tp-resizeme caption-h1"
                            data-x="left" data-hoffset="0"
                            data-y="top" data-voffset="325"
                            data-whitespace="nowrap"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="1000">
                        </div>
                    </li>
				@endforeach
				{{--<li data-transition="slidingoverlayleft">
					<img src="img/slider2.jpg"  alt=""  width="1280" height="936" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" >
					<div class="tp-caption sfr tp-resizeme caption-h1" 
				        data-x="left" data-hoffset="0" 
				        data-y="top" data-voffset="325" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1000">
				    </div>
				</li>
				<li data-transition="slidingoverlayleft">
					<img src="img/slider3.jpg"  alt=""  width="1280" height="936" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3" >
					<div class="tp-caption sfr tp-resizeme caption-h1" 
				        data-x="left" data-hoffset="0" 
				        data-y="top" data-voffset="325" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1000">
				    </div>
				</li>
				<li data-transition="slidingoverlayleft">
					<img src="img/slider4.jpg"  alt=""  width="1280" height="936" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="4" >
					<div class="tp-caption sfr tp-resizeme caption-h1" 
				        data-x="left" data-hoffset="0" 
				        data-y="top" data-voffset="325" 
				        data-whitespace="nowrap"
				        data-transform_idle="o:1;" 
				        data-transform_in="o:0" 
				        data-transform_out="o:0" 
				        data-start="1000">
				    </div>
				</li>--}}
			</ul>
		</div>
	</section>

	<section class="team-wrapper team-page section-padding">
		<div class="container">
			<div class="section-title text-center mb0">
				<h2><span>get fast & reliable</span></h2>
				<small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-lightbulb-o"></i>
                    <span>&nbsp;</span>
                </small>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 wow fadeInLeft animated" data-wow-duration="0.5s" data-wow-delay="2.4s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 2.4s; animation-name: fadeInLeft;">
					<div class="single-team-member">
						<div class="img-box">
							<img src="img/Untitled-1.png" alt="Awesome Image"/>
						</div>
						<div class="content-box">
							<h3>Vehicles On The Demands</h3>
							<p>Choose the time,location and your car and you'll get a quality rental car on your door step! We guarentee you'll satisfied our services</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 wow fadeInLeft animated" data-wow-duration="0.5s" data-wow-delay="2.0s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 2.0s; animation-name: fadeInLeft;">
					<div class="single-team-member">
						<div class="img-box">
							<img src="img/airportt.png" alt="Awesome Image"/>
						</div>
						<div class="content-box">
							<h3>Airport Transfers & Receive</h3>
							<p>Choose the time, location and car-type and you'll get a quality rental car for Airport transfer or receive! We guarantee you'll satisfied our services.</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 wow fadeInRight animated" data-wow-duration="0.5s" data-wow-delay="2.0s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 2.0s; animation-name: fadeInRight;">
					<div class="single-team-member">
						<div class="img-box">
							<img src="img/tag.png" alt="Awesome Image"/>
						</div>
						<div class="content-box">
							<h3>Selected Extras</h3>
							<p>We provide tour & travel services Pan India basis. Book tour packages, taxi for local sightseeing or for one or more day visit on very competitive price.</p>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 wow fadeInRight animated" data-wow-duration="0.5s" data-wow-delay="2.4s" style="visibility: visible; animation-duration: 0.5s; animation-delay: 2.4s; animation-name: fadeInRight;">
					<div class="single-team-member">
						<div class="img-box">
							<img src="img/bulb.png" alt="Awesome Image"/>
						</div>
						<div class="content-box">
							<h3>Fast and Efficient</h3>
							<p>We provide value for money services. We have many satisfied clients, who took our travel services and reccommending our company to their friends.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="vehicle-sorter-area section-padding">
		<div class="container">
			<div class="section-title text-center">
				<h2><span>OUR NEWEST VEHICLES</span></h2>
				<small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-plus"></i>
                    <span>&nbsp;</span>
                </small>
			</div>
			<div class="vehicle-sorter-wrapper mix-it-gallery">
				<div class="row">
				@if(isset($newstvehicles))
                	@foreach($newstvehicles as $index => $newstvehicle)
                        <div class="col-md-3 col-sm-3 mix home-car-grid wow fadeInLeft animated" data-wow-duration="0.5s" data-wow-delay="{{2+($index*0.4)}}s" style="visibility: visible; animation-duration: 0.5s; animation-delay: {{2+($index*0.4)}}s; animation-name: fadeInLeft;">
                            <div class="single-vehicle-sorter">
                                <div class="img-box car-img">
                                    @if(isset($newstvehicle->vehicle->files[0]))
                                        <a href="/showvehicle/{{$newstvehicle->vehicle->id}}"><img src="images/{{$newstvehicle->vehicle->files[0]->name}}" alt=""></a>
                                    @else
                                        <a href="/showvehicle/{{$newstvehicle->vehicle->id}}"><img src="img/car.png" alt=""></a>
                                    @endif
                                    <div class="tag-fuel"><span>{{$newstvehicle->vehicle->fuel}}</span></div>
                                    <div class="tag-price"><span><i class="fa fa-inr"></i>{{$newstvehicle->vehicle->to35}}</span></div>
                                </div>
                                <a href="/showvehicle/{{$newstvehicle->vehicle->id}}"><h3>{{$newstvehicle->vehicle->brand->name}} {{$newstvehicle->vehicle->name}}</h3></a>
                                <div class="main-box-wrapper clearfix">
                                    <div class="main-box hom-box">
                                        <ul>
                                        	<li><span><i class="flaticon-gasoline-pump"></i></span>{{$newstvehicle->vehicle->fuel}}</li>
                                        	<li><span><i class="flaticon-automatic-flash-symbol"></i></span>{{$newstvehicle->vehicle->transmission}}</li>
                                        </ul>
                                    </div>
                                    <div class="main-box hom-box">
                                        <ul>
                                        	<li><span><i class="flaticon-road-with-broken-line"></i></span>{!! str_limit($newstvehicle->vehicle->mileage, $limit = 9, $end = '') !!}</li>
                                        	<li><span><i class="fa fa-users"></i></span>{{$newstvehicle->vehicle->seat}} Seats</li>
                                        </ul>
                                    </div>
                                    <div class="main-box hom-box">
                                    	<ul>
                                    		<li><span><i class="flaticon-transport"></i></span>{{$newstvehicle->vehicle->noofgear}} Gears</li>
                                        	<li><span><i class="flaticon-time"></i></span>{{$newstvehicle->vehicle->modelyear}}</li>
                                    	</ul>
                                    </div>
                                </div>
                                <div class="bottom-box-wrapper clearfix">
                                    <a  ng-click="selectCar({{$newstvehicle->vehicle->id}});" data-toggle="modal" data-target=".booking-form" href="#" class="thm-btn">book now</a>
                                </div>
                            </div>
                        </div>
					@endforeach
				@endif
				</div>
			</div>
		</div>
	</section>

	<section class="welcome-section section-padding">
		<div class="container">
			<div class="section-title text-center">
				<h2><span>who we are</span></h2>
				<small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-question"></i>
                    <span>&nbsp;</span>
                </small>
				<p>Holiday rent a car  is a professionally managed passenger car rental service. Launched 6 years ago as a company providing cars to passengers in Calicut, Cochin, Trivandrum aireports and, and also Kerala cities. As a pioneer in the car rental services Kerala, we set a goal to continue to offer our customers the lowest prices, while improving quality of service.We have got special expertise in tailor-made holidays in India.</p>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<div class="single-welcome">
						<div class="top-box">
							<img src="img/home1.png" alt="">
							<div class="content-box">
								<h3>About Us</h3>
							</div>
						</div>
						<p>We provides all types of cars that suits your tastes for monthly rental at Cochin, Calicut and Trivandrum International Airports. After Using for one or more months and you can return at Airports itself.<br />
						Are you looking for Kerala rent car or rental cars Kerala deals ?<br />
						Or do you want to rent a luxury rent car in Kerala for your Holiday Trips ?</p>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="single-welcome">
						<div class="top-box">
							<img src="img/home1.jpg" alt="">
							<div class="content-box">
								<h3>Frequently asked questions</h3>
							</div>
						</div>
						<div class="accrodion-grp" data-grp-name="faq-accrodion">
							<div class="accrodion ">
								<div class="accrodion-title">
									<h4>May i book a cab or car online ?</h4>
								</div>
								<div class="accrodion-content">
									<p>Yes, you can book a cab or car online. Or You Can Order in Your Android Phone Using Our Application</p>
								</div>
							</div>
							<div class="accrodion active">
								<div class="accrodion-title">
									<h4>Which type of cab you provide ?</h4>
								</div>
								<div class="accrodion-content">
									<p>We provide all type of vehicle packages for all budgets from economical to luxurious.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="testimonials section-padding testimonials-carousel">
		<div class="container">
			<div class="section-title text-center">
				<h2><span>Our Success Stories</span></h2>
				<small class="heading heading-double-icon center-block">
                    <span>&nbsp;</span>
                    <i class="fa fa-comments"></i>
                    <span>&nbsp;</span>
                </small>
			</div>
			<div class="owl-carousel owl-theme">
			@if(isset($testimonials))
			    @foreach($testimonials as $testimonial)
				<div class="item itembid">
					<div class="single-testimonials">
						<p>“{{$testimonial->content}}”</p>
						<div class="box">
							<div class="img-box">
								<img src="images/{{$testimonial->photo}}" class="img-circle" style="height: 79px;width: 79px;" alt="Awesome Image"/>
							</div>
							<div class="content">
								<h3>{{$testimonial->name}}</h3>
								<p>{{$testimonial->designation}}</p>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			@endif
			</div>
		</div>
	</section>

	<section class="call-to-action">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
				</div>
				<div class="col-md-5">
					<div class="icon-box pull-left">
						<div class="box" style="cursor: pointer;">
							<img src="img/app.png" alt="Awesome Image" title="Click and install"/>
						</div>
					</div>
					<div class="icon-box pull-left">
						<div class="box" style="cursor: pointer;" onclick="window.open('https://play.google.com/store/apps/details?id=com.psybo.holiday.holidaycarlease','new_window');">
							<img src="img/play.png" alt="Awesome Image" title="Click and install"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="client-carousel">
		<div class="container">
			<div class="owl-carousel owl-theme">
				<div class="item">
					<img src="img/carlogo/1.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/2.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/3.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/4.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/5.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/6.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/7.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/8.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/9.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/10.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/11.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/12.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/13.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/14.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/15.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/16.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/17.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/18.png" alt="Awesome Image"/>
				</div>
				<div class="item">
					<img src="img/carlogo/19.png" alt="Awesome Image"/>
				</div>
			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget about-widget">
						<div class="title">
							<h2><span>About Us</span></h2>
						</div>
						<p>We provides all types of cars that suits your tastes for monthly rental at Cochin, Calicut and Trivandrum International Airports. After Using for one or more months and you can return at Airports itself.</p>
					</div>
				</div>				
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget post-widget">
						<div class="title">
							<h2><span>Useful Links</span></h2>
						</div>
						<ul>
							<li><a href="/about"><i class="fa fa-angle-right"></i> About Us</a></li>
							<li><a href="/manual-cars"><i class="fa fa-angle-right"></i> Manual Cars</a></li>
							<li><a href="/automatic-cars"><i class="fa fa-angle-right"></i> Automatic Cars</a></li>
							<li><a href="/ratechart"><i class="fa fa-angle-right"></i> Service Rates</a></li>
						</ul>
					</div>									
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget post-widget">
						<div class="title">
							<h2><span>recent cars</span></h2>
						</div>
						<ul>
						<?php
						    $recents=\App\NewestCar::with('vehicle.files')->take(4)->get();
						 ?>
						@if(isset($recents))
						    @foreach($recents as $recent)
							   <li><a href="#"><i class="fa fa-angle-right"></i> {{$recent->vehicle->name}}</a></li>
							@endforeach
						@endif
						</ul>
					</div>									
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<div class="title">
							<h2><span>contact info</span></h2>
						</div>
						<ul class="contact-infos">
							<li>
								<div class="icon-box">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">
									<p>Calicut, Cochin, Trivandrum</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9947 818 815</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9562 157 211</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text">
									<p>holidayrentalcars@gmail.com</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<section class="bottom-bar">
		<div class="container">
			<div class="text pull-left">
				<p>Copyright <span id="year"></span> &copy; All Rights Reserved <a href="http://psybotechnologies.com/" target="_blank" style="color: #5c5c5c;">Psybo Technologies</a></p>
			</div>
			<div class="social pull-right">
				<ul class="list-inline">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</section>




	<!-- Modal -->
	<div  class="modal contact-page fade booking-form" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="booking-form">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3>Send message for Booking: </h3>
					<form class="contact-form search-form-box" method="post" action="/sendbooking">
					    {!! csrf_field() !!}
						<div class="row">
							<div class="col-md-6">
								<div class="form-grp">
									<label>Name <span>*</span></label>
									<input type="text" name="name" placeholder="Enter your name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Email <span>*</span></label>
									<input type="text" name="email" placeholder="Enter your email">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-grp">
									<label>Phone <span>*</span></label>
									<input type="text" name="phone" placeholder="Enter your phone">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Vehicle: <span>*</span></label>
									<?php $cars=\App\Vehicle::with('brand')->get(); ?>
									<select name="vehicle_id" ng-model="vehicle_id" id="vehicle_id" ng-model="vehicle_id" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" class="select-input" style="width: 100%;background: none;border: 2px solid #8A8989;height: 37px;outline: none;border-radius: 3px;">
                                        <option value=""></option>
                                        @if(isset($cars))
                                        @foreach($cars as $car)
										<option value="{{$car->id}}">{{$car->brand->name}} {{$car->name}}-{{$car->transmission}}-{{$car->fuel}}</option>
										@endforeach
										@endif
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-grp">
									<label>Pickup Date: <span>*</span></label>
									<input type="text" name="pickup_date" ng-model="pickup_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY" class="date-picker">
									{{--<input id="datetimepicker1" type="text" name="pickup_date" ng-model="pickup_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY">--}}
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-grp">
									<label>Return Date: <span>*</span></label>
									<input type="text" name="return_date" ng-model="return_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY" class="date-picker">
									{{--<input id="datetimepicker2" type="text" name="return_date" ng-model="return_date" ng-change="getVehicleAmount(pickup_date,return_date,vehicle_id);" placeholder="MM/DD/YYYY">--}}
								</div>
							</div>

						</div>
						<div class="row">
						    <div class="col-md-6">
						        <div class="form-grp">
						            <p class="expecte-amt">Expected Total Amount : <span><b>₹</b>@{{billamount}}</span></p>
						            <input type="hidden" name="amount" value="@{{billamount}}">
						        </div>
						    </div>
							<div class="col-md-6">
                                <div class="form-grp">
                                    <?php $locations=\App\PickupLocation::all(); ?>
                                    <label>Pickup Location:  <span>*</span></label>
                                    <select name="pickup_location_id"  id="pickup_location_id" class="select-input" style="width: 100%;background: none;border: 2px solid #8A8989;height: 37px;outline: none;border-radius: 3px;">
                                        @if(isset($locations))
                                        @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
						<div class="clear"></div>
                        <div class="row">
							<div class="col-md-12 clearfix">
								<label>Additional Note <span>*</span></label>
								<textarea name="message" placeholder="Enter Your message"></textarea>
								<button type="submit" class="pull-right thm-btn hvr-sweep-to-top">SEND MESSAGE</button>

							</div>
						</div>

            <input mdc-datetime-picker date="false" time="true" type="text" id="time" short-time="true"
               show-todays-date click-outside-to-close="true"
               placeholder="Time" auto-ok="true"
               min-date="minDate" minute-steps="1"
               format="hh:mm a"
               ng-change="vm.saveChange()"
               ng-model="time">
					</form>
				</div>
			</div>
		</div>
	</div>

	<div id="myModal" class="modal fade" role="dialog" style="top: 150px;">
	  <div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Connect With Holida Car Lease</h4>
	      </div>
	      <div class="modal-body">
	        <div class="fb-page" data-href="https://www.facebook.com/holidayrentcars/" data-tabs="timeline" data-width="1200" data-height="450" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/holidayrentcars/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/holidayrentcars/">Holiday CAR Rentals</a></blockquote></div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		
	



	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/validate.js"></script>

	<!-- Revolution slider JS -->
	<script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script> 
	<script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
	<script src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>

	<script src="assets/owl.carousel-2/owl.carousel.min.js"></script>

	<!-- jQuery ui js -->
	<script src="assets/jquery-ui-1.11.4/jquery-ui.js"></script>


	<!-- mixit up -->
	<script src="assets/jquery.mixitup.min.js"></script>
	<!-- fancy box -->
	<script src="assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>




	<!-- custom.js -->
	<script src="js/default-map-script.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/angular.min.js"></script>
	<script src="js/webapp.js"></script>

    <!-- year scrpt -->
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>

    <script>
	function openNav() {
	    document.getElementById("mySidenav").style.width = "250px";
	}

	function closeNav() {
	    document.getElementById("mySidenav").style.width = "0";
	}
    document.getElementById("vehicle_id").selectedIndex = 2;

    function SelectCar(inVal)
    {
        document.getElementById('vehicle_id').value = inVal;
    }

     $(window).load(function(){        
	   $('#myModal').modal('show');
	  }); 
	</script>


</body>
</html>