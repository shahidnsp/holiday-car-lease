
<?php $myVehicle=\App\Vehicle::find($vehicle_id); ?>
<section id="message" class="vehicle-sorter-area section-padding">
    <div class="container text-center col-sm-12">
       <div class="alert alert-success">
         <h2><strong>Thanks for Your Booking!</strong> We contact you shortly.</h2>
       </div>
    </div>
    {{--<div class="panel panel-primary">
        <div class="col-md-12">
            <h2 class="panel-heading text-success">
                {{$myVehicle->name}} <small class="text-primary">Brand: {{$myVehicle->brand->name}}</small>
            </h2>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="table-responsive panel-body">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td>Transmission:</td><td>{{$myVehicle->transmission}}</td>
                            </tr>
                            <tr>
                                <td>Displacement:</td><td>{{$myVehicle->displacement}}</td>
                            </tr>
                            <tr>
                                <td>Fuel:</td><td>{{$myVehicle->fuel}}</td>
                            </tr>
                            <tr>
                                <td>Center Lock:</td><td>@if($myVehicle->center_lock=='1')<span class="text-primary">Yes</span>@endif @if($myVehicle->center_lock=='0')<span class="text-success">No</span>@endif</td>
                            </tr>
                            <tr>
                                <td>Video System:</td><td>@if($myVehicle->video=='1')<span class="text-primary">Yes</span>@endif @if($myVehicle->video=='0')<span class="text-success">No</span>@endif</td>
                            </tr>
                            <tr>
                                <td>Air Bag:</td><td>@if($myVehicle->airbag=='1')<span class="text-primary">Yes | {{$myVehicle->no_airbag}} Nons</span>@endif @if($myVehicle->airbag=='0')<span class="text-success">No</span>@endif</td>
                            </tr>
                            <tr class="text-danger">
                                <td>Price Per Day:</td><td>{{$myVehicle->price_per_day}}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="table-responsive panel-body">
                        <table class="table table-striped table-bordered table-hover">
                            <tr>
                                <td>BHP:</td><td>{{$myVehicle->bhp}}</td>
                            </tr>
                            <tr>
                                <td>Mileage:</td><td>{{$myVehicle->mileage}}</td>
                            </tr>
                            <tr>
                                <td>Torque:</td><td>{{$myVehicle->torque}}</td>
                            </tr>
                            <tr>
                                <td>No.of Seat:</td><td>{{$myVehicle->seat}}</td>
                            </tr>
                            <tr>
                                <td>Engine:</td><td>{{$myVehicle->engine}}</td>
                            </tr>
                            <tr>
                                <td>Audio System:</td><td>@if($myVehicle->audio_system=='1')<span class="text-primary">Yes</span>@endif @if($myVehicle->audio_system=='0')<span class="text-success">No</span>@endif</td>
                            </tr>
                            <tr>
                                <td>GPS Control:</td><td>@if($myVehicle->gps=='1')<span class="text-primary">Yes</span>@endif @if($myVehicle->gps=='0')<span ng-if="myVehicle.gps=='0'" class="text-success">No</span>@endif</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>--}}
</section>



