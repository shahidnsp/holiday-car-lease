<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>contact | Holiday Car Rentals</title>
	<!-- mobile responsive meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>


	<!-- main stylesheet -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">
	<link rel="stylesheet" href="css/animate.css">


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
		<script src="js/respond.js"></script>
	<![endif]-->

</head>
<body>
<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="flat-information">
                    <li class="phone">
                        <a href="#" title="Phone number"><i>Call us:  (+91) 9947 818 815</i></a>
                    </li>
                    <li class="email">
                        <a href="#" title="Email address"><i>Email: holidayrentalcars@gmail.com</i></a>
                    </li>
                </ul>
                <div class="style-box text-right">
                    <ul class="flat-socials v1">
                        <li class="facebook">
                            <a href="#"><i class="fa fa-facebook-f"></i></a>
                        </li>
                        <li class="twitter">
                            <a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li class="instagram">
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </li>
                    </ul>
                    <div class="question">
                        <div onclick="javascript:location.href='contact'">
                            <i class="fa fa-question-circle-o"></i><p class="text">Have any questions ?</p>
                        </div>
                    </div>
                    <div class="box-text text-right">
                        <a href="#" data-toggle="modal" data-target=".booking-form">order now</a>
                    </div>   
                </div>
            </div><!-- col-md-12 -->
        </div><!-- row -->
    </div><!-- container -->
</div>
<header class="stricky">
	<div class="navbar-width container">
		<div class="logo pull-left">
			<a href="index.html">
				<img src="img/logo.png" alt="Genurent Logo Image">
			</a>
		</div>
		<nav class="mainmenu-holder pull-right">
			<div class="nav-header">
				<ul class="navigation list-inline">
					<li><a href="/">Home</a></li>
					<li class="dropdown">
						<a href="/about">About Us</a>
						<ul class="submenu">
							<li><a href="/about">About Us</a></li>
							<li><a href="/testimonial">Testimonials</a></li>
							<li><a href="/ratechart">Service Rates</a></li>
						</ul>
					</li>
					<li><a href="/manual-cars">Manual Cars</a></li>
					<li><a href="/automatic-cars">Automatic Cars</a></li>
					<li><a href="/gallery">Our Gallery</a></li>
					<li class="active"><a href="/contact">Contact us</a></li>
				</ul>
			</div>
			<div class="nav-footer">
				<ul class="list-inline">
					<li>
						<a href="#"><i class="icon icon-Search"></i></a>
						<ul class="search-box">
							<li>
								<form action="#">
									<input type="text" placeholder="Type and Hit Enter">
									<button type="submit"><i class="icon icon-Search"></i></button>
								</form>
							</li>
						</ul>
					</li>
					<li class="menu-expander hidden-lg hidden-md"><a href="#"><i class="icon icon-List"></i></a></li>
				</ul>
			</div>
		</nav>
	</div>
</header>


	<section class="inner-banner inner-banner-4">
		<div class="container text-center">
			<h2><span>get in touch</span></h2>
		</div>
	</section>



	<section class="latest-news contact-page section-padding contact-page-two blog-2-col blog-3-col blog-page">
		<div class="container">
			<div class="row">
				<div class="col-md-3 pull-right">
					<div class="sidebar-widget-wrapper">
						<div class="single-sidebar-widget contact-widget">
							<div class="title">
								<h3><span>Contact Infos</span></h3>
							</div>
							<ul class="contact-infos">
								<li>
									<div class="icon-box">
										<i class="fa fa-map-marker"></i>
									</div>
									<div class="info-text">
										<p>Calicut, Cochin, Trivandrum</p>
									</div>
								</li>
								<li>
									<div class="icon-box">
										<i class="fa fa-phone"></i>
									</div>
									<div class="info-text">
										<p>+91 9947 828 815</p>
									</div>
								</li>
								<li>
									<div class="icon-box">
										<i class="fa fa-phone"></i>
									</div>
									<div class="info-text">
										<p>+91 9562 157 211</p>
									</div>
								</li>
								<li>
									<div class="icon-box">
										<i class="fa fa-envelope"></i>
									</div>
									<div class="info-text">
										<p>holidayrentalcars@gmail.com</p>
									</div>
								</li>
							</ul>
						</div>
						<div class="single-sidebar-widget latest-post-widget">
							<div class="title">
								<h3><span>Scan the QR Code</span></h3>
							</div>
							<div class="img-box">
								<img src="img/qr.png" alt="">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-9 pull-left">
					<form class="contact-form" method="post" action="/sendcontact">

					    {!! csrf_field() !!}
						<div class="row">
							<div class="col-md-4">
								<div class="form-grp">
									<label>Name <span>*</span></label>
									<input type="text" name="name" placeholder="Enter your name">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-grp">
									<label>Email <span>*</span></label>
									<input type="text" name="email" placeholder="Enter your email">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-grp">
									<label>Phone <span>*</span></label>
									<input type="text" name="phone" placeholder="Enter your phone">
								</div>
							</div>
							<div class="col-md-12">
								<div class="form-grp">
									<label>Subject <span>*</span></label>
									<input type="text" name="subject" placeholder="Enter your subject">
								</div>
							</div>
							<div class="col-md-12 clearfix">
								<label>Message <span>*</span></label>
								<textarea name="message" placeholder="Enter Your message"></textarea>
								<button type="submit" class="pull-right thm-btn hvr-sweep-to-top">SEND MESSAGE</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>



	<section class="full-width-google-map">
		<div class="google-map" id="contact-page-google-map" data-map-lat="9.441074" data-map-lng="76.5327752" data-icon-path="img/resources/map-
			pin-2.png" data-map-title="Awesome Place" data-map-zoom="14"></div>
	</section>


	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget about-widget">
						<div class="title">
							<h2><span>About Us</span></h2>
						</div>
						<p>We provides all types of cars that suits your tastes for monthly rental at Cochin, Calicut and Trivandrum International Airports. After Using for one or more months and you can return at Airports itself.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>Useful Links</span></h2>
                        </div>
                        <ul>
                            <li><a href="/about"><i class="fa fa-angle-right"></i> About Us</a></li>
                            <li><a href="/manual-cars"><i class="fa fa-angle-right"></i> Manual Cars</a></li>
                            <li><a href="/automatic-cars"><i class="fa fa-angle-right"></i> Automatic Cars</a></li>
                            <li><a href="/ratechart"><i class="fa fa-angle-right"></i> Service Rates</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget post-widget">
                        <div class="title">
                            <h2><span>recent cars</span></h2>
                        </div>
                        <ul>
                        <?php
                            $recents=\App\NewestCar::with('vehicle.files')->take(4)->get();
                         ?>
                        @if(isset($recents))
                            @foreach($recents as $recent)
                               <li><a href="#"><i class="fa fa-angle-right"></i> {{$recent->vehicle->name}}</a></li>
                            @endforeach
                        @endif
                        </ul>
                    </div>
                </div>
				<div class="col-md-3 col-sm-6">
					<div class="footer-widget contact-widget">
						<div class="title">
							<h2><span>contact info</span></h2>
						</div>
						<ul class="contact-infos">
							<li>
								<div class="icon-box">
									<i class="fa fa-map-marker"></i>
								</div>
								<div class="info-text">
									<p>Calicut, Cochin, Trivandrum</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9947 818 815</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-phone"></i>
								</div>
								<div class="info-text">
									<p>+91 9562 157 211</p>
								</div>
							</li>
							<li>
								<div class="icon-box">
									<i class="fa fa-envelope"></i>
								</div>
								<div class="info-text">
									<p>holidayrentalcars@gmail.com</p>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<section class="bottom-bar">
		<div class="container">
			<div class="text pull-left">
				<p>Copyright <span id="year"></span> &copy; All Rights Reserved <a href="http://psybotechnologies.com/" target="_blank" style="color: #5c5c5c;">Psybo Technologies</a></p>
			</div>
			<div class="social pull-right">
				<ul class="list-inline">
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				</ul>
			</div>
		</div>
	</section>




	<!-- Modal -->
	<div class="modal contact-page fade booking-form" id="booking-form" tabindex="-1" role="dialog" aria-labelledby="booking-form">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h3>Send message for Booking: </h3>
					<form class="contact-form search-form-box" action="inc/booking-form.php">
						<div class="row">
							<div class="col-md-6">
								<div class="form-grp">
									<label>Name <span>*</span></label>
									<input type="text" name="name" placeholder="Enter your name">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Email <span>*</span></label>
									<input type="text" name="email" placeholder="Enter your email">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Phone <span>*</span></label>
									<input type="text" name="phone" placeholder="Enter your phone">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Vehicle: <span>*</span></label>
									<select name="vehicle" class="select-input">
										<option value="Black Lincoln MKT">Black Lincoln MKT</option>
										<option value="Black Lincoln Sedan">Black Lincoln Sedan</option>
										<option value="Mercedes Grand Sedan">Mercedes Grand Sedan</option>
										<option value="Black Cadillac Sedan">Black Cadillac Sedan</option>
										<option value="Cadillac Escalade Limo">Cadillac Escalade Limo</option>
										<option value="Lincoln Stretch Limo">Lincoln Stretch Limo</option>
										<option value="Hummer Strecth Limo">Hummer Strecth Limo</option>
										<option value="Ford Party Bus Limo">Ford Party Bus Limo</option>
										<option value="Mercedes Party Limo">Mercedes Party Limo</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Pickup Date: <span>*</span></label>
									<input type="text" name="date" placeholder="MM/DD/YYYY" class="date-picker">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-grp">
									<label>Pickup Date: <span>*</span></label>
									<input type="text" name="date" placeholder="MM/DD/YYYY" class="date-picker">
								</div>
							</div>
							<div class="clear"></div>
							<div class="col-md-12 clearfix">
								<label>Additional Note <span>*</span></label>
								<textarea name="message" placeholder="Enter Your message"></textarea>
								<button type="submit" class="pull-right thm-btn hvr-sweep-to-top">SEND MESSAGE</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>




	<script src="assets/jquery/jquery-1.11.3.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>

	<script src="http://maps.google.com/maps/api/js?key=AIzaSyC2YTCFvpAnUKs2SBwbklbgwjuZO16ouMY"></script>
	<script src="assets/gmap.js"></script>
	<script src="assets/validate.js"></script>



	<!-- jQuery ui js -->
	<script src="assets/jquery-ui-1.11.4/jquery-ui.js"></script>


	<!-- mixit up -->
	<script src="assets/jquery.mixitup.min.js"></script>
	<!-- fancy box -->
	<script src="assets/fancyapps-fancyBox/source/jquery.fancybox.pack.js"></script>



	<!-- custom.js -->

	<script src="js/map-script.js"></script>
	<script src="js/default-map-script.js"></script>
	<script src="js/custom.js"></script>


    <!-- year scrpt -->
    <script type="text/javascript">
        n =  new Date();
        y = n.getFullYear();
        document.getElementById("year").innerHTML = y;
    </script>


</body>
</html>
