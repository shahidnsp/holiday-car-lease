<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Dashboard <small>Statistics Overview</small>
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Dashboard
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->


<div class="row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comments fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{status.totaltestimonial}}</div>
                        <div>Total Testimonials!</div>
                    </div>
                </div>
            </div>
            <a href="#testimonial">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-tasks fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{status.totalvehicle}}</div>
                        <div>Total Vehicles!</div>
                    </div>
                </div>
            </div>
            <a href="#vehicle">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{status.totalbooking}}</div>
                        <div>Total Booking!</div>
                    </div>
                </div>
            </div>
            <a href="#booking">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

</div>
<!-- /.row -->



<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> New Booking</h3>
            </div>
            <div class="panel-body">
                <div class="list-group">
                    <a ng-repeat="newbooking in newbookings" href="#booking" class="list-group-item">
                        <span class="badge">{{newbooking.pickup_date | date:'dd-MM-yyyy'}} - {{newbooking.return_date | date:'dd-MM-yyyy'}}</span>
                        <i class="fa fa-fw fa-calendar"></i> {{newbooking.vehicle.brand.name}} {{newbooking.vehicle.name}}
                    </a>
                </div>
                <div class="text-right">
                    <a href="#booking">View All Booking <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Today Booking</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Vehicle</th>
                            <th>Pickup Date</th>
                            <th>Return Date</th>
                            <th>Amount (RS)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="todaybooking in todaybookings">
                            <td>{{todaybooking.vehicle.brand.name}} {{todaybooking.vehicle.name}}</td>
                            <td>{{todaybooking.pickup_date | date:'dd-MM-yyyy'}}</td>
                            <td>{{todaybooking.return_date | date:'dd-MM-yyyy'}}</td>
                            <td class="text-danger">₹ {{todaybooking.amount}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="text-right">
                    <a href="#booking">View All Booking <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->
