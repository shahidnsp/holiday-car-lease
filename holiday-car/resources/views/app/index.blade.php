<!DOCTYPE html>
<html lang="en" ng-app="myApp">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Holiday Car Lease</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/app.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body ng-controller="HomeController">

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Administrator</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a ng-init="name='{{\Illuminate\Support\Facades\Auth::user()->name}}'"  class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> @{{name}} <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a ng-init="username='{{\Illuminate\Support\Facades\Auth::user()->username}}'" href="#profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="user/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li ng-class="menuClass('')">
                        <a href="#/"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li ng-class="menuClass('brand')">
                        <a href="#brand"><i class="fa fa-fw fa-bar-chart-o"></i> Brand</a>
                    </li>
                    <li ng-class="menuClass('location')">
                        <a href="#location"><i class="fa fa-fw fa-table"></i> Pickup Location</a>
                    </li>
                    <li ng-class="menuClass('vehicle')">
                        <a href="#vehicle"><i class="fa fa-fw fa-edit"></i> Vehicles</a>
                    </li>
                    <li ng-class="menuClass('newestcar')">
                        <a href="#newestcar"><i class="fa fa-fw fa-desktop"></i> Newest Cars </a>
                    </li>
                    <!--<li ng-class="menuClass('popularcar')">
                        <a href="#popularcar"><i class="fa fa-fw fa-wrench"></i> Popular Cars</a>
                    </li>-->
                     <li ng-class="menuClass('booking')">
                        <a href="#booking"><i class="fa fa-fw fa-file"></i>Booking</a>
                     </li>
                      <li ng-class="menuClass('status')">
                         <a href="#status"><i class="fa fa-fw fa-file"></i>Vehicle Status</a>
                      </li>
                    <li ng-class="menuClass('gallery')">
                        <a href="#gallery"><i class="fa fa-fw fa-file"></i>Gallery</a>
                    </li>
                    <li ng-class="menuClass('slider')">
                        <a href="#slider"><i class="fa fa-fw fa-file"></i>Slider</a>
                    </li>
                     <li ng-class="menuClass('testimonial')">
                        <a href="#testimonial"><i class="fa fa-fw fa-arrows-v"></i>Testimonial</a>
                     </li>
                     <li ng-class="menuClass('contact')">
                         <a href="#contact"><i class="fa fa-fw fa-file"></i>Get In Touch</a>
                     </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">
                <div class="row" ng-view></div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!--Angular Files-->
       <script src="js/angularjs.min.js"></script>
       <script src="js/app.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="js/plugins/morris/raphael.min.js"></script>
    <script src="js/plugins/morris/morris.min.js"></script>
    <script src="js/plugins/morris/morris-data.js"></script>


</body>

</html>
