<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Testimonial <small>Testimonial Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Testimonial
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-click="newTestimonial();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Testimonial</button>
            <form class="form-horizontal" ng-show="testimonialedit" ng-submit="addTestimonial();">
                <h3>New Testimonial</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newtestimonial.name" placeholder="Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Designation</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newtestimonial.designation" placeholder="Designation" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Content</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" ng-model="newtestimonial.content" placeholder="Content" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-10">
                        <input accept="image/*" ng-file-model="newtestimonial.photos" type="file" multiple/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelTestimonial();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Testimonial List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Name</th>
                        <th>Designation</th>
                        <th>Content</th>
                        <th>Photo</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="testimonial in listCount  = (testimonials | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{testimonial.name}}</td>
                        <td>{{testimonial.designation}}</td>
                        <td><p class="description" popover="{{testimonial.content}}" popover-trigger="mouseenter">{{testimonial.content}}</p></td>
                        <td><img src="images/{{testimonial.photo}}" style="width: 79px;height: 79px;" alt=""/></td>
                        <td>{{testimonial.created_at}}</td>
                        <td>{{testimonial.updated_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="editTestimonial(testimonial);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-default" ng-click="deleteTestimonial(testimonial);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="testimonials.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>