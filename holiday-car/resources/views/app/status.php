<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Booking <small>Booking Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Vehicle Status
            </li>
        </ol>
    </div>
</div>


<br/>
<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input  type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<br/>

<div class="row text-center">
    <label class="radio-inline">
        <input type="radio" ng-model="status" ng-click="loadStatus(status);" value="All">All Car
    </label>
    <label class="radio-inline">
        <input type="radio" ng-model="status" ng-click="loadStatus(status);" value="Running">Running Cars
    </label>
    <label class="radio-inline">
        <input type="radio" ng-model="status" ng-click="loadStatus(status);" value="Overdue">Over Due Cars
    </label>
    <label class="radio-inline">
        <input type="radio" ng-model="status" ng-click="loadStatus(status);" value="Available">Available Cars
    </label>
</div>
<br/>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Vehicle Status</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Vehicle</th>
                        <th>Name</th>
                        <th>Phone number</th>
                        <th>Pickup date</th>
                        <th>Return date</th>
                        <th>Through</th>
                        <th>Pickup Location</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="vehicle in listCount  = (vehicles | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{vehicle.brand.name}} {{vehicle.name}} | {{vehicle.transmission}}</span></a></td>
                        <td>{{vehicle.person}}</td>
                        <td>{{vehicle.phone}}</td>
                        <td>{{vehicle.pickup_date}}</td>
                        <td>{{vehicle.return_date}}</td>
                        <td>{{vehicle.bookingthrough}}</td>
                        <td>{{vehicle.location}}</td>
                        <td>{{vehicle.amount}}</td>
                        <td ng-class="vehicle.status=='Running' ? 'bg-success' : vehicle.status=='Overdue' ? 'bg-danger' : ''">
                            {{vehicle.status}}
                        </td>
                        <td>
                            <button ng-if="vehicle.status=='Running' || vehicle.status=='Overdue'" ng-click="makePayment(vehicle);" class="btn btn-sm btn-primary">Make Payment</button>
                            <button ng-if="vehicle.status=='Running' || vehicle.status=='Overdue'" ng-click="endBooking(vehicle);" class="btn btn-sm btn-danger">End Booking</button>
                            <button ng-if="vehicle.status=='Running' || vehicle.status=='Overdue'" ng-click="message(vehicle);" class="btn btn-sm btn-success">Message</button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="bookings.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>
</div>





