<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Change Profile
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">

            <tabset>
                <tab heading="Profile" active="true">
                    <br/><br/>
                    <form class="form-horizontal" ng-submit="updateProfile();">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Profile Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="name" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">User Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="username" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7 text-center">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                </tab>
                <tab heading="Change Password">
                    <br/>
                    <form class="form-horizontal" ng-submit="changePassword(curpassword,newpassword,repassword);">

                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Current Password</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="curpassword" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">New Password</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="newpassword" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Re-enter Password</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" ng-model="repassword" required=""/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-7 text-center">
                                <button type="submit" class="btn btn-primary">Change Password</button>
                            </div>
                        </div>
                        <hr>
                    </form>
                </tab>
            </tabset>


        </div>
    </div>
</div>

<br/>

