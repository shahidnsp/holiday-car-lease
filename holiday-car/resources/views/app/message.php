<div>
    <br/>
    <div class="island">
        <form ng-submit="sendMessage(message)">
            <div class="example text-center">
                <div class="large-8 columns large-centered">
                    <div class="row" style="padding-left: 10px;padding-right: 10px;">
                        <textarea ng-model="message" class="form-control" required=""></textarea>
                    </div>
                </div>
            </div>
            <br/>
            <div class="text-center">
                <button type="submit" class="btn btn-primary">Send Message</button>
            </div>
        </form>
    </div>
</div>