<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Booking <small>Booking Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Booking
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-click="newBooking();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Booking</button>
            <form class="form-horizontal" ng-show="bookingedit" ng-submit="addBooking();">
                <h3>New Booking</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbooking.name" placeholder="Booking Name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" ng-model="newbooking.email" placeholder="Email" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Phone number</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbooking.phone" placeholder="Phone number" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Pickup date</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-click="pickup_datepicker=true" datepicker-popup="dd-MMMM-yyyy" ng-model="newbooking.pickup_date" is-open="pickup_datepicker" ng-change="getAmount(newbooking.pickup_date,newbooking.return_date,newbooking.vehicle_id);"  show-button-bar="true" show-weeks="false" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Return date</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-click="return_datepicker=true" datepicker-popup="dd-MMMM-yyyy" ng-model="newbooking.return_date" ng-change="getAmount(newbooking.pickup_date,newbooking.return_date,newbooking.vehicle_id);" is-open="return_datepicker" show-button-bar="true" show-weeks="false" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Pickup Location</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="location_id" ng-model="newbooking.pickup_location_id" ng-change="getAmount(newbooking.pickup_date,newbooking.return_date,newbooking.vehicle_id);" id="" required="">
                            <option></option>
                            <option ng-repeat="location in Locations" value="{{location.id}}">{{location.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Vehicle</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="vehicle_id" ng-model="newbooking.vehicle_id" id="" required="">
                            <option></option>
                            <option ng-repeat="vehicle in vehicles" value="{{vehicle.id}}">{{vehicle.brand.name}} {{vehicle.name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Amount</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" ng-model="newbooking.amount" placeholder="Amount" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" ng-model="newbooking.description" placeholder="Description" required></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-11 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelBooking();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input  type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-2 form-horizontal">
        <div class="form-inline  form-group">
            <label class="col-sm-3 control-label">Status</label>
            <div class="col-sm-8">
                <select class="form-control" ng-model="status" id="">
                    <option value="All">All</option>
                    <option value="1">Pending</option>
                    <option value="0">Approved</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-8 form-horizontal">
        <div class="col-sm-6">
            <div class="form-inline form-group">
                <label for="" class="col-sm-4 control-label">From Date</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" ng-click="from_datepicker=true" datepicker-popup="dd-MMMM-yyyy" ng-model="fromDate" is-open="from_datepicker" show-button-bar="true"  show-weeks="false" readonly>
                </div>
            </div>
        </div>
       <div class="col-sm-6">
           <div class="form-inline form-group">
               <label for="" class="col-sm-4 control-label">To Date</label>
               <div class="col-sm-8">
                   <input type="text" class="form-control" ng-click="to_datepicker=true" datepicker-popup="dd-MMMM-yyyy" ng-model="toDate" is-open="to_datepicker" show-button-bar="true" show-weeks="false" readonly>
               </div>
           </div>
       </div>
    </div>
    <div class="col-md-2 form-horizontal">
        <button ng-click="search(status,fromDate,toDate);" class="btn btn-primary">Search</button>
    </div>

</div>
<br/>
<div ng-show="showList" class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Booking List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Vehicle</th>
                        <th>Name</th>
                        <th>Phone number</th>
                        <th>Pickup date</th>
                        <th>Return date</th>
                        <th>Through</th>
                        <th>Pickup Location</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>End</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="booking in listCount  = (bookings | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage" ng-class="booking.isnew=='0' ? 'success' : 'warning'">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td><a ng-click="showDetails(booking);">{{booking.vehicle.name}}|<span ng-class="booking.vehicle.transmission=='Manual' ? 'text-success' : 'text-danger'">{{booking.vehicle.transmission}}</span></a></td>
                        <td>{{booking.name}}</td>
                        <td>{{booking.phone}}</td>
                        <td>{{booking.pickup_date | date:'dd-MMMM-yyyy'}}</td>
                        <td>{{booking.return_date | date:'dd-MMMM-yyyy'}}</td>
                        <td>{{booking.bookingthrough}}</td>
                        <td>{{booking.pickup_location.name}}</td>
                        <td>{{booking.amount}}</td>
                        <td class="text-center"><button ng-if="booking.isnew=='0'" ng-click="moveToNotified(booking);" class="btn btn-success">Approved</button><button ng-if="booking.isnew=='1'" ng-click="moveToNotified(booking);" class="btn btn-danger">Pending</button></td>
                        <td class="text-center"><button ng-if="booking.isnew=='0' && booking.endbooking=='0'" ng-click="endBooking(booking);" class="btn btn-success">End Booking</button><button ng-if="booking.isnew=='0' && booking.endbooking=='1'" ng-click="endBooking(booking);" class="btn btn-success">Active Booking</button></td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="editBooking(booking);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-default" ng-click="deleteBooking(booking);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="bookings.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>
</div>


    <!-- /.row-->
<div ng-hide="showList" class="row">
        <div class="panel panel-primary">
            <div class="col-md-12">
                <h2 class="panel-heading text-success">
                    Vehicle: {{myBooking.name}} <small class="text-primary">Brand: {{myBooking.vehicle.brand.name}}</small>
                </h2>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr class="text-danger text-center">
                                    <td colspan="2"><h3>Booking Details</h3></td>
                                </tr>
                                <tr>
                                    <td>Name:</td><td>{{myBooking.name}}</td>
                                </tr>
                                <tr>
                                    <td>Email:</td><td>{{myBooking.email}}</td>
                                </tr>
                                <tr>
                                    <td>Phone:</td><td>{{myBooking.phone}}</td>
                                </tr>
                                <tr>
                                    <td>Pickup Date:</td><td>{{myBooking.pickup_date}}</td>
                                </tr>
                                <tr>
                                    <td>Return Date:</td><td>{{myBooking.return_date}}</td>
                                </tr>
                                <tr>
                                    <td>Pickup Location:</td><td>{{myBooking.pickup_location.name}}</td>
                                </tr>
                                <tr>
                                    <td>Amount:</td><td>₹ {{myBooking.amount}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr class="text-danger text-center">
                                    <td colspan="2"><h3>Home Address</h3></td>
                                </tr>
                                <tr>
                                    <td>Address:</td><td>{{myBooking.home_address.address1}}, {{myBooking.home_address.address2}}</td>
                                </tr>
                                <tr>
                                    <td>Post:</td><td>{{myBooking.home_address.post}}</td>
                                </tr>
                                <tr>
                                    <td>City:</td><td>{{myBooking.home_address.city}},{{myBooking.home_address.state}} - {{myBooking.home_address.pincode}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr class="text-danger text-center">
                                    <td colspan="2"><h3>Vehicle Details</h3></td>
                                </tr>
                                <tr>
                                    <td>Vehicle:</td><td>{{myBooking.vehicle.brand.name}} {{myBooking.vehicle.name}}</td>
                                </tr>
                                <tr>
                                    <td>Transmission:</td><td>{{myBooking.vehicle.transmission}}</td>
                                </tr>
                                <tr>
                                    <td>Displacement:</td><td>{{myBooking.vehicle.displacement}}</td>
                                </tr>
                                <tr>
                                    <td>Fuel:</td><td>{{myBooking.vehicle.fuel}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr class="text-danger text-center">
                                    <td colspan="2"><h3>Contact Info</h3></td>
                                </tr>
                                <tr>
                                    <td>Name:</td><td>{{myBooking.contact_information.name}}</td>
                                </tr>
                                <tr>
                                    <td>Mobile:</td><td>{{myBooking.contact_information.mobile}}</td>
                                </tr>
                                <tr>
                                    <td>Email:</td><td>{{myBooking.contact_information.email}}</td>
                                </tr>
                                <tr>
                                    <td>DOB:</td><td>{{myBooking.contact_information.dateofbirth}}</td>
                                </tr>
                                <tr>
                                    <td>Gender:</td><td>{{myBooking.contact_information.gender}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 text-center">
                <button ng-click="backToList();" class="btn btn-primary">Back To List</button> <button ng-click="editBooking(myBooking);" class="btn btn-success">Edit Booking</button> <button ng-click="moveToNotified(myBooking);" class="btn btn-danger">Move To Approved</button>
            </div>
        </div>
    </div>


