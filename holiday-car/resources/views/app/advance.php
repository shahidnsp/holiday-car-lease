<h3 class="text-center">Advance and Processing Fee</h3>

<form class="form-horizontal text-center" ng-submit="submit(booking);">
    <div class="form-group">
        <label class="control-label">Amount: {{booking.amount}}</label>
    </div>
    <div class="form-group">
        <label class="control-label" style="color: blue;">Pickup Date: {{booking.pickup_date}}</label>
        <label class="control-label" style="color: green;">Return Date: {{booking.return_date}}</label>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-4">Processing Fee</label>
        <div class="col-sm-8">
            <input class="form-control" ng-model="booking.processfee" type="text"/>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label  col-sm-4">Advance Amount</label>
        <div class="col-sm-8">
            <input class="form-control" ng-model="booking.advance" type="text"/>
        </div>
    </div>
    <div class="form-group">
        <button class="btn btn-primary">Submit and Approve</button>
    </div>
</form>