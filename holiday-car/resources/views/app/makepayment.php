<h3 class="text-center">Make Payment</h3>

<form class="form-horizontal text-center" ng-submit="submit(booking,paid);">
    <div class="form-group">
        <label class="control-label">Amount: {{booking.amount}}</label>
    </div>
    <div class="form-group">
        <label class="control-label" style="color: blue;">Pickup Date: {{booking.pickup_date}}</label>
        <label class="control-label" style="color: green;">Return Date: {{booking.return_date}}</label>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-6">Processing Fee: {{booking.processfee}}</label>
        <label class="control-label col-sm-6">Advance Amount: {{booking.advance}}</label>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-6" style="color: blue;">Total Amount: {{total}}</label>
        <label class="control-label col-sm-6" style="color: red;">Balance Amount: {{balance}}</label>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-4">Paid Amount:</label>
        <div class="col-sm-7">
            <input class="form-control" ng-model="paid" type="text"/>
        </div>
    </div>

    <div class="form-group">
        <button class="btn btn-primary">Make Payment</button>
    </div>
</form>