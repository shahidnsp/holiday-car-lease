<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Contact <small>Contact Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Contact
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div ng-hide="showMessage" class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Contact List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Created_at</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="contact in listCount  = (contacts | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td><a ng-click="showSingleMessage(contact);">{{contact.name}}</a></td>
                        <td>{{contact.email}}</td>
                        <td>{{contact.phone}}</td>
                        <td>{{contact.subject}}</td>
                        <td><p class="description" popover="{{contact.message}}" popover-trigger="mouseenter">{{contact.message}}</p></td>
                        <td>{{contact.created_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="deleteContact(contact);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div ng-hide="showMessage" class="clearfix" ng-show="contacts.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

    <div ng-show="showMessage" class="row">
        <div class="panel panel-primary">
            <div class="col-md-12">
                <h2 class="panel-heading text-success">
                   {{myMessage.name}} <small class="text-primary">Email: {{myMessage.email}}, Phone: {{myMessage.phone}}</small>
                </h2>
            </div>
            <div class="col-sm-12">
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i> <span class="text-danger">{{myMessage.subject}}</span>
                    </li>
                </ol>
            </div>
            <div class="col-sm-12">
                <div class="panel-body">
                    &nbsp;&nbsp;&nbsp;<p>{{myMessage.message}}</p>
                </div>
            </div>
            <div class="col-sm-12 text-center">
                <button ng-click="backToList();" class="btn btn-primary">Back To List</button>
            </div>
        </div>
    </div>

</div>