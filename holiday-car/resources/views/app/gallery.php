<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<style>

    @import url(http://fonts.googleapis.com/css?family=Anaheim);

    *{
        margin: 0;
        padding: 0;
        outline: none;
        border: none;
        box-sizing: border-box;
    }
    *:before,
    *:after{
        box-sizing: border-box;
    }
    html,
    body{
        min-height: 100%;
    }
    body{
        background-image: radial-gradient(mintcream 0%, lightgray 100%);
    }
    h1{
        display: table;
        margin: 5% auto 0;
        text-transform: uppercase;
        font-family: 'Anaheim', sans-serif;
        font-size: 4em;
        font-weight: 400;
        text-shadow: 0 1px white, 0 2px black;
    }
    .container{
        margin: 4% auto;
        width: 210px;
        height: 140px;
        position: relative;
        perspective: 1000px;
    }
    #carousel{
        width: 100%;
        height: 100%;
        position: absolute;
        transform-style: preserve-3d;
        animation: rotation 20s infinite linear;
    }
    #carousel:hover{
        animation-play-state: paused;
    }
    #carousel figure{
        display: block;
        position: absolute;
        width: 90%;
        height: 50%px;
        left: 10px;
        top: 10px;
        background: black;
        overflow: hidden;
        border: solid 5px black;
    }
    #carousel figure:nth-child(1){transform: rotateY(0deg) translateZ(288px);}
    #carousel figure:nth-child(2) { transform: rotateY(40deg) translateZ(288px);}
    #carousel figure:nth-child(3) { transform: rotateY(80deg) translateZ(288px);}
    #carousel figure:nth-child(4) { transform: rotateY(120deg) translateZ(288px);}
    #carousel figure:nth-child(5) { transform: rotateY(160deg) translateZ(288px);}
    #carousel figure:nth-child(6) { transform: rotateY(200deg) translateZ(288px);}
    #carousel figure:nth-child(7) { transform: rotateY(240deg) translateZ(288px);}
    #carousel figure:nth-child(8) { transform: rotateY(280deg) translateZ(288px);}
    #carousel figure:nth-child(9) { transform: rotateY(320deg) translateZ(288px);}

    img{
        -webkit-filter: grayscale(1);
        cursor: pointer;
        transition: all .5s ease;
    }
    img:hover{
        -webkit-filter: grayscale(0);
        transform: scale(1.2,1.2);
    }

    @keyframes rotation{
        from{
            transform: rotateY(0deg);
        }
        to{
            transform: rotateY(360deg);
        }
    }

    .container {
        position: relative;
        width: 50%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%)
    }

    .container:hover .image {
        opacity: 0.3;
    }

    .container:hover .middle {
        opacity: 1;
    }

    .text {
        background-color: #4CAF50;
        color: white;
        font-size: 16px;
        padding: 16px 32px;
    }
</style>
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Gallery <small>Gallery Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Gallery
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-click="newGallery();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Gallery</button>
            <form class="form-horizontal" ng-show="galleryedit" ng-submit="addGallery();">
                <h3>New Gallery</h3><br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" ng-model="newgallery.name" placeholder="Gallery Name">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" ng-model="newgallery.description" placeholder="Description"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Photo</label>
                    <div class="col-sm-10">
                        <input accept="image/*" ng-file-model="newgallery.photos" type="file" multiple/>
                    </div>
                </div>

                <div class="row" ng-show="isEdit">
                    <div class="panel panel-info">
                        <h3>Photos in This Gallery</h3>
                        <div ng-repeat="photo in newgallery.files" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp delay-03s">
                            <div class="container">
                                <img src="images/{{photo.name}}" alt="Avatar" class="image" style="width: 150px; height: 120px;">
                                <div class="middle">
                                    <div class="text"><button type="button" ng-click="deletePhoto(photo);" class="btn btn-sm btn-danger">Delete</button></div>
                                </div>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12 text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-default" ng-click="cancelGallery();">Cancel</button>
                    </div>
                </div>
                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>

<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Gallery List</span>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="gallery in listCount  = (galleries | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td>{{gallery.name}}</td>
                        <td><p class="description" popover="{{gallery.description}}" popover-trigger="mouseenter">{{gallery.description}}</p></td>
                        <td><img src="images/{{gallery.files[0].name}}" style="height: 80px;width: 80px;" alt=""/></td>
                        <td>{{gallery.created_at}}</td>
                        <td>{{gallery.updated_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="editGallery(gallery);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-default" ng-click="deleteGallery(gallery);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="clearfix" ng-show="galleries.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

</div>