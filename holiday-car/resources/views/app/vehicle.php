<div show-during-resolve class="alert alert-info">
    <strong>Loading....Please Wait</strong>
</div>
<style>
    @import url(http://fonts.googleapis.com/css?family=Anaheim);

    *{
        margin: 0;
        padding: 0;
        outline: none;
        border: none;
        box-sizing: border-box;
    }
    *:before,
    *:after{
        box-sizing: border-box;
    }
    html,
    body{
        min-height: 100%;
    }
    body{
        background-image: radial-gradient(mintcream 0%, lightgray 100%);
    }
    h1{
        display: table;
        margin: 5% auto 0;
        text-transform: uppercase;
        font-family: 'Anaheim', sans-serif;
        font-size: 4em;
        font-weight: 400;
        text-shadow: 0 1px white, 0 2px black;
    }
    .container{
        margin: 4% auto;
        width: 210px;
        height: 140px;
        position: relative;
        perspective: 1000px;
    }
    #carousel{
        width: 100%;
        height: 100%;
        position: absolute;
        transform-style: preserve-3d;
        animation: rotation 20s infinite linear;
    }
    #carousel:hover{
        animation-play-state: paused;
    }
    #carousel figure{
        display: block;
        position: absolute;
        width: 90%;
        height: 50%px;
        left: 10px;
        top: 10px;
        background: black;
        overflow: hidden;
        border: solid 5px black;
    }
    #carousel figure:nth-child(1){transform: rotateY(0deg) translateZ(288px);}
    #carousel figure:nth-child(2) { transform: rotateY(40deg) translateZ(288px);}
    #carousel figure:nth-child(3) { transform: rotateY(80deg) translateZ(288px);}
    #carousel figure:nth-child(4) { transform: rotateY(120deg) translateZ(288px);}
    #carousel figure:nth-child(5) { transform: rotateY(160deg) translateZ(288px);}
    #carousel figure:nth-child(6) { transform: rotateY(200deg) translateZ(288px);}
    #carousel figure:nth-child(7) { transform: rotateY(240deg) translateZ(288px);}
    #carousel figure:nth-child(8) { transform: rotateY(280deg) translateZ(288px);}
    #carousel figure:nth-child(9) { transform: rotateY(320deg) translateZ(288px);}

    img{
        -webkit-filter: grayscale(1);
        cursor: pointer;
        transition: all .5s ease;
    }
    img:hover{
        -webkit-filter: grayscale(0);
        transform: scale(1.2,1.2);
    }

    @keyframes rotation{
        from{
            transform: rotateY(0deg);
        }
        to{
            transform: rotateY(360deg);
        }
    }

    .container {
        position: relative;
        width: 50%;
    }

    .image {
        opacity: 1;
        display: block;
        width: 100%;
        height: auto;
        transition: .5s ease;
        backface-visibility: hidden;
    }

    .middle {
        transition: .5s ease;
        opacity: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%)
    }

    .container:hover .image {
        opacity: 0.3;
    }

    .container:hover .middle {
        opacity: 1;
    }

    .text {
        background-color: #4CAF50;
        color: white;
        font-size: 16px;
        padding: 16px 32px;
    }
</style>


<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--        <h2 class="page-header">-->
        <!--            Brand <small>Brand Overview</small>-->
        <!--        </h2>-->
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Vehicle
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <button ng-click="newVehicle();" class="btn btn-primary pull-right" type="button"><i class="fa fa-plus"></i> Add Vehicle</button>
            <form class="form-horizontal" ng-show="vehicleedit" ng-submit="addVehicle();">
                <h3>New Vehicle</h3><br>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Brand <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="fuel"  class="form-control" ng-model="newvehicle.brand_id" id="" placeholder="Vehicle Brand" required>
                                    <option value=""></option>
                                    <option ng-repeat="brand in brands" value="{{brand.id}}">{{brand.name}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Name<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.name " placeholder="Vehicle Name" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Transmission <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="fuel" class="form-control" ng-model="newvehicle.transmission" id="" placeholder="Vehicle Transmission" required="">
                                    <option value=""></option>
                                    <option value="Automatic">Automatic</option>
                                    <option value="Manual">Manual</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Displacement <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.displacement" placeholder="Vehicle Displacement" required="">
                            </div>
                        </div> <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Fuel <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <select name="fuel" class="form-control" ng-model="newvehicle.fuel" id="" placeholder="Vehicle Fuel" required="">
                                    <option value=""></option>
                                    <option value="Petrol">Petrol</option>
                                    <option value="Diesel">Diesel</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Bhp <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.bhp" placeholder="Vehicle Bhp" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Mileage <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.mileage" placeholder="Vehicle Mileage" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Torque <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.torque" placeholder="Vehicle Torque" required="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">Engine <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.engine" placeholder="Vehicle Engine" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">No of Seat <span class="text-danger">*</span></label>
                            <div class="col-sm-8"> 
                                <input type="text" class="form-control" ng-model="newvehicle.seat" placeholder="No of Seat" required="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Center Lock</label>
                            <div class="col-sm-8">
                                <select name="fuel" class="form-control" ng-model="newvehicle.center_lock" id="" placeholder="Vehicle Center Lock">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Audio system</label>
                            <div class="col-sm-8">
                                <select name="fuel" class="form-control" ng-model="newvehicle.audio_system" id="" placeholder="Audio system">
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">

                       <div class="form-group">
                           <label class="col-sm-4 control-label">Video</label>
                           <div class="col-sm-8">
                               <select name="fuel" class="form-control" ng-model="newvehicle.video" id="" placeholder="Vehicle Video">
                                   <option value="1">Yes</option>
                                   <option value="0">No</option>
                               </select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-4 control-label">GPS</label>
                           <div class="col-sm-8">
                               <select name="fuel" class="form-control" ng-model="newvehicle.gps" id="" placeholder="GPS Control">
                                   <option value="1">Yes</option>
                                   <option value="0">No</option>
                               </select>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-4 control-label">Airbag</label>
                           <div class="col-sm-8">
                               <select name="fuel" class="form-control" ng-model="newvehicle.airbag" id="" placeholder="Vehicle Airbag">
                                   <option value="1">Yes</option>
                                   <option value="0">No</option>
                               </select>
                           </div>
                       </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Vehicle Modal <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.modelyear" placeholder="Vehicle Modal (Year)" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Number of Gears <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" ng-model="newvehicle.noofgear" placeholder="Number of Gears" required>
                            </div>
                        </div>
                       <div class="form-group">
                           <label class="col-sm-4 control-label">No airbag</label>
                           <div class="col-sm-8">
                               <input type="text" class="form-control" ng-model="newvehicle.no_airbag" placeholder="No of Airbag">
                           </div>
                       </div>
                        <div class="form-group">
                            <label class="control-label">Add Day wise rent :</label>
                        </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">5 to 15 <span class="text-danger">*</span></label>
                           <div class="col-sm-4">
                               <input type="text" class="form-control" ng-model="newvehicle.to15" placeholder="5 to 15 Days" required>
                           </div>
                           <label class="col-sm-2 control-label">15 to 25 <span class="text-danger">*</span></label>
                           <div class="col-sm-4">
                               <input type="text" class="form-control" ng-model="newvehicle.to25" placeholder="15 to 25 Days" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">25 to 35 <span class="text-danger">*</span></label>
                           <div class="col-sm-4">
                               <input type="text" class="form-control" ng-model="newvehicle.to35" placeholder="25 to 35 Days" required>
                           </div>
                           <label class="col-sm-2 control-label">35 to 45 <span class="text-danger">*</span></label>
                           <div class="col-sm-4">
                               <input type="text" class="form-control" ng-model="newvehicle.to45" placeholder="35 to 45 Days" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-2 control-label">45 to 55 <span class="text-danger">*</span></label>
                           <div class="col-sm-4">
                               <input type="text" class="form-control" ng-model="newvehicle.to55" placeholder="45 to 55 Days" required>
                           </div>
                           <label class="col-sm-2 control-label">55 and above <span class="text-danger">*</span></label>
                           <div class="col-sm-4">
                               <input type="text" class="form-control" ng-model="newvehicle.above" placeholder="55 and above" required>
                           </div>
                       </div>
                       <div class="form-group">
                           <label class="col-sm-4 control-label">Photos <span class="text-danger">*</span></label>
                           <div class="col-sm-8">
                               <input accept="image/*" ng-file-model="newvehicle.photos" type="file" multiple/>
                           </div>
                       </div>
                   </div>
                </div>

                <div class="row" ng-show="isEdit">
                    <div class="panel panel-info">
                        <h3>Photos in this Vehicle</h3>
                        <div ng-repeat="photo in newvehicle.files" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 wow fadeInUp delay-03s">
                            <div class="container">
                                <img src="images/{{photo.name}}" alt="Avatar" class="image" style="width: 150px; height: 120px;">
                                <div class="middle">
                                    <div class="text"><button type="button" ng-click="deletePhoto(photo);" class="btn btn-sm btn-danger">Delete</button></div>
                                </div>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>

                <div class="row">
                  <div class="col-sm-11 text-right">
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-default" ng-click="cancelVehicle();">Cancel</button>
                     </div>
                  </div>
                </div>

                <hr>
            </form>
        </div>
    </div>
</div>

<br/>

<div class="row">
    <div class="col-md-4">
        <label for="">Search:
            <select class="form-control pagiantion" ng-model="numPerPage" ng-options="numPage for numPage in numsForPage" ng-change="changeNum(numPerPage);"></select>
            entries
        </label>
    </div>
    <div class="col-md-8 text-right">
        <div class="form-inline form-group">
            <label for="filter-list">Search </label>
            <input  type="text" class="form-control" id="filter-list" placeholder="Search" ng-model="filterlist">
        </div>
    </div>
</div>
<div  class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="text-success">Vehicle List</span>
        </div>
        <div ng-hide="showVehicle" class="panel-body">
            <div class="table-responsive">
                <table id="expenseTable" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>$</th>
                        <th>Brand</th>
                        <th>Name</th>
                        <th>Transmission</th>
                        <th>Displacement</th>
                        <th>Fuel</th>
                        <th>Photo</th>
                        <th>Created_at</th>
                        <th>Updated_at</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="vehicle in listCount  = (vehicles | filter:filterlist) | orderBy:'-created_at' | pagination: currentPage : numPerPage">
                        <td>{{numPerPage *(currentPage-1)+$index+1}}</td>
                        <td><a ng-click="showSingleVehicle(vehicle);">{{vehicle.brand.name}}</a></td>
                        <td>{{vehicle.name}}</td>
                        <td>{{vehicle.transmission}}</td>
                        <td>{{vehicle.displacement}}</td>
                        <td>{{vehicle.fuel}}</td>
                        <td><img src="images/{{vehicle.files[0].name}}" style="height: 80px;width: 80px;" alt=""/></td>
                        <td>{{vehicle.created_at}}</td>
                        <td>{{vehicle.updated_at}}</td>
                        <td>
                            <div  class="btn-group btn-group-xs" role="group" ng-init="editmode=false">
                                <button type="button" class="btn btn-default" ng-click="editVehicle(vehicle);">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button type="button" class="btn btn-default" ng-click="deleteVehicle(vehicle);">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-11" ng-hide="showVehicle" class="clearfix" ng-show="vehicles.length > numPerPage">
        <pagination
            ng-model="currentPage"
            total-items="listCount.length"
            max-size="maxSize"
            items-per-page="numPerPage"
            boundary-links="true"
            class="pagination-sm pull-right"
            previous-text="&lsaquo;"
            next-text="&rsaquo;"
            first-text="&laquo;"
            last-text="&raquo;"
            ></pagination>
    </div>

    <div ng-show="showVehicle" class="row">
        <div class="panel panel-primary">
            <div class="col-md-12">
                <h2 class="panel-heading text-success">
                    {{myVehicle.name}} <small class="text-primary">Brand: {{myVehicle.brand.name}}</small>
                </h2>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td>Transmission:</td><td>{{myVehicle.transmission}}</td>
                                </tr>
                                <tr>
                                    <td>Displacement:</td><td>{{myVehicle.displacement}}</td>
                                </tr>
                                <tr>
                                    <td>Fuel:</td><td>{{myVehicle.fuel}}</td>
                                </tr>
                                <tr>
                                    <td>Center Lock:</td><td><span ng-if="myVehicle.center_lock=='1'" class="text-primary">Yes</span><span ng-if="myVehicle.center_lock=='0'" class="text-success">No</span></td>
                                </tr>
                                <tr>
                                    <td>Video System:</td><td><span ng-if="myVehicle.video=='1'" class="text-primary">Yes</span><span ng-if="myVehicle.video=='0'" class="text-success">No</span></td>
                                </tr>
                                <tr>
                                    <td>Air Bag:</td><td><span ng-if="myVehicle.airbag=='1'" class="text-primary">Yes | {{myVehicle.no_airbag}} Nons</span><span ng-if="myVehicle.airbag=='0'" class="text-success">No</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td>BHP:</td><td>{{myVehicle.bhp}}</td>
                                </tr>
                                <tr>
                                    <td>Mileage:</td><td>{{myVehicle.mileage}}</td>
                                </tr>
                                <tr>
                                    <td>Torque:</td><td>{{myVehicle.torque}}</td>
                                </tr>
                                <tr>
                                    <td>No.of Seat:</td><td>{{myVehicle.seat}}</td>
                                </tr>
                                <tr>
                                    <td>Engine:</td><td>{{myVehicle.engine}}</td>
                                </tr>
                                <tr>
                                    <td>Audio System:</td><td><span ng-if="myVehicle.audio_system=='1'" class="text-primary">Yes</span><span ng-if="myVehicle.audio_system=='0'" class="text-success">No</span></td>
                                </tr>
                                <tr>
                                    <td>GPS Control:</td><td><span ng-if="myVehicle.gps=='1'" class="text-primary">Yes</span><span ng-if="myVehicle.gps=='0'" class="text-success">No</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row col-sm-12">
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="table-responsive panel-body">
                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <td>5 To 15 Days:</td><td>{{myVehicle.to15}}</td>
                                </tr>
                                <tr>
                                    <td>15 To 25 Days:</td><td>{{myVehicle.to25}}</td>
                                </tr>
                                <tr>
                                    <td>25 To 35 Days:</td><td>{{myVehicle.to35}}</td>
                                </tr>
                                <tr>
                                    <td>35 To 45 Days:</td><td>{{myVehicle.to45}}</td>
                                </tr>
                                <tr>
                                    <td>45 To 55 Days:</td><td>{{myVehicle.to55}}</td>
                                </tr>
                                <tr>
                                    <td>55 and Above:</td><td>{{myVehicle.above}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div ng-hide="myVehicle.files.length==0" class="col-sm-6">
                    <div class="col-sm-12 text-center">
                        <div class="container">
                            <div id="carousel">
                                <figure ng-repeat="file in myVehicle.files"><img style="width: 150px;height: 150px;" src="images/{{file.name}}" alt=""></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 text-center">
                <button ng-click="backToList();" class="btn btn-primary">Back To List</button> <button ng-click="editVehicle(myVehicle);" class="btn btn-success">Edit Vehicle</button>
            </div>
        </div>
    </div>

</div>