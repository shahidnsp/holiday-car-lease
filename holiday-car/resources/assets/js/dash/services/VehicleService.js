/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('VehcleService',[]).factory('Vehicle',['$resource',
    function($resource){
        return $resource('/api/vehicle/:vehicleId',{
            vehicleId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);