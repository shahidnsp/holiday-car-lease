/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('BookingService',[]).factory('Booking',['$resource',
    function($resource){
        return $resource('/api/booking/:bookingId',{
            bookingId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);