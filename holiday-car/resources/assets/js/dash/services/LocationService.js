/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('LocationService',[]).factory('Location',['$resource',
    function($resource){
        return $resource('/api/pickuplocatioon/:pickuplocatioonId',{
            pickuplocatioonId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);