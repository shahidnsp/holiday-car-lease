/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('GalleryService',[]).factory('Gallery',['$resource',
    function($resource){
        return $resource('/api/gallery/:galleryId',{
            galleryId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);