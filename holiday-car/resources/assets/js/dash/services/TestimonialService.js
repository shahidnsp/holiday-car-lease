/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('TestimonialService',[]).factory('Testimonial',['$resource',
    function($resource){
        return $resource('/api/testimonial/:testimonialId',{
            testimonialId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);