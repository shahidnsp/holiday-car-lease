/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('NewestCarService',[]).factory('NewestCar',['$resource',
    function($resource){
        return $resource('/api/newestcar/:newestcarId',{
            newestcarId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);