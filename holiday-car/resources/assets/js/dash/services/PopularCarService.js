/**
 * Created by Shahid Neermunda on 22/7/17.
 */

angular.module('PopularCarService',[]).factory('PopularCar',['$resource',
    function($resource){
        return $resource('/api/popularcar/:popularcarId',{
            popularcarId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);