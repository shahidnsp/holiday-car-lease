/**
 * Created by Shahid Neermunda on 21/7/17.
 */

angular.module('BrandService',[]).factory('Brand',['$resource',
    function($resource){
        return $resource('/api/brand/:brandId',{
            brandId:'@id'
        },{
            update:{
                method:'PUT'
            }
        });
    }
]);