app.controller('GalleryController', function($scope,$http,$anchorScroll,ngNotify,Gallery){
    $scope.galleries=[];
    $scope.galleryedit=false;
    $scope.isEdit=false;

    Gallery.query(function(gallery){
        $scope.galleries=gallery;
    });

    $scope.newGallery = function (argument) {
        $scope.galleryedit = true;
        $scope.newgallery = new Gallery();
        $scope.curGallery = {};
        $scope.isEdit=false;
    };
    $scope.editGallery = function (thisGallery) {
        $scope.galleryedit = true;
        $scope.curGallery =  thisGallery;
        $scope.newgallery = angular.copy(thisGallery);
        $anchorScroll();
        $scope.isEdit=true;
    };
    $scope.addGallery = function () {

        if ($scope.curGallery.id) {
            $scope.newgallery.$update(function(gallery){
                angular.extend($scope.curGallery, $scope.curGallery, gallery);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Updated Successfully');
            });
        } else{
            $scope.newgallery.$save(function(gallery){
                $scope.galleries.push(gallery);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Saved Successfully');
            });
        }
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };
    $scope.deleteGallery = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.galleries.indexOf(item);
                $scope.galleries.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Gallery Removed Successfully');
            });
        }
    };
    $scope.cancelGallery = function () {
        $scope.galleryedit = false;
        $scope.newgallery = new Gallery();
    };

    $scope.deletePhoto=function(item){
        var r = confirm("Are you sure want to delete this photo!");
        if (r == true) {
            $http.delete('api/file/' + item.id).
                success(function (data, status, headers, config) {
                    var curIndex = $scope.newgallery.files.indexOf(item);
                    $scope.newgallery.files.splice(curIndex, 1);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };
});