app.controller('StatusController', function($scope,$http,$anchorScroll,$rootScope,$fancyModal,ngNotify){


    $scope.status='All';


    $scope.loadStatus=function(status){
        $http.get('/api/getVehicleStatus',{params:{status:status}}).
            success(function(data, status)
            {
                $scope.vehicles=data;
            });
    };

    $scope.loadStatus('All');

    $scope.endBooking=function(vehicle){
        var confirmDelete = confirm("Do you really need to end Booking ?");
        if (confirmDelete) {
            /*$http.post('/api/endbooking', {booking_id: vehicle.bookingid}).
                success(function (data, status) {
                    $scope.loadStatus($scope.status);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking ended Successfully');
                });*/
            $fancyModal.open({
                templateUrl: 'template/makepaymentexist',
                controller: 'MakePaymentExistModalCtrl',
                resolve: {
                    booking: function () {
                        return vehicle;
                    }
                }
            });

        }
    };

    $scope.message=function(vehicle){
        $fancyModal.open({
            templateUrl: 'template/message',
            controller: 'MessageModalCtrl',
            resolve: {
                user_id: function () {
                    return vehicle.user_id;
                }
            }
        });
    };

    $scope.makePayment=function(vehicle){
        $fancyModal.open({
            templateUrl: 'template/makepayment',
            controller: 'MakePaymentModalCtrl',
            resolve: {
                booking: function () {
                    return vehicle;
                }
            }
        });
    };

    $rootScope.$on('$fancyModal.closed', function (e, id) {
        $scope.loadStatus('All');
    });

   /* $scope.showDetails=function(booking){
        $scope.myBooking=booking;
        $scope.showList=false;
    };

    $scope.backToList=function(){
        $scope.myBooking={};
        $scope.showList=true;
    };*/

});

app.controller('MessageModalCtrl', function($scope,$http,ngNotify,$fancyModal,user_id){
    $scope.sendMessage=function(message){
        $http.post('/api/sendmessagetouser', {message:message,user_id:user_id}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});

app.controller('MakePaymentModalCtrl', function($scope,$http,ngNotify,$fancyModal,booking){
    $scope.booking=booking;

    $scope.balance=0;
    $scope.total=0;
    $scope.paid=0;

    var amount= 0,fee= 0,advance= 0,bal= 0,total=0;
    try{
        amount=parseFloat(booking.amount);
        fee=parseFloat(booking.processfee);
        advance=parseFloat(booking.advance);
        bal=(amount+fee)-advance;
        total=(amount+fee);
        $scope.balance=bal;
        $scope.total=total;
        $scope.paid=bal;
    }catch(ex) {}

    $scope.submit=function(booking,paid){
        $http.post('/api/makepayment', {id:booking.bookingid,paid:paid}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});

app.controller('MakePaymentExistModalCtrl', function($scope,$http,ngNotify,$fancyModal,booking){
    $scope.booking=booking;

    $scope.balance=0;
    $scope.total=0;
    $scope.paid=0;

    var amount= 0,fee= 0,advance= 0,bal= 0,total=0;
    try{
        amount=parseFloat(booking.amount);
        fee=parseFloat(booking.processfee);
        advance=parseFloat(booking.advance);
        bal=(amount+fee)-advance;
        total=(amount+fee);
        $scope.balance=bal;
        $scope.total=total;
        $scope.paid=bal;
    }catch(ex) {}

    $scope.submit=function(booking,paid){
        $http.post('/api/makepaymentexist', {id:booking.bookingid,paid:paid}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});