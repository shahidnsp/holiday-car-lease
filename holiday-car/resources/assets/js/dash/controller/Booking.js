app.controller('BookingController', function($scope,$http,$anchorScroll,$filter,$rootScope,$fancyModal,ngNotify,Booking,Vehicle,Location){

    $scope.bookingedit=false;
    $scope.showList=true;
    $scope.myBooking={};
    $scope.bookings=[];
    $scope.vehicles=[];
    $scope.locations=[];


    Booking.query(function(booking){
        $scope.bookings=booking;
    });

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    Location.query(function(location){
        $scope.Locations=location;
    });

    $scope.newBooking = function (argument) {
        $scope.bookingedit = true;
        $scope.newbooking = new Booking();
        $scope.curBooking = {};
        $scope.newbooking.pickup_date=new Date();
        $scope.newbooking.return_date=new Date();
        $scope.showList=true;
        $scope.myBooking={};
    };

    $scope.editBooking=function(thisBooking){
        $scope.bookingedit = true;
        $scope.newbooking =angular.copy(thisBooking);
        $scope.curBooking=thisBooking;
        $scope.showList=true;
        $scope.myBooking={};
    };

    $scope.fromDate=new Date();
    $scope.toDate=new Date();
    $scope.status=1;

    $scope.search=function(status,fromDate,toDate){
        Booking.query({status:status,fromDate:fromDate,toDate:toDate},function(booking){
            $scope.bookings=booking;
        });
    };

    $scope.getAmount=function(fromDate,toDate,vehicle_id){
        var from = $filter("date")(Date.parse(fromDate), 'yyyy-MM-dd hh:mm:ss');
        var to = $filter("date")(Date.parse(toDate), 'yyyy-MM-dd hh:mm:ss');

        $http.post('/getVehicleAmount', {fromDate:from,toDate:to,vehicle_id:vehicle_id}).
            success(function (data, status) {
               $scope.newbooking.amount=data;
            });
    };


    $scope.showDetails=function(booking){
        $scope.myBooking=booking;
        $scope.showList=false;
    };

    $scope.backToList=function(){
        $scope.myBooking={};
        $scope.showList=true;
    };


    $scope.deleteBooking = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.bookings.indexOf(item);
                $scope.bookings.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Booking Removed Successfully');
            });
        }
    };

    $scope.endBooking = function (item) {
        var confirmDelete = confirm("Do you really need to end Booking ?");
        if (confirmDelete) {

            $http.post('/api/endbooking',{id:item.id}).
                success(function(data, status)
                {
                    angular.extend(item, item, data);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Ended Successfully');
                });


        }
    };


    $scope.addBooking = function () {
       // $scope.newbooking.pickup_date = $scope.newbooking.pickup_date.toISOString();
        //$scope.newbooking.return_date = $scope.newbooking.return_date.toISOString();
            if ($scope.curBooking.id) {
                $scope.newbooking.$update(function(brand){
                    angular.extend($scope.curBooking, $scope.curBooking, brand);

                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Updated Successfully');
                });
            } else {
                $scope.newbooking.$save(function (booking) {
                    $scope.bookings.push(booking);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Saved Successfully');
                });
            }

            $scope.bookingedit = false;
            $scope.newbooking = new Booking();
            $scope.showList=true;
        };
    
    $scope.cancelBooking = function () {
        $scope.bookingedit = false;
        $scope.newbooking = new Booking();
    };

    $scope.moveToNotified=function(booking){

        if(booking.isnew==1){
            var modal=$fancyModal.open({
                templateUrl: 'template/advance',
                controller: 'AdvanceModalCtrl',
                resolve: {
                    booking: function () {
                        return booking;
                    }
                }
            });

        }else{
            $http.post('/api/updateBookingStatus',{id:booking.id}).
                success(function(data, status)
                {
                    angular.extend(booking, booking, data);
                    ngNotify.config({
                        theme: 'pure',
                        position: 'top',
                        duration: 3000,
                        type: 'info',
                        sticky: false,
                        button: true,
                        html: false
                    });

                    ngNotify.set('Booking Status Changed Successfully');
                });
        }

    };


    $rootScope.$on('$fancyModal.closed', function (e, id) {
        Booking.query(function(booking){
            $scope.bookings=booking;
        });
    });


});

app.controller('AdvanceModalCtrl', function($scope,$http,ngNotify,$fancyModal,booking){
    $scope.booking=booking;


    $scope.submit=function(booking){
        $http.post('/api/submitandapprovebooking', {id:booking.id,processfee:booking.processfee,advance:booking.advance}).
            success(function (data, status) {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Message Sent Successfully');
                $fancyModal.close();
            });
    };
});