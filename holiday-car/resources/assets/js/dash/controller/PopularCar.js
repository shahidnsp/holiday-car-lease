app.controller('PopularCarController', function($scope,$anchorScroll,ngNotify,Vehicle,PopularCar){

    $scope.caredit=false;

    $scope.vehicles=[];
    $scope.popularcars=[];

    PopularCar.query(function(popularcar){
        $scope.popularcars=popularcar;
    });

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    $scope.newPopularCar = function (argument) {
        $scope.caredit = true;
        $scope.newcar = new PopularCar();
    };
    $scope.addPopularCar = function () {
        $scope.newcar.$save(function(popularcar){
            $scope.popularcars.push(popularcar);
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });

            ngNotify.set('PopularCar Saved Successfully');
        });
        $scope.caredit = false;
        $scope.newcar = new PopularCar();
    };
    $scope.deletePopularCar = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.popularcars.indexOf(item);
                $scope.popularcars.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('PopularCar Car Removed Successfully');
            });
        }
    };
    $scope.cancelPopularCar = function () {
        $scope.caredit = false;
        $scope.newcar = new PopularCar();
    };





});