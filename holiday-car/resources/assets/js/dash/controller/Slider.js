app.controller('SliderController', function($scope,$http,$anchorScroll,ngNotify,Slider){
    $scope.sliders=[];
    $scope.slideredit=false;
    $scope.isEdit=false;

    function loadSlider() {
        $scope.sliders=[];
        Slider.query(function (slider) {
            $scope.sliders = slider;
        });
    }
    loadSlider();

    $scope.newSlider = function (argument) {
        $scope.slideredit = true;
        $scope.newslider = new Slider();
        $scope.curSlider = {};
        $scope.isEdit=false;
    };

    $scope.addSlider = function () {

        $scope.newslider.$save(function(data){
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });
            Slider.query(function (slider) {
                $scope.sliders = slider;
            });
            ngNotify.set('Slider Saved Successfully');

            //loadSlider();
        });

        $scope.slideredit = false;
        $scope.newslider = new Slider();


    };
    $scope.deleteSlider = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.sliders.indexOf(item);
                $scope.sliders.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Slider Removed Successfully');
            });
        }
    };
    $scope.cancelSlider = function () {
        $scope.slideredit = false;
        $scope.newslider = new Slider();
    };

});