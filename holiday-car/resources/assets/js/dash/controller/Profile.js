app.controller('ProfileController', function($scope,$http,ngNotify){
    $scope.updateProfile=function(){
        $http.post('/api/updateProfile',{name:$scope.name,username:$scope.username}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Profile Changed Successfully...Please Re login to Change Profile name');
            });
    };

    $scope.changePassword=function(curpassword,newpassword,repassword){
        $http.post('/api/changePassword',{curpassword:curpassword,newpassword:newpassword,repassword:repassword}).
            success(function(data, status)
            {
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Password Changed Successfully...Please Re login');
            });
    };
});