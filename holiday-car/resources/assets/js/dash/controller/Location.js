app.controller('LocationController', function($scope,$anchorScroll,ngNotify,Location){
    $scope.locations=[];

    $scope.locationedit=false;
    $scope.curLocation={};

    Location.query(function(location){
        $scope.locations=location;
    });

    $scope.newLocation=function(){
        $scope.locationedit=true;
        $scope.newlocation=new Location();
        $scope.curLocation={};
    };

    $scope.cancelLocation=function(){
        $scope.locationedit=false;
        $scope.newlocation=new Location();
    };

    $scope.editLocation=function(thisLocation){
        $scope.curLocation=thisLocation;
        $scope.newlocation=angular.copy(thisLocation);
        $scope.locationedit=true;
        $anchorScroll();
    };

    $scope.addLocation=function(){
        if( $scope.curLocation.id){
            $scope.newlocation.$update(function(brand){
                angular.extend($scope.curLocation, $scope.curLocation, brand);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Location Updated Successfully');
            });
        }else {
            $scope.newlocation.$save(function (location) {
                $scope.locations.push(location);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Location Saved Successfully');
            });
        }

        $scope.locationedit=false;
        $scope.newlocation=new Location();
    };

    $scope.deleteLocation=function(item){
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.locations.indexOf(item);
                $scope.locations.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Location Removed Successfully');
            });
        }
    };


});