app.controller('ContactController', function($scope,$http,$anchorScroll,ngNotify,Contact){
    $scope.contacts=[];

    Contact.query(function(contact){
        $scope.contacts=contact;
    });

    $scope.showMessage=false;
    $scope.myMessage={};

    $scope.showSingleMessage=function(message){
        $scope.showMessage=true;
        $scope.myMessage=message;
    };

    $scope.backToList=function(){
        $scope.showMessage=false;
        $scope.myMessage={};
    };
    
    $scope.deleteContact = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.contacts.indexOf(item);
                $scope.contacts.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Contact Removed Successfully');
            });
        }
    };
});