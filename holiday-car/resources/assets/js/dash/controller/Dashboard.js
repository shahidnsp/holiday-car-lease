app.controller('DashboardController', function($scope,$http){
    $scope.status={};

    function loadStatus() {
        $http.get('/api/getStatus').
            success(function (data, status) {
                $scope.status = data;
                console.log(status);
            });
    }
    loadStatus();

    $scope.todaybookings=[];
    function loadTodayBooking() {
        $http.get('/api/todayBooking').
            success(function (data, status) {
                $scope.todaybookings = data;
            });
    }
    loadTodayBooking();

    $scope.todaybookings=[];
    function loadnewBooking() {
        $http.get('/api/newBooking').
            success(function (data, status) {
                $scope.newbookings = data;
                console.log(data);
            });
    }
    loadnewBooking();
});