app.controller('NewestCarController', function($scope,$anchorScroll,ngNotify,Vehicle,NewestCar){

    $scope.caredit=false;
    
    $scope.vehicles=[];

    $scope.newestcars=[];

    NewestCar.query(function(newestcar){
        $scope.newestcars=newestcar;
    });

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    $scope.newNewestCar = function (argument) {
        $scope.caredit = true;
        $scope.newcar = new NewestCar();
    };
    $scope.addNewestCar = function () {
        $scope.newcar.$save(function(newestcar){
            $scope.newestcars.push(newestcar);
            ngNotify.config({
                theme: 'pure',
                position: 'top',
                duration: 3000,
                type: 'info',
                sticky: false,
                button: true,
                html: false
            });

            ngNotify.set('NewestCar Saved Successfully');
        });
        $scope.caredit = false;
        $scope.newcar = new NewestCar();
    };
    $scope.deleteNewestCar = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.newestcars.indexOf(item);
                $scope.newestcars.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Newest Car Removed Successfully');
            });
        }
    };
    $scope.cancelNewestCar = function () {
        $scope.caredit = false;
        $scope.newcar = new NewestCar();
    };
});