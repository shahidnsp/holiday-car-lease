app.controller('BrandController', function($scope,$http,$anchorScroll,ngNotify,Brand){
    $scope.brands=[];
    $scope.brandedit=false;

    Brand.query(function(brand){
       $scope.brands=brand;
    });

    $scope.newBrand = function (argument) {
        $scope.brandedit = true;
        $scope.newbrand = new Brand();
        $scope.curBrand = {};
    };
    $scope.editBrand = function (thisBrand) {
        $scope.brandedit = true;
        $scope.curBrand =  thisBrand;
        $scope.newbrand = angular.copy(thisBrand);
        $anchorScroll();
    };
    $scope.addBrand = function () {

        if ($scope.curBrand.id) {
            $scope.newbrand.$update(function(brand){
                angular.extend($scope.curBrand, $scope.curBrand, brand);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brand Updated Successfully');
            });
        } else{
            $scope.newbrand.$save(function(brand){
                $scope.brands.push(brand);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brand Saved Successfully');
            });
        }
        $scope.brandedit = false;
        $scope.newbrand = new Brand();
    };
    $scope.deleteBrand = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.brands.indexOf(item);
                $scope.brands.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Brand Removed Successfully');
            });
        }
    };
    $scope.cancelBrand = function () {
        $scope.brandedit = false;
        $scope.newbrand = new Brand();
    };
});