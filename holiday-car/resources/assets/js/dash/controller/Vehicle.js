app.controller('VehicleController', function($scope,$http,$anchorScroll,ngNotify,Vehicle,Brand){

    $scope.vehicles=[];
    $scope.brands=[];
    $scope.vehicleedit=false;
    $scope.showVehicle=false;

    $scope.isEdit=false;

    Vehicle.query(function(vehicle){
        $scope.vehicles=vehicle;
    });

    Brand.query(function(brand){
        $scope.brands=brand;
    });

    $scope.myVehicle={};
    $scope.showSingleVehicle=function(vehicle){
        $scope.showVehicle=true;
        $scope.myVehicle=vehicle;
    };

    $scope.backToList=function(){
        $scope.showVehicle=false;
        $scope.myVehicle={};
    };

    $scope.newVehicle = function (argument) {
        $scope.vehicleedit = true;
        $scope.newvehicle = new Vehicle();
        $scope.curVehicle = {};
        $scope.isEdit=false;
    };

    $scope.editVehicle = function (thisVehicle) {
        $scope.vehicleedit = true;
        $scope.curVehicle =  thisVehicle;
        $scope.newvehicle = angular.copy(thisVehicle);
        $anchorScroll();
        $scope.isEdit=true;
    };

    $scope.cancelVehicle = function () {
        $scope.vehicleedit = false;
        $scope.newvehicle = new Vehicle();
    };

    $scope.deleteVehicle = function (item) {
        var confirmDelete = confirm("Do you really need to delete the item ?");
        if (confirmDelete) {
            //TODO error handling
            item.$delete(function(){
                var curIndex = $scope.vehicles.indexOf(item);
                $scope.vehicles.splice(curIndex, 1);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Vehicle Removed Successfully');
            });
        }
    };

    $scope.addVehicle = function () {

        if ($scope.curVehicle.id) {
            $scope.newvehicle.$update(function(vehicle){
                angular.extend($scope.curVehicle, $scope.curVehicle, vehicle);

                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Vehicle Updated Successfully');
            });
        } else{
            $scope.newvehicle.$save(function(vehicle){
                $scope.vehicles.push(vehicle);
                ngNotify.config({
                    theme: 'pure',
                    position: 'top',
                    duration: 3000,
                    type: 'info',
                    sticky: false,
                    button: true,
                    html: false
                });

                ngNotify.set('Vehicle Saved Successfully');
            });
        }
        $scope.vehicleedit = false;
        $scope.newvehicle = new Vehicle();
    };



    $scope.deletePhoto=function(item){
        var r = confirm("Are you sure want to delete this photo!");
        if (r == true) {
            $http.delete('api/file/' + item.id).
                success(function (data, status, headers, config) {
                    var curIndex = $scope.newvehicle.files.indexOf(item);
                    $scope.newvehicle.files.splice(curIndex, 1);
                    console.log(data);
                }).error(function (data, status, headers, config) {
                    console.log(data);
                });
        }
    };





});