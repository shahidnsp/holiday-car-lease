<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'pickup_date', 'return_date', 'description', 'pickup_location_id', 'vehicle_id','amount','bookingthrough','user_id','contact_information_id','home_address_id','proof_id'];

    public function pickup_location()
    {
        return $this->belongsTo('App\PickupLocation','pickup_location_id');
    }

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle','vehicle_id');
    }

    public function contact_information()
    {
        return $this->belongsTo('App\ContactInformation','contact_information_id');
    }

    public function home_address()
    {
        return $this->belongsTo('App\HomeAddress','home_address_id');
    }
}
