<?php
namespace App\Helper;

use Illuminate\Pagination\BootstrapThreePresenter;

class SmallPaginationPresenter extends BootstrapThreePresenter {

    public function render()
    {
        if ($this->hasPages())
        {
            return sprintf(
                '<ul class="post-pagination text-center">%s %s %s</ul>',
                $this->getPreviousButton(),
                $this->getLinks(),
                $this->getNextButton()
            );
        }

        return '';
    }
}


?>