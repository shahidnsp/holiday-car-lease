<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeAddress extends Model
{
    protected $fillable=['address1','address2','post','pincode','city','state','user_id'];
}
