<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactInformation extends Model
{
   protected $fillable=['name','mobile','email','dateofbirth','gender','user_id'];
}
