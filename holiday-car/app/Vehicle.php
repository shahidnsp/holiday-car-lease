<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'name',
        'transmission',
        'displacement',
        'fuel',
        'bhp',
        'mileage',
        'torque',
        'engine',
        'seat',
        'center_lock',
        'audio_system',
        'video',
        'gps',
        'air_bag',
        'no_airbag',
        'extra',
        'brand_id',
        'to15',
        'to25',
        'to35',
        'to45',
        'to55',
        'above',
        'modelyear',
        'noofgear',
    ];

    function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function booking()
    {
        return $this->hasMany('App\Booking');
    }

    public function newest_car()
    {
        return $this->hasOne('App\NewestCar');
    }

    public function files()
    {
        return $this->hasMany('App\File');
    }

    // this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();

        static::deleting(function($vehicle) { // before delete() method call this
            $vehicle->newest_car()->delete();
            $vehicle->booking()->delete();
        });
    }
}
