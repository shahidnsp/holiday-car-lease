<?php

namespace App\Http\Controllers;

use App\Booking;
use App\ContactInformation;
use App\HomeAddress;
use App\NewestCar;
use App\Notification;
use App\PopularCar;
use App\Proof;
use App\User;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;

class MobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getContactInformation(Request $request){
        $user_id=$request->user_id;
        return ContactInformation::where('user_id',$user_id)->get()->last();
    }

    public function saveContactInformation(Request $request){

        $information=new ContactInformation($request->all());
        if($information->save())
            return $information;
    }

    public function saveHomeAddress(Request $request){
        $home=new HomeAddress($request->all());
        if($home->save())
            return $home;
    }

    public function getHomeAddress(Request $request){
        $user_id=$request->user_id;
        return HomeAddress::where('user_id',$user_id)->get()->last();
    }

    public function getPopularCars(){
        return NewestCar::with('vehicle.brand')->with('vehicle.files')->get();
    }

    public function getMyLastBooking(Request $request){
        $user_id=$request->user_id;
        $bookings=Booking::where('user_id',$user_id)->where('endbooking',0)->with('vehicle.brand')->with('vehicle.files')->get();
        foreach($bookings as $booking){
            $fromDate = Carbon::parse($booking->pickup_date);
            $toDate = Carbon::parse($booking->return_date);
            $days = $toDate->diffInDays($fromDate);

            if ($days == 0)
                $days = 1;

            $booking['noofdays']=$days;

            $amount=$booking->amount;
            $fee=$booking->processfee;
            $advance=$booking->advance;

            $total=$amount+$fee;

            $booking['subtotal']=$total;
            $booking['balance']=$total-$advance;

        }

        return $bookings;
    }

    /**
     * For Saving PDF Files....................................................
     */
    private function savePDFFile($excel)
    {
        $fileName = '';
        try {
            if(strlen($excel) > 128) {
                list($ext, $data)   = explode(';', $excel);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($excel, 11, strpos($excel, ';')-11);
                $fileName = 'proof'.rand(11111,99999).'.pdf';//.$mime_type;
                // file_put_contents('uploads/excel/'.$fileName, $data);

                Storage::disk('local')->put($fileName,$data);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * For Saving Image Files....................................................
     */
    private function saveFile($excel)
    {
        $fileName = '';
        try {
            if(strlen($excel) > 128) {
                list($ext, $data)   = explode(';', $excel);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($excel, 11, strpos($excel, ';')-11);
                $fileName = 'profile'.rand(11111,99999).'.png';//.$mime_type;
                // file_put_contents('uploads/excel/'.$fileName, $data);

                Storage::disk('local')->put($fileName,$data);
            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    public function uploadProofNew(Request $request){
        $license=$request->license;
        $aadhar=$request->aadhar;
        $user_id=$request->user_id;


        if($license!=null || $aadhar!=null){
            $proof=new Proof();
            $proof->user_id=$user_id;
            /*if($license!=null) {
                foreach ($license as $licen) {
                    $proof->license = $this->savePDFFile($licen['data']);
                }
            }

            if($aadhar!=null) {
                foreach ($aadhar as $aadh) {
                    $proof->adhaar = $this->savePDFFile($aadh['data']);
                }
            }*/
            if($license!=null) {
                //$proof->license = $this->savePDFFile($license);
                $proof->license = $this->saveFile($license);
            }

            if($aadhar!=null) {
                //$proof->adhaar = $this->savePDFFile($aadhar);
                $proof->adhaar = $this->saveFile($aadhar);
            }

            if($proof->save())
                return $proof;
        }

        //$examkey->filename= $this->saveDATFile($keys[0]['data'],$type);
    }

    public function uploadProof(Request $request){
        $license=$request->license;
        $aadhar=$request->aadhar;
        $user_id=$request->user_id;


        if($license!=null || $aadhar!=null){
            $proof=new Proof();
            $proof->user_id=$user_id;
            if($license!=null) {
                foreach ($license as $licen) {
                    $proof->license = $this->savePDFFile($licen['data']);
                }
            }

            if($aadhar!=null) {
                foreach ($aadhar as $aadh) {
                    $proof->adhaar = $this->savePDFFile($aadh['data']);
                }
            }
           /* if($license!=null) {
                //$proof->license = $this->savePDFFile($license);
                $proof->license = $this->saveFile($license);
            }

            if($aadhar!=null) {
                //$proof->adhaar = $this->savePDFFile($aadhar);
                $proof->adhaar = $this->saveFile($aadhar);
            }*/

            if($proof->save())
                return $proof;
        }

        //$examkey->filename= $this->saveDATFile($keys[0]['data'],$type);
    }

    public function saveOrder(Request $request){
        $booking=new Booking($request->all());
        $user_id=$request->user_id;
        $proof=Proof::where('user_id',$user_id)->get()->last();
        if($proof!=null)
            $booking->proof_id=$proof->id;
        if($booking->save())
            return $booking;
    }

    public function getMyNotification(Request $request){
        $user_id=$request->user_id;
        return Notification::where('user_id',$user_id)->orderBy('created_at','desc')->get();
    }

    public function getLogin(Request $request){
        $username=$request->username;
        $password=$request->password;

        $user=User::where('username',$username)->get()->first();
        if($user!=null){
            if(\Hash::check($password,$user->password)){
                return $user;
            }else{
                return Response::json(array('error'=>'Record not found'),400);
            }
        }else{
            return Response::json(array('error'=>'NO Parameter....'),400);
        }
    }

    public function getSignup(Request $request){
        $name=$request->name;
        $username=$request->username;
        $password=$request->password;
        $user=new User();
        $user->name=$name;
        $user->username=$username;
        $user->password=$password;
        if($user->save()){
            return $user;
        }
    }

    public function updateMyProfile(Request $request){
        $photos=$request->photos;
        $user_id=$request->user_id;
        $name=$request->name;

        $user=User::findOrfail($user_id);
        if($user){
            $user->name=$name;
            if($photos!=null) {
                foreach ($photos as $photo) {
                    $user->photo = $this->saveFile($photo['data']);
                }
            }

            if($user->save()){
                return $user;
            }
        }
    }

    public function getMyProfile($id){
        $user=User::select('id','name','photo')->find($id);

        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     *
     *
     * NEW METHODS..................................
     */
    public function getSignupMobile(Request $request){
        $name=$request->name;
        $username=$request->username;
        $password=$request->password;
        $user=new User();
        $user->name=$name;
        $user->username=$username;
        $user->password=$password;

        $usenew=User::where('username',$username)->get()->first();
        if($usenew!=null){
            $json['token']='';
            $json['name']='';
            $json['status']=false;
            $json['message']='Username already taken';
            return $json;
        }

        if($user->save()){
            try {
                $credentials = $request->only('username', 'password');
                // attempt to verify the credentials and create a token for the user
                if (! $token = JWTAuth::attempt($credentials)) {
                    //return response()->json(['error' => 'invalid_credentials'], 401);
                    $json['token']='';
                    $json['name']='';
                    $json['status']=false;
                    $json['message']='invalid_credentials';
                    return $json;
                }
            } catch (JWTException $e) {
                // something went wrong whilst attempting to encode the token
                //return response()->json(['error' => 'could_not_create_token'], 500);
                $json['token']='';
                $json['name']='';
                $json['status']=false;
                $json['message']='could_not_create_token';
                return $json;
            }

            // all good so return the token
            $json['token']=$token;

            $user=Auth::user();

            $json['name']=$user->name;
            $json['status']=true;
            $json['message']='Success';
            //return response()->json($json);
            return $json;
        }
    }

    public function getMyContactInformationMobile(Request $request){

        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $contacts=ContactInformation::where('user_id',$user_id)->get()->last();

                $json['contacts']=$contacts;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['contacts']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function saveContactInformationMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $information=new ContactInformation($request->all());
                $information->user_id=$user_id;
                if($information->save()){
                    $json['saveddata']=$information;
                    $json['status']=true;
                    $json['loginstatus']=true;
                    $json['message']='Success';

                    return $json;
                }
                $json['saveddata']=[];
                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='Data not saved';

                return $json;

            }
        }catch (JWTException $e) {
            $json['saveddata']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function getHomeAddressMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $address= HomeAddress::where('user_id',$user_id)->get()->last();
                $json['address']=$address;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['address']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function saveHomeAddressMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $home=new HomeAddress($request->all());
                $home->user_id=$user_id;
                if($home->save()){
                    $json['saveddata']=$home;
                    $json['status']=true;
                    $json['loginstatus']=true;
                    $json['message']='Success';

                    return $json;
                }
                $json['saveddata']=[];
                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='Not Saved';

                return $json;


            }
        }catch (JWTException $e) {
            $json['saveddata']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function uploadProofMobile(Request $request){

        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $license=$request->license;
                $aadhar=$request->aadhar;

                if($license!=null || $aadhar!=null){
                    $proof=new Proof();
                    $proof->user_id=$user_id;
                    if($license!=null) {
                        $proof->license = $this->savePDFFile($license);
                    }

                    if($aadhar!=null) {
                        $proof->adhaar = $this->savePDFFile($aadhar);
                    }
                    if($proof->save()){
                        $json['status']=true;
                        $json['loginstatus']=true;
                        $json['message']='Success';

                        return $json;
                    }
                }
                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='Not Saved';

                return $json;


            }
        }catch (JWTException $e) {
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function saveOrderMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $booking=new Booking($request->all());
                $booking->user_id=$user_id;
                $booking->bookingthrough='Mobile';

                $proof=Proof::where('user_id',$user_id)->get()->last();
                if($proof!=null)
                    $booking->proof_id=$proof->id;

                $contact=ContactInformation::where('user_id',$user_id)->get()->last();
                if($contact!=null)
                    $booking->contact_information_id=$contact->id;

                $home=HomeAddress::where('user_id',$user_id)->get()->last();
                if($home!=null)
                    $booking->home_address_id=$home->id;


                if($booking->save()){
                    $json['booking_id']=$booking->id;
                    $json['status']=true;
                    $json['loginstatus']=true;
                    $json['message']='Success';

                    return $json;
                }

                $json['booking_id']=null;
                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='Not Saved';

                return $json;


            }
        }catch (JWTException $e) {
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function getMyProfileMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $user=User::select('id','name','photo')->find($user_id);

                $json['profile']=$user;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['profile']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }


    }

    public function updateMyProfileMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $photo=$request->photo;
                $name=$request->name;

                $user->name=$name;
                if($photo!=null) {
                    $user->photo = $this->saveFile($photo);
                }

                if($user->save()){
                    $json['status']=true;
                    $json['loginstatus']=true;
                    $json['message']='Success';
                    return $json;
                }

                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='Not saved';

                return $json;

            }
        }catch (JWTException $e) {
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function getPopularCarsMobile(){

        $newcars= NewestCar::with('vehicle.brand')->with('vehicle.files')->get();

        $json['popularcars']=$newcars;
        $json['status']=true;
        $json['message']='Success';

        return $json;
    }

    public function getMyNotificationMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $notifications=Notification::where('user_id',$user_id)->orderBy('created_at','desc')->get();
                $json['notifications']=$notifications;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';
                return $json;

            }
        }catch (JWTException $e) {
            $json['notifications']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function getMyLastBookingMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$request->user_id;
                $bookings=Booking::where('user_id',$user_id)->where('endbooking',0)->with('vehicle.brand')->with('vehicle.files')->get();
                foreach($bookings as $booking){
                    $fromDate = Carbon::parse($booking->pickup_date);
                    $toDate = Carbon::parse($booking->return_date);
                    $days = $toDate->diffInDays($fromDate);

                    if ($days == 0)
                        $days = 1;

                    $booking['noofdays']=$days;

                    $amount=$booking->amount;
                    $fee=$booking->processfee;
                    $advance=$booking->advance;

                    $total=$amount+$fee;

                    $booking['subtotal']=$total;
                    $booking['balance']=$total-$advance;

                }

                $json['bookings']=$bookings;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';
                return $json;
            }
        }catch (JWTException $e) {
            $json['bookings']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function endBookingMobile(Request $request){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $id=$request->booking_id;
                $booking=Booking::find($id);
                if($booking!=null){
                    if($booking->endbooking==0)
                        $booking->endbooking=1;
                    else
                        $booking->endbooking=0;
                    if($booking->save()){
                        $json['status']=true;
                        $json['loginstatus']=true;
                        $json['message']='Success';
                        return $json;
                    }
                }
                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='No Booking Found';
                return $json;
            }
        }catch (JWTException $e) {
            $json['bookings']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    public function extendMyBookingMobile(Request $request){

        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $id=$request->booking_id;
                $return_date=$request->return_date;

                $booking=Booking::find($id);
                if($booking!=null){
                    $booking->return_date=$return_date;

                    $returnDate=Carbon::parse($booking->return_date);
                    $pickupDate=Carbon::parse($booking->pickup_date);
                    $diff=$returnDate->diffInDays($pickupDate);
                    $amt=$this->getVehicleAmount($diff,$booking->vehicle_id);
                    $booking->amount=$amt;
                    if($booking->save())
                    {
                        $json['status']=true;
                        $json['loginstatus']=true;
                        $json['message']='Success';
                        return $json;
                    }
                }
                $json['status']=false;
                $json['loginstatus']=true;
                $json['message']='No Booking Found';
                return $json;
            }
        }catch (JWTException $e) {
            $json['bookings']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }


    }

    private  function getVehicleAmount($days,$vehicle_id){
        $amount = 0;
        if ($days == 0)
            $days = 1;

        $vehicle = Vehicle::find($vehicle_id);

        if ($vehicle) {
            if ($days >= 1 && $days <= 15) {
                $amount = $vehicle->to15;
            } elseif ($days > 15 && $days <= 25) {
                $amount = $vehicle->to25;
            } elseif ($days > 25 && $days <= 35) {
                $amount = $vehicle->to35;
            } elseif ($days > 35 && $days <= 45) {
                $amount = $vehicle->to45;
            } elseif ($days > 45 && $days <= 55) {
                $amount = $vehicle->to55;
            } elseif ($days > 55) {
                $amount = $vehicle->above;
            }
        }

        $total=$days*$amount;
        return $total;

    }

}
