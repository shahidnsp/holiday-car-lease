<?php

namespace App\Http\Controllers\Auth;

use App\File;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Storage;

class FileController extends Controller
{

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
        ]);
    }

    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return File::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $file=new File($request->all());

        if($file->save()) {
            return $file;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $file=File::findOrfail($id);
        $file->fill($request->all());


        if($file->update()) {
            return $file;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file=File::find($id);
        if($file) {
            if (strcmp($file->name,'car.png')==1 || strcmp($file->name,'car2.png')==1) {
                $exist = Storage::disk('local')->exists($file->name);
                if ($exist)
                    Storage::delete($file->name);
            }

            if (strcmp($file->temp,'cartemp.png')==1) {
                $exist = Storage::disk('local')->exists($file->temp);
                if ($exist)
                    Storage::delete($file->temp);
            }
        }
        if(File::destroy($id)) {
            return Response::json(array('msg' => 'File record deleted'));
        }
        else
            return Response::json(array('error'=>'File not found'),400);
    }

}
