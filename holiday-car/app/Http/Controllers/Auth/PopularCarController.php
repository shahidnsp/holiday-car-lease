<?php

namespace App\Http\Controllers\Auth;

use App\PopularCar;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class PopularCarController extends Controller
{

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'vehicle_id'=>'required',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PopularCar::with('vehicle')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $popularcar=new PopularCar($request->all());

        if($popularcar->save()) {

            return PopularCar::with('vehicle')->find($popularcar->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $popularcar=PopularCar::findOrfail($id);
        $popularcar->fill($request->all());


        if($popularcar->update()) {
            return $popularcar;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(PopularCar::destroy($id)) {
            return Response::json(array('msg' => 'PopularCar record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);

    }
}
