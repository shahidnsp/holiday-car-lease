<?php

namespace App\Http\Controllers\Auth;

use App\Booking;
use App\Brand;
use App\File;
use App\Notification;
use App\PickupLocation;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Image;
use Illuminate\Support\Facades\Storage;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class VehicleController extends Controller
{
    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'transmission'=>'required',
            'displacement'=>'required',
            'fuel'=>'required',
            'bhp'=>'required',
            'mileage'=>'required',
            'torque'=>'required',
            'to15'=>'required',
            'to25'=>'required',
            'to35'=>'required',
            'to45'=>'required',
            'to55'=>'required',
            'above'=>'required',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Vehicle::with('brand')->with('files')->get();
    }

    public function getManualCars(){
        return Vehicle::with('brand')->with('files')->where('transmission','Manual')->get();
    }

    public function getManualCarsMobile(){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
               $cars= Vehicle::with('brand')->with('files')->where('transmission','Manual')->get();

                $json['manualcars']=$cars;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['manualcars']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }
    }

    public function getAutomaticCars(){
        return Vehicle::with('brand')->with('files')->where('transmission','Automatic')->get();
    }

    public function getAutomaticCarsMobile(){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $cars= Vehicle::with('brand')->with('files')->where('transmission','Automatic')->get();

                $json['automaticcars']=$cars;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['automaticcars']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }
    }

    public function getLocation(){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $locations= PickupLocation::all();

                $json['locations']=$locations;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['locations']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }
    }

    public function getBrand(){
        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $brands= Brand::all();

                $json['brands']=$brands;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['brands']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }
    }

    public function getCars(Request $request){
        $brand_id=$request->brand_id;
        return Vehicle::where('brand_id',$brand_id)->with('brand')->with('files')->get();
    }

    public function getCarsByBrand(Request $request){
        try {
            $brand_id=$request->brand_id;
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $cars=Vehicle::where('brand_id',$brand_id)->with('brand')->with('files')->get();

                $json['cars']=$cars;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['cars']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }
    }

    public function getMyBooking(Request $request){

        try {
            $user = JWTAuth::parseToken()->authenticate();
            if($user!=null) {
                $user_id=$user->id;
                $bookings=Booking::where('user_id',$user_id)->with('pickup_location')->with('vehicle.brand')->with('vehicle.files')->orderBy('pickup_date','desc')->get();
                foreach($bookings as $booking){
                    $returnDate=Carbon::parse($booking->return_date);
                    $pickupDate=Carbon::parse($booking->pickup_date);

                    $now=Carbon::now();
                    if($returnDate > $now){
                        $booking['status']='Open';
                        $booking['remaining']=Carbon::parse(Carbon::now())->diffInDays($returnDate);
                        $booking['totaldays']=$pickupDate->diffInDays($returnDate);
                    }else{
                        $booking['status']='Closed';
                        $booking['remaining']='';
                        $booking['totaldays']=$pickupDate->diffInDays($returnDate);
                    }
                }

                $json['bookings']=$bookings;
                $json['status']=true;
                $json['loginstatus']=true;
                $json['message']='Success';

                return $json;

            }
        }catch (JWTException $e) {
            $json['bookings']=[];
            $json['status']=false;
            $json['loginstatus']=false;
            $json['message']='Token Expired or Invalid';

            return $json;
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $vehicle=new Vehicle($request->all());

        if($vehicle->save()) {
            if($request->photos!=null){
                $photos=$request->photos;
                foreach($photos as $photo){
                    $file=new File();
                    $file->name=$this->savePhoto($photo['data']);
                    $file->vehicle_id=$vehicle->id;
                    $file->save();
                }
            }
            return Vehicle::with('brand')->with('files')->find($vehicle->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'car'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(600, 500)->stream();
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mycar=\App\Vehicle::with('brand')->with('files')->find($id);
        return view('frontend.cardetails',compact('mycar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $vehicle=Vehicle::findOrfail($id);
        $vehicle->fill($request->all());


        if($vehicle->update()) {
            if($request->photos!=null){
                /*$oldfiles=File::where('vehicle_id',$vehicle->id)->get();
                foreach($oldfiles as $oldfile){
                    if($oldfile->name != 'profile.jpg') {
                        $exist=Storage::disk('local')->exists($oldfile->name);
                        if($exist)
                            Storage::delete($oldfile->name);
                    }
                }
                $oldfiles=File::where('vehicle_id',$vehicle->id)->delete();*/
                $photos=$request->photos;
                foreach($photos as $photo){
                    $file=new File();
                    $file->name=$this->savePhoto($photo['data']);
                    $file->vehicle_id=$vehicle->id;
                    $file->save();
                }
            }
            return Vehicle::with('brand')->with('files')->find($vehicle->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldfiles=File::where('vehicle_id',$id)->get();
        foreach($oldfiles as $oldfile){
            if($oldfile->name != 'profile.jpg') {
                $exist=Storage::disk('local')->exists($oldfile->name);
                if($exist)
                    Storage::delete($oldfile->name);
            }
        }
        $oldfiles=File::where('vehicle_id',$id)->delete();
        if(Vehicle::destroy($id)) {
            return Response::json(array('msg' => 'Vehicle record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }

    public function getVehicleAmount(Request $request){
        $from=$request->fromDate;
        $to=$request->toDate;
        $vehicle_id=$request->vehicle_id;
        $amount = 0;
        $days=0;
        if($from!=null && $to !=null && $vehicle_id!=null) {

            $to = date('Y-m-d H:i:s', strtotime('+23 hour 59 minutes', strtotime($to)));

            $fromDate = Carbon::parse($from);
            $toDate = Carbon::parse($to);
            $days = $toDate->diffInDays($fromDate);

            if ($days == 0)
                $days = 1;

            $vehicle = Vehicle::find($vehicle_id);

            if ($vehicle) {
                if ($days >= 1 && $days <= 15) {
                    $amount = $vehicle->to15;
                } elseif ($days > 15 && $days <= 25) {
                    $amount = $vehicle->to25;
                } elseif ($days > 25 && $days <= 35) {
                    $amount = $vehicle->to35;
                } elseif ($days > 35 && $days <= 45) {
                    $amount = $vehicle->to45;
                } elseif ($days > 45 && $days <= 55) {
                    $amount = $vehicle->to55;
                } elseif ($days > 55) {
                    $amount = $vehicle->above;
                }
            }
        }

        $total=$days*$amount;
        return $total;

    }

    public function getVehicleAmountMobile(Request $request){
        $from=$request->fromDate;
        $to=$request->toDate;
        $vehicle_id=$request->vehicle_id;
        $amount = 0;
        $givendays=$request->days;
        $days=0;
        if($givendays!=null && $vehicle_id!=null) {

           /* $to = date('Y-m-d', strtotime(strtotime($to)));

            $fromDate = Carbon::parse($from);
            $toDate = Carbon::parse($to);*/
            $days = $givendays;

            if ($days == 0)
                $days = 1;

            $vehicle = Vehicle::find($vehicle_id);

            if ($vehicle) {
                if ($days >= 1 && $days <= 15) {
                    $amount = $vehicle->to15;
                } elseif ($days > 15 && $days <= 25) {
                    $amount = $vehicle->to25;
                } elseif ($days > 25 && $days <= 35) {
                    $amount = $vehicle->to35;
                } elseif ($days > 35 && $days <= 45) {
                    $amount = $vehicle->to45;
                } elseif ($days > 45 && $days <= 55) {
                    $amount = $vehicle->to55;
                } elseif ($days > 55) {
                    $amount = $vehicle->above;
                }
            }
        }

        $total=$days*$amount;
        return $total;

    }

    public function getVehicleAmountMobileNew(Request $request){
        $vehicle_id=$request->vehicle_id;
        $amount = 0;
        $givendays=$request->days;
        $days=0;
        if($givendays!=null && $vehicle_id!=null) {

           /* $to = date('Y-m-d', strtotime(strtotime($to)));

            $fromDate = Carbon::parse($from);
            $toDate = Carbon::parse($to);*/
            $days = $givendays;

            if ($days == 0)
                $days = 1;

            $vehicle = Vehicle::find($vehicle_id);

            if ($vehicle) {
                if ($days >= 1 && $days <= 15) {
                    $amount = $vehicle->to15;
                } elseif ($days > 15 && $days <= 25) {
                    $amount = $vehicle->to25;
                } elseif ($days > 25 && $days <= 35) {
                    $amount = $vehicle->to35;
                } elseif ($days > 35 && $days <= 45) {
                    $amount = $vehicle->to45;
                } elseif ($days > 45 && $days <= 55) {
                    $amount = $vehicle->to55;
                } elseif ($days > 55) {
                    $amount = $vehicle->above;
                }
            }
        }

        $total=$days*$amount;

        $json['amount']=$total;
        $json['status']=true;
        $json['message']='Success';
        return $json;


    }

    public function getVehicleStatus(Request $request){

        $status=$request->status;

        $lists=[];
        $vehicles=Vehicle::with('brand')->get();
        if($status=='All'){
            foreach($vehicles as $vehicle){
                    $booking=Booking::with('pickup_location')->where('vehicle_id',$vehicle->id)->where('endbooking',0)->get()->last();
                    if($booking!=null){
                        $vehicle['bookingid']=$booking->id;
                        $vehicle['user_id']=$booking->user_id;
                        $vehicle['person']=$booking->name;
                        $vehicle['phone']=$booking->phone;
                        $vehicle['pickup_date']=$booking->pickup_date;
                        $vehicle['return_date']=$booking->return_date;
                        $vehicle['bookingthrough']=$booking->bookingthrough;
                        $vehicle['processfee']=$booking->processfee;
                        $vehicle['advance']=$booking->advance;
                        if($booking->pickup_location!=null)
                            $vehicle['location']=$booking->pickup_location->name;
                        $vehicle['amount']=$booking->amount;

                        $pickup=Carbon::parse($booking->pickup_date);
                        $return=Carbon::parse($booking->return_date);
                        $now=Carbon::now();

                        if($now<$return){
                            $vehicle['status']='Running';
                        }else{
                            $vehicle['status']='Overdue';
                        }

                    }else{
                        $vehicle['status']='Available';
                    }
                array_push($lists,$vehicle);
            }
        }elseif($status=='Running'){
            foreach($vehicles as $vehicle){
                $booking=Booking::with('pickup_location')->where('vehicle_id',$vehicle->id)->where('endbooking',0)->get()->last();
                if($booking!=null){
                    $vehicle['bookingid']=$booking->id;
                    $vehicle['user_id']=$booking->user_id;
                    $vehicle['person']=$booking->name;
                    $vehicle['phone']=$booking->phone;
                    $vehicle['pickup_date']=$booking->pickup_date;
                    $vehicle['return_date']=$booking->return_date;
                    $vehicle['bookingthrough']=$booking->bookingthrough;
                    if($booking->pickup_location!=null)
                        $vehicle['location']=$booking->pickup_location->name;
                    $vehicle['amount']=$booking->amount;

                    $pickup=Carbon::parse($booking->pickup_date);
                    $return=Carbon::parse($booking->return_date);
                    $now=Carbon::now();

                    if($now<$return){
                        $vehicle['status']='Running';
                    }else{
                        $vehicle['status']='Overdue';
                    }

                }else{
                    $vehicle['status']='Available';
                }
                if($vehicle['status']=='Running'){
                    array_push($lists,$vehicle);
                }
            }
        }elseif($status=='Overdue'){
            foreach($vehicles as $vehicle){
                $booking=Booking::with('pickup_location')->where('vehicle_id',$vehicle->id)->where('endbooking',0)->get()->last();
                if($booking!=null){
                    $vehicle['bookingid']=$booking->id;
                    $vehicle['user_id']=$booking->user_id;
                    $vehicle['person']=$booking->name;
                    $vehicle['phone']=$booking->phone;
                    $vehicle['pickup_date']=$booking->pickup_date;
                    $vehicle['return_date']=$booking->return_date;
                    $vehicle['bookingthrough']=$booking->bookingthrough;
                    if($booking->pickup_location!=null)
                        $vehicle['location']=$booking->pickup_location->name;
                    $vehicle['amount']=$booking->amount;

                    $pickup=Carbon::parse($booking->pickup_date);
                    $return=Carbon::parse($booking->return_date);
                    $now=Carbon::now();

                    if($now<$return){
                        $vehicle['status']='Running';
                    }else{
                        $vehicle['status']='Overdue';
                    }

                }else{
                    $vehicle['status']='Available';
                }
                if($vehicle['status']=='Overdue'){
                    array_push($lists,$vehicle);
                }
            }
        }elseif($status=='Available'){
            foreach($vehicles as $vehicle){
                $booking=Booking::with('pickup_location')->where('vehicle_id',$vehicle->id)->where('endbooking',0)->get()->last();
                if($booking!=null){
                    $vehicle['bookingid']=$booking->id;
                    $vehicle['user_id']=$booking->user_id;
                    $vehicle['person']=$booking->name;
                    $vehicle['phone']=$booking->phone;
                    $vehicle['pickup_date']=$booking->pickup_date;
                    $vehicle['return_date']=$booking->return_date;
                    $vehicle['bookingthrough']=$booking->bookingthrough;
                    if($booking->pickup_location!=null)
                        $vehicle['location']=$booking->pickup_location->name;
                    $vehicle['amount']=$booking->amount;

                    $pickup=Carbon::parse($booking->pickup_date);
                    $return=Carbon::parse($booking->return_date);
                    $now=Carbon::now();

                    if($now<$return){
                        $vehicle['status']='Running';
                    }else{
                        $vehicle['status']='Overdue';
                    }
                }else{
                    $vehicle['status']='Available';
                }
                if($vehicle['status']=='Available'){
                    array_push($lists,$vehicle);
                }
            }
        }

        return $lists;
    }

    public function endBooking(Request $request){
        $booking_id=$request->booking_id;
        $booking=Booking::findOrfail($booking_id);
        if($booking){
            $booking->endbooking=1;
            if($booking->save()){
                return $booking;
            }
        }
    }

    public function sendMessageToUser(Request $request){
        $message=$request->message;
        $user_id=$request->user_id;
        $notification=new Notification();
        $notification->message=$message;
        $notification->user_id=$user_id;
        if($notification->save())
        {
            return $notification;
        }
    }

}
