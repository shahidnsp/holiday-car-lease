<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Response;
use Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    function updateProfile(Request $request){

        $validator = Validator::make($request->all(),[
            'name'=>'required',
            'username'=>'required',
        ]);

        if($validator->passes()) {
            $user = User::find(Auth::id());
            if ($user) {
                $user->name = $request->name;
                $user->username = $request->username;
                if ($user->save())
                    return $user;
            }
        }
        return $validator->errors()->all();
    }

    function changePassword(Request $request){

        $data = $request->only('curpassword','newpassword','repassword');
        $validator = Validator::make($data,[
            'curpassword'=>'required',
            'newpassword'=>'required',
        ]);


        if($validator->passes()){
            try{

                $user = User::findOrFail(Auth::id());
            }
            catch(ModelNotFoundException $e)
            {
                return Response::json(['error'=>'Requested user not found'],404);
            }



            if($user->changePassword($data['curpassword'],$data['newpassword'])){
                if($user->save()){
                    return $user;
                }
            }
            else
                return Response::json(['error'=>'Old password does not match'],400);
        }
        else
            return $validator->errors()->all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
