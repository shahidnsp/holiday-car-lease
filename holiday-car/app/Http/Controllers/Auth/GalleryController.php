<?php

namespace App\Http\Controllers\Auth;

use App\File;
use App\Gallery;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Image;
use Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Gallery::with('files')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $gallery=new Gallery($request->all());


        if($gallery->save()) {
            if($request->photos!=null){
                $photos=$request->photos;
                foreach($photos as $photo){
                    $file=new File();
                    $file->name=$this->savePhoto($photo['data']);
                    $file->temp=$this->savePhotoTemp($photo['data']);
                    $file->gallery_id=$gallery->id;
                    $file->save();
                }
            }
            return Gallery::with('files')->find($gallery->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'gallery'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(1920, 1200)->stream();
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    private function savePhotoTemp($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'temp'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(380, 300)->stream();
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $gallery=Gallery::findOrfail($id);
        $gallery->fill($request->all());


        if($gallery->update()) {

            if($request->photos!=null){
                /*$oldfiles=File::where('gallery_id',$gallery->id)->get();
                foreach($oldfiles as $oldfile){
                    $exist=Storage::disk('local')->exists($oldfile->name);
                    if($exist)
                        Storage::delete($oldfile->name);
                }
                $oldfiles=File::where('gallery_id',$gallery->id)->delete();*/
                $photos=$request->photos;
                foreach($photos as $photo){
                    $file=new File();
                    $file->name=$this->savePhoto($photo['data']);
                    $file->gallery_id=$gallery->id;
                    $file->save();
                }
            }
            return $gallery;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $oldfiles=File::where('gallery_id',$id)->get();
        foreach($oldfiles as $oldfile){
            if (strcmp($oldfile->name,'car.png')==1 || strcmp($oldfile->name,'car2.png')==1) {
                $exist = Storage::disk('local')->exists($oldfile->name);
                if ($exist)
                    Storage::delete($oldfile->name);
            }
            if (strcmp($oldfile->temp,'cartemp.png')==1) {
                $exist = Storage::disk('local')->exists($oldfile->temp);
                if ($exist)
                    Storage::delete($oldfile->temp);
            }
        }
        $oldfiles=File::where('gallery_id',$id)->delete();
        //
        if(Gallery::destroy($id)) {
            return Response::json(array('msg' => 'Gallery record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}
