<?php

namespace App\Http\Controllers\Auth;

use App\Booking;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;

class BookingController extends Controller
{

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'email'=>'required',
            'phone'=>'required',
            'pickup_date'=>'required',
            'return_date'=>'required',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $status=$request->status;
        $fromDate=$request->fromDate;
        $toDate=$request->toDate;
        if($status==null && $fromDate==null && $toDate==null) {
            return Booking::with('pickup_location')->with('contact_information')->with('home_address')->with('vehicle.brand')->where('isnew',1)->get();
        }
        else{

            $toDate= date('Y-m-d H:i:s',strtotime('23 hour 59 minutes',strtotime($toDate)));
            if($status=='All'){
                return Booking::with('pickup_location')->with('contact_information')->with('home_address')->with('vehicle.brand')->where('created_at','>=',$fromDate)->where('created_at','<=',$toDate)->get();
            }else{
                return Booking::with('pickup_location')->with('contact_information')->with('home_address')->with('vehicle.brand')->where('created_at','>=',$fromDate)->where('created_at','<=',$toDate)->where('isnew',$status)->get();
            }
        }

    }

    public function endBooking(Request $request){
        $id=$request->id;
        $booking=Booking::find($id);
        if($booking!=null){
            if($booking->endbooking==0)
                $booking->endbooking=1;
            else
                $booking->endbooking=0;
            if($booking->save())
                return $booking;
        }
    }

    public function extendMyBooking(Request $request){
        $id=$request->id;
        $return_date=$request->return_date;

        $booking=Booking::find($id);
        if($booking!=null){
            $booking->return_date=$return_date;

            $returnDate=Carbon::parse($booking->return_date);
            $pickupDate=Carbon::parse($booking->pickup_date);
            $diff=$returnDate->diffInDays($pickupDate);
            $amt=$this->getVehicleAmount($diff,$booking->vehicle_id);
            $booking->amount=$amt;
            if($booking->save())
                return $booking;
        }
    }

    private  function getVehicleAmount($days,$vehicle_id){
        $amount = 0;
        if ($days == 0)
            $days = 1;

        $vehicle = Vehicle::find($vehicle_id);

        if ($vehicle) {
            if ($days >= 1 && $days <= 15) {
                $amount = $vehicle->to15;
            } elseif ($days > 15 && $days <= 25) {
                $amount = $vehicle->to25;
            } elseif ($days > 25 && $days <= 35) {
                $amount = $vehicle->to35;
            } elseif ($days > 35 && $days <= 45) {
                $amount = $vehicle->to45;
            } elseif ($days > 45 && $days <= 55) {
                $amount = $vehicle->to55;
            } elseif ($days > 55) {
                $amount = $vehicle->above;
            }
        }

        $total=$days*$amount;
        return $total;

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $booking=new Booking($request->all());

        if($booking->save()) {
            //return Booking::with('vehicle')->find($booking->id);

            return Booking::with('pickup_location')->with('vehicle')->find($booking->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    public function takeBooking(Request $request){
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $booking=new Booking($request->all());
        $from= date('Y-m-d',strtotime($request->pickup_date));
        $to= date('Y-m-d',strtotime($request->return_date));
        $booking->pickup_date=$from;
        $booking->return_date=$to;
        $booking->bookingthrough='Website';

        if($booking->save()) {
            $vehicle_id=$booking->vehicle_id;
            return view('frontend.messagebooking',compact('vehicle_id'));
        }

        return Response::json( ['error'=>'Server Down']
            ,400);

    }

    public function updateBookingStatus(Request $request){
        $id=$request->id;
        if($id!=null){
            $booking=Booking::find($id);
            if($booking){
                if($booking->isnew=='0')
                    $booking->isnew='1';
                else
                    $booking->isnew='0';
                if($booking->save())
                    return $booking;
            }
        }
        return Response::json( ['error'=>'Server Down']
            ,400);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $booking=Booking::findOrfail($id);
        $booking->fill($request->all());


        if($booking->update()) {
            return Booking::with('pickup_location')->with('vehicle')->find($booking->id);
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Booking::destroy($id)) {
            return Response::json(array('msg' => 'Booking record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);

    }

    public function getMyBooking(Request $request){
        $user_id=$request->user_id;
        $bookings=Booking::where('user_id',$user_id)->with('pickup_location')->with('vehicle.brand')->with('vehicle.files')->orderBy('pickup_date','desc')->get();
        foreach($bookings as $booking){
            $returnDate=Carbon::parse($booking->return_date);
            $pickupDate=Carbon::parse($booking->pickup_date);

            $now=Carbon::now();
            if($returnDate > $now){
                $booking['status']='Open';
                $booking['remaining']=Carbon::parse(Carbon::now())->diffInDays($returnDate);
                $booking['totaldays']=$pickupDate->diffInDays($returnDate);
            }else{
                $booking['status']='Closed';
                $booking['remaining']='';
                $booking['totaldays']=$pickupDate->diffInDays($returnDate);
            }
        }

        return $bookings;
    }

    public function submitAndApproveBooking(Request $request){
        $id=$request->id;
        $processfee=$request->processfee;
        $advance=$request->advance;

        $booking=Booking::find($id);
        if($booking!=null){
            $booking->processfee=$processfee;
            $booking->advance=$advance;
            $booking->isnew=0;
            if($booking->save())
                return $booking;
        }
    }

    public function makePayment(Request $request){
        $id=$request->id;
        $paid=$request->paid;
        $booking=Booking::find($id);
        if($booking!=null){
            $adv=$booking->advance;
            $adv=$adv+$paid;
            $booking->advance=$adv;
            if($booking->save())
                return $booking;
        }
    }

    public function makePaymentExist(Request $request){
        $id=$request->id;
        $paid=$request->paid;
        $booking=Booking::find($id);
        if($booking!=null){
            $adv=$booking->advance;
            $adv=$adv+$paid;
            $booking->advance=$adv;
            $booking->endbooking=1;
            if($booking->save())
                return $booking;
        }
    }
}
