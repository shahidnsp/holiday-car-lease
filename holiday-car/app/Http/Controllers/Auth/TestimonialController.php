<?php

namespace App\Http\Controllers\Auth;

use App\Testimonial;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Response;
use Image;
use Storage;

class TestimonialController extends Controller
{

    public function Validator(array $data)
    {
        return Validator::make($data,[
            'name'=>'required',
            'designation'=>'required',
            'content'=>'required',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Testimonial::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $testimonial=new Testimonial($request->all());
        if($request->photos==null){
            $testimonial->photo='profile.png';
        }else{
            $photos=$request->photos;
            foreach($photos as $photo){
                $testimonial->photo=$this->savePhoto($photo['data']);
            }
        }

        if($testimonial->save()) {

            return $testimonial;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'testimonial'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(79, 79)->stream();
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails()){
            return Response::json( $validator->errors()
                ,400);
        }

        $testimonial=Testimonial::findOrfail($id);
        $testimonial->fill($request->all());
        if($request->photos!=null){
            $photos=$request->photos;
            foreach($photos as $photo){
                $testimonial->photo=$this->savePhoto($photo['data']);
            }
        }

        if($testimonial->update()) {
            return $testimonial;
        }

        return Response::json( ['error'=>'Server Down']
            ,400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Testimonial::destroy($id)) {
            return Response::json(array('msg' => 'Testimonial record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}
