<?php

namespace App\Http\Controllers\Auth;

use App\Slider;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use Storage;
use Response;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Slider::all();
    }

    public function getSliders(){
        $sliders=Slider::all();
        $lists=[];
        foreach($sliders as $slider){
            $list=[];
            $list['src']=url('images/'.$slider->photo);
            $list['alt']=$slider->photo;
            array_push($lists,$list);
        }
        return json_encode($lists);
    }

    public function getSlidersMobile(){
        $sliders=Slider::all();
        $lists=[];
        foreach($sliders as $slider){
            $list=[];
            $list['src']=url('images/'.$slider->photo);
            $list['alt']=$slider->photo;
            array_push($lists,$list);
        }
        $json['sliders']=json_encode($lists);;
        $json['status']=true;
        $json['loginstatus']=true;
        $json['message']='Success';

        return $json;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->photos!=null){
            $photos=$request->photos;
            foreach($photos as $photo){
                $slider=new Slider();
                $slider->photo=$this->savePhoto($photo['data']);
                $slider->save();
            }
            return 'Ok';
        }
        return 'Failed';
    }


    private function savePhoto($photo)
    {
        $fileName = '';
        try {
            if(strlen($photo) > 128) {
                list($ext, $data)   = explode(';', $photo);
                list(, $data)       = explode(',', $data);
                $data = base64_decode($data);
                $mime_type = substr($photo, 11, strpos($photo, ';')-11);
                $fileName = 'gallery'.rand(11111,99999).'.'.$mime_type;
                //file_put_contents('uploads/images/'.$fileName, $data);

                $img=Image::make($data)->resize(1280, 936)->stream();
                Storage::disk('local')->put($fileName,$img);

            }
        }
        catch (\Exception $e) {
            $msg = $e;
        }
        return $fileName;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::find($id);
        if (strcmp($slider->photo,'slider1.jpg')==1) {
            $exist = Storage::disk('local')->exists($slider->photo);
            if ($exist)
                Storage::delete($slider->photo);
        }
        if(Slider::destroy($id)) {
            return Response::json(array('msg' => 'Slider record deleted'));
        }
        else
            return Response::json(array('error'=>'Record not found'),400);
    }
}
