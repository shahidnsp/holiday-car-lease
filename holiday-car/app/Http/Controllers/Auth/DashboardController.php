<?php

namespace App\Http\Controllers\Auth;

use App\Booking;
use App\Testimonial;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.index');
    }

    public function getStatus(){
        $data=[];

        $testimonialCount=Testimonial::count();
        $vehicleCount=Vehicle::count();
        $bookingCount=Booking::count();

        $data['totaltestimonial']=$testimonialCount;
        $data['totalvehicle']=$vehicleCount;
        $data['totalbooking']=$bookingCount;

        return $data;
    }

    public function todayBooking(){
        $from=date('Y-m-d');
        $to=date('Y-m-d');
        $to= date('Y-m-d H:i:s',strtotime('+23 hour 59 minutes',strtotime($to)));

        $booking=Booking::with('vehicle.brand')->where('created_at','>=',$from)->where('created_at','<=',$to)->get();

        return $booking;
    }

    public function newBooking(){
        $booking=Booking::with('vehicle.brand')->where('isnew','1')->get();
        return $booking;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
