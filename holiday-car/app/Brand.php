<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table='brands';
    protected $fillable = ['name', 'description'];

    public function vehicle()
    {
        return $this->hasMany('App\Vehicle');
    }

    // this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();

        static::deleting(function($brand) { // before delete() method call this
            $brand->vehicle()->delete();
        });
    }
}
